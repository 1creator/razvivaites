<?php

use Tygh\Registry;
use Tygh\Enum\ProfileTypes;
use Tygh\Enum\VendorStatuses;
use Tygh\Providers\VendorServicesProvider;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'apply_for_vendor') {

        if (Registry::get('settings.Vendors.apply_for_vendor') != 'Y') {
            return array(CONTROLLER_STATUS_NO_PAGE);
        }

        if (!empty($auth['user_type']) && ($auth['user_type'] == 'A' || $auth['user_type'] == 'V')) {
            fn_set_notification('E', __('error'), __('error_admin_registers_as_vendor'));
            return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
        }

        $data = $_REQUEST['company_data'];

        $data['timestamp'] = TIME;
        $data['status'] = VendorStatuses::NEW_ACCOUNT;
        $data['request_user_id'] = !empty($auth['user_id']) ? $auth['user_id'] : 0;

        $fields = isset($_REQUEST['company_data']['fields']) ? $_REQUEST['company_data']['fields'] : array();
        $company_data = fn_mve_extract_company_data_from_profile($fields);

        $account_data = array(
            'company_fields'  => $fields,
            'admin_firstname' => isset($_REQUEST['company_data']['admin_firstname']) ? $_REQUEST['company_data']['admin_firstname'] : $company_data['admin_firstname'],
            'admin_lastname'  => isset($_REQUEST['company_data']['admin_lastname']) ? $_REQUEST['company_data']['admin_lastname'] : $company_data['admin_lastname'],
        );

        $account_data['fields'] = fn_mve_profiles_match_company_and_user_fields($account_data['company_fields']);
        $data['request_account_data'] = serialize($account_data);

        if (empty($data['request_user_id'])) {
            $login_condition = empty($data['request_account_name']) ? '' : db_quote(" OR user_login = ?s", $data['request_account_name']);
            $user_account_exists = db_get_field('SELECT user_id FROM ?:users WHERE email = ?s ?p', $data['email'], $login_condition);

            if ($user_account_exists) {
                fn_save_post_data('user_data', 'company_data');
                fn_set_notification('E', __('error'), __('error_user_exists'));

                return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
            }
        }

        $company_id = fn_update_company($data);

        if (!$company_id) {
            fn_save_post_data('user_data', 'company_data');
            fn_set_notification('E', __('error'), __('text_error_adding_request'));

            return array(CONTROLLER_STATUS_REDIRECT, 'companies.apply_for_vendor');
        }

        $data = array_merge($data, fn_get_company_data($company_id));

        $msg = Tygh::$app['view']->fetch('views/companies/components/apply_for_vendor.tpl');
        fn_set_notification('I', __('information'), $msg);
        // Create activated vendor and sen password to vendor
        $status_from = 'N';
        fn_change_company_status($company_id, 'A', '', $status_from, false, true);
        // Notify user department on the new vendor application
        /** @var \Tygh\Mailer\Mailer $mailer */
        $mailer = Tygh::$app['mailer'];

        $mailer->send(array(
            'to' => 'default_company_users_department',
            'from' => 'default_company_users_department',
            'data' => array(
                'company_id' => $company_id,
                'company' => $data,
                'company_update_url' => fn_url('companies.update?company_id=' . $company_id, 'A', 'http')
            ),
            'template_code' => 'apply_for_vendor_notification',
            'tpl' => 'companies/apply_for_vendor_notification.tpl', // this parameter is obsolete and is used for back compatibility
        ), 'A', Registry::get('settings.Appearance.backend_default_language'));

        $return_url = !empty(Tygh::$app['session']['apply_for_vendor']['return_url']) ? Tygh::$app['session']['apply_for_vendor']['return_url'] : fn_url('');
        unset(Tygh::$app['session']['apply_for_vendor']['return_url']);

        return array(CONTROLLER_STATUS_REDIRECT, $return_url);
    }
}
