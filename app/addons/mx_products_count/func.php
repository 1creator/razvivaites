<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Registry;

function fn_mx_products_count_get_count($object_data) {

// Uncomment for Responsive theme (comment for UniTheme)
/*
    $location=fn_geo_maps_get_customer_stored_geolocation();
    $data = db_get_field("SELECT DISTINCT COUNT(*) FROM ?:products_categories INNER JOIN ?:products ON ?:products_categories.product_id = ?:products.product_id INNER JOIN ?:store_location_descriptions ON ?:products.ec_store_location_id = ?:store_location_descriptions.store_location_id WHERE city = ?s and lang_code = 'ru' and category_id = ?i and approved = ?s", $location['city'], $object_data['category_id'], 'Y');
*/

// Comment for Responsive theme (uncomment for UniTheme)
    $location=fn_geo_maps_get_customer_stored_geolocation();
    parse_str($object_data['href'], $output);
    $data = db_get_field("SELECT DISTINCT COUNT(*) FROM ?:products_categories INNER JOIN ?:products ON ?:products_categories.product_id = ?:products.product_id INNER JOIN ?:store_location_descriptions ON ?:products.ec_store_location_id = ?:store_location_descriptions.store_location_id WHERE city = ?s and lang_code = 'ru' and category_id = ?i and approved = ?s", $location['city'], $output['categories_view?category_id'], 'Y');

    return $data;

}