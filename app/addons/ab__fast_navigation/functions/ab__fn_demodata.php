<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\Registry;
use Tygh\Menu;
function fn_ab__fast_navigation_install_demodata() {
$res = array();
foreach ( array('fn_ab__fn_demodata_add_menus') as $func ) {
$res[$func] = $func();
}
return $res;
}
function fn_ab__fn_demodata_add_menus() {
$path = Registry::get('config.dir.var') . ab_____(base64_decode('YmNgYGVidWIwYmNgYGdidHVgb2J3amhidWpwbzA='));
$data = call_user_func(ab_____(base64_decode('Z29gaGZ1YGRwb3Vmb3V0')),"{$path}menu/data.json");
if (empty($data)) {
call_user_func(ab_____(base64_decode('Z29gdGZ1YG9wdWpnamRidWpwbw==')),ab_____(base64_decode('Rg==')), __(ab_____(base64_decode('ZnNzcHM='))), ab_____(base64_decode('VWlmc2YhYnNmIW9wIWVidWIi')));
return false;
}
$menu = call_user_func(ab_____(base64_decode('a3Rwb2BlZmRwZWY=')),call_user_func(ab_____(base64_decode('a3Rwb2Bmb2RwZWY=')),call_user_func(ab_____(base64_decode('a3Rwb2BlZmRwZWY=')),$data)), true);
if (empty($menu)){
call_user_func(ab_____(base64_decode('Z29gdGZ1YG9wdWpnamRidWpwbw==')),ab_____(base64_decode('Rg==')), __(ab_____(base64_decode('ZnNzcHM='))), /*t*/'There are no data!'/*t*/);
return false;
}
$menu_name = 'AB:_FAST_NAVIGATION_' . TIME;
$menu_data = array(
ab_____(base64_decode('b2JuZg==')) => $menu_name,
ab_____(base64_decode('bWJvaGBkcGVm')) => DESCR_SL,
ab_____(base64_decode('dHVidXZ0')) => 'A',
ab_____(base64_decode('ZHBucWJvemBqZQ=='))=> call_user_func(ab_____(base64_decode('VXpoaV1TZmhqdHVzejs7aGZ1')),ab_____(base64_decode('c3ZvdWpuZi9kcG5xYm96YGpl'))) ? call_user_func(ab_____(base64_decode('VXpoaV1TZmhqdHVzejs7aGZ1')),ab_____(base64_decode('c3ZvdWpuZi9kcG5xYm96YGpl'))) : 1,
);
$menu_id = Menu::update($menu_data);
fn_copy($path, fn_get_files_dir_path());
$parrents = array();
fn_ab__fn_demodata_run_throw_items ($path, $menu, $menu_id, $parrents);
call_user_func(ab_____(base64_decode('Z29gdGZ1YG9wdWpnamRidWpwbw==')),ab_____(base64_decode('Tw==')), __(ab_____(base64_decode('b3B1amRm'))), 'Menu <a target="_blank" href="' . fn_url('static_data.manage&section=A&menu_id=' . $menu_id) . '">' . $menu_name . ') was created!');
return $menu_id;
}
function fn_ab__fn_demodata_run_throw_items ($path, $menus, $menu_id, $parrents) {
foreach($menus as $menu) {
foreach ($menu as $m) {
$item = array(
ab_____(base64_decode('cWJzYm5gNA==')) => ($m[ab_____(base64_decode('cWJzYm5gNA=='))] == ab_____(base64_decode('RDsxO08='))) ? $m[ab_____(base64_decode('cWJzYm5gNA=='))] : '',
ab_____(base64_decode('cWJzYm5gNg==')) => $menu_id,
ab_____(base64_decode('cWJzZm91YGpl')) => !empty($parrents[$m[ab_____(base64_decode('cWJzZm91YGpl'))]]) ? $parrents[$m[ab_____(base64_decode('cWJzZm91YGpl'))]] : 0,
ab_____(base64_decode('ZWZ0ZHM=')) => $m[ab_____(base64_decode('anVmbg=='))],
ab_____(base64_decode('cWJzYm4=')) => $m[ab_____(base64_decode('aXNmZw=='))],
ab_____(base64_decode('cXB0anVqcG8=')) => $m[ab_____(base64_decode('cXB0anVqcG8='))],
ab_____(base64_decode('ZG1idHQ=')) => $m[ab_____(base64_decode('ZG1idHQ='))],
ab_____(base64_decode('YmNgYGdvYG5mb3ZgdHVidXZ0')) => $m[ab_____(base64_decode('YmNgYGdvYG5mb3ZgdHVidXZ0'))],
ab_____(base64_decode('YmNgYGdvYHZ0ZmBwc2poam9gam5iaGY=')) => $m[ab_____(base64_decode('YmNgYGdvYHZ0ZmBwc2poam9gam5iaGY='))],
ab_____(base64_decode('YmNgYGdvYG1iY2ZtYHVmeXU=')) => !empty($m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYHVmeXU='))]) ? $m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYHVmeXU='))] : "",
ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGRwbXBz')) => !empty($m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGRwbXBz'))]) ? $m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGRwbXBz'))] : "",
ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGNiZGxoc3B2b2U=')) => !empty($m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGNiZGxoc3B2b2U='))]) ? $m[ab_____(base64_decode('YmNgYGdvYG1iY2ZtYGNiZGxoc3B2b2U='))] : "",
ab_____(base64_decode('c3Y=')) => $m[ab_____(base64_decode('c3Y='))],
ab_____(base64_decode('bmZoYmNweQ==')) => array(
ab_____(base64_decode('dXpxZg==')) => array(ab_____(base64_decode('cWJzYm5gNA==')) => ($m[ab_____(base64_decode('cWJzYm5gNA=='))] == ab_____(base64_decode('RDsxO08='))) ? $m[ab_____(base64_decode('cWJzYm5gNA=='))] : ''),
ab_____(base64_decode('dnRmYGp1Zm4=')) => array(ab_____(base64_decode('cWJzYm5gNCghPj8hKFo='))),
),
);
$i =call_user_func(ab_____(base64_decode('Z29gYmNgYGdvYGJlZWB0dWJ1amRgZWJ1Yg==')),$item);
$parrents[$m[ab_____(base64_decode('cWJzYm5gamU='))]] = $i;
$images = array();
$img = ab_____(base64_decode('YmNgYGdvYG5mb3ZgamRwbw=='));
$images["{$img}_image_data"] = array(array(ab_____(base64_decode('dXpxZg==')) => ab_____(base64_decode('Tg==')), ab_____(base64_decode('cGNrZmR1YGpl')) => 0, ab_____(base64_decode('am5iaGZgYm11')) => '',));
$images["file_{$img}_image_icon"] = array(!empty($m[$img]) ? 'menu/' . $m[$img] : "");
$images["type_{$img}_image_icon"] = array(ab_____(base64_decode('dGZzd2Zz')));
$_REQUEST = call_user_func(ab_____(base64_decode('YnNzYnpgbmZzaGY=')),$_REQUEST, $images);
call_user_func(ab_____(base64_decode('Z29gYnV1YmRpYGpuYmhmYHFianN0')),ab_____(base64_decode('YmNgYGdvYG5mb3ZgamRwbw==')), ab_____(base64_decode('YmNgYGdvYG5mb3ZgamRwbw==')), $i);
}
}
}
function fn_ab__fn_add_static_data($data, $param_id = 0, $section = 'A', $lang_code = DESCR_SL) {
$current_id_path = '';
if ($data[ab_____(base64_decode('cWJzYm5gNA=='))] != ab_____(base64_decode('RDsxO08=')) and !empty($data[ab_____(base64_decode('bmZoYmNweQ=='))])){
foreach ($data[ab_____(base64_decode('bmZoYmNweQ=='))][ab_____(base64_decode('dXpxZg=='))] as $p => $v) {
if (!empty($v) and !empty($data[$p][$v])) {
$data[$p] = $v . ':' . intval($data[$p][$v]) . ':' . $data[ab_____(base64_decode('bmZoYmNweQ=='))][ab_____(base64_decode('dnRmYGp1Zm4='))][$p];
} else {
$data[$p] = '';
}
}
}
$condition =call_user_func(ab_____(base64_decode('ZWNgcnZwdWY=')),ab_____(base64_decode('cWJzYm5gamUhPiFAag==')), $param_id);
fn_set_hook(ab_____(base64_decode('dnFlYnVmYHR1YnVqZGBlYnVi')), $data, $param_id, $condition, $section, $lang_code);
if (!empty($param_id)) {
$current_id_path =call_user_func(ab_____(base64_decode('ZWNgaGZ1YGdqZm1l')),"SELECT id_path FROM ?:static_data WHERE $condition");
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('VlFFQlVGIUA7dHVidWpkYGVidWIhVEZVIUB2IVhJRlNGIXFic2JuYGplIT4hQGo=')), $data, $param_id);
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('VlFFQlVGIUA7dHVidWpkYGVidWJgZWZ0ZHNqcXVqcG90IVRGVSFAdiFYSUZTRiFxYnNibmBqZSE+IUBqIUJPRSFtYm9oYGRwZWYhPiFAdA==')), $data, $param_id, $lang_code);
} else {
$data[ab_____(base64_decode('dGZkdWpwbw=='))] = $section;
$param_id = $data[ab_____(base64_decode('cWJzYm5gamU='))] =call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('Sk9URlNVIUpPVVAhQDt0dWJ1amRgZWJ1YiFAZg==')), $data);
foreach (call_user_func(ab_____(base64_decode('Z29gaGZ1YHVzYm90bWJ1anBvYG1ib2h2YmhmdA=='))) as $data[ab_____(base64_decode('bWJvaGBkcGVm'))] => $_v) {
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('U0ZRTUJERiFKT1VQIUA7dHVidWpkYGVidWJgZWZ0ZHNqcXVqcG90IUBm')), $data);
}
}
if (call_user_func(ab_____(base64_decode('am9gYnNzYno=')),ab_____(base64_decode('c3Y=')), call_user_func(ab_____(base64_decode('YnNzYnpgbGZ6dA==')),call_user_func(ab_____(base64_decode('Z29gaGZ1YHVzYm90bWJ1anBvYG1ib2h2YmhmdA=='))))) and !empty($data[ab_____(base64_decode('c3Y='))])){
$data[ab_____(base64_decode('bWJvaGBkcGVm'))] = 'ru';
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('U0ZRTUJERiFKT1VQIUA7dHVidWpkYGVidWJgZWZ0ZHNqcXVqcG90IUBm')), call_user_func(ab_____(base64_decode('YnNzYnpgbmZzaGY=')),$data, $data[ab_____(base64_decode('c3Y='))]));
}
if (isset($data[ab_____(base64_decode('cWJzZm91YGpl'))])) {
if (!empty($data[ab_____(base64_decode('cWJzZm91YGpl'))])) {
$new_id_path =call_user_func(ab_____(base64_decode('ZWNgaGZ1YGdqZm1l')),ab_____(base64_decode('VEZNRkRVIWplYHFidWkhR1NQTiFAO3R1YnVqZGBlYnViIVhJRlNGIXFic2JuYGplIT4hQGo=')), $data[ab_____(base64_decode('cWJzZm91YGpl'))]);
$new_id_path .= '/' . $param_id;
} else {
$new_id_path = $param_id;
}
if (!empty($current_id_path) && $current_id_path != $new_id_path) {
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('VlFFQlVGIUA7dHVidWpkYGVidWIhVEZVIWplYHFidWkhPiFEUE9EQlUpQHQtIVRWQ1RVU0pPSClqZWBxYnVpLSFAaioqIVhJRlNGIWplYHFidWkhTUpMRiFAbQ==')), "$new_id_path/", strlen($current_id_path . '/') + 1, "$current_id_path/%");
}
call_user_func(ab_____(base64_decode('ZWNgcnZmc3o=')),ab_____(base64_decode('VlFFQlVGIUA7dHVidWpkYGVidWIhVEZVIWplYHFidWkhPiFAdCFYSUZTRiFxYnNibmBqZSE+IUBq')), $new_id_path, $param_id);
}
return $param_id;
}