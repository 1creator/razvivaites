<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\Registry;
foreach (glob(Registry::get("config.dir.addons") . "/ab__fast_navigation/functions/ab__fn_*.php") as $functions) require_once $functions;
function fn_ab__fast_navigation_install() {
$objects = array(
array( "t" => "?:static_data",
"i" => array(
array("n" => "ab__fn_menu_status", "p" => "char(1) NOT NULL DEFAULT 'N'"),
array("n" => "ab__fn_label_color", "p" => "char(7) NOT NULL DEFAULT '#ffffff'"),
array("n" => "ab__fn_label_background", "p" => "char(7) NOT NULL DEFAULT '#333333'"),
array("n" => "ab__fn_use_origin_image", "p" => "char(1) NOT NULL DEFAULT 'N'")
)
),
array( "t" => "?:static_data_descriptions",
"i" => array(
array("n" => "ab__fn_label_text", "p" => "varchar(100) NOT NULL DEFAULT ''"),
array("n" => "ab__fn_label_show", "p" => "char(1) NOT NULL DEFAULT 'Y'")
)
)
);
if (!empty($objects) and is_array($objects)){
foreach ($objects as $o){
$fields = db_get_fields('DESCRIBE ' . $o['t']);
if (!empty($fields) and is_array($fields)){
if (!empty($o['i']) and is_array($o['i'])){
foreach ($o['i'] as $f) {
if (!in_array($f['n'], $fields)){
db_query("ALTER TABLE ?p ADD ?p ?p", $o['t'], $f['n'], $f['p']);
}
}
}
}
}
}
}
function fn_ab__fn_get_menu_items ($item, $params) {
$items = fn_get_menu_items('', $params, '');
$items = array_filter($items, 'ab__fn_filter_items_by_status');
$images = fn_get_image_pairs(array_keys($items), 'ab__fn_menu_icon', 'M', true, false);
foreach ( $items as $key => &$item ) {
if (!empty($images[$key])){
$item['image'] = array_shift($images[$key]);
} elseif(!empty($item['param_3']) && strpos($item['param_3'], 'C') !== false) {
$str = stristr(substr(stristr($item['param_3'], ':'), 1), ':', true);
$item['image'] = fn_get_image_pairs($str, 'category', 'M', false, true);
}
if (!empty($item['subitems'])) {
fn_ab__fn_subelements_get_images( $item );
}
}
return $items;
}
function fn_ab__fn_subelements_get_images ( &$item ) {
$cat_images = $item_images = array();
foreach ($item['subitems'] as $subkey => &$subitem) {
unset($subitem['subitems']);
if (!isset($subitem['param_3']) and strpos($subitem['href'], 'categories.view') !== false){
list(, $category_id) = explode('=', $subitem['href']);
$subitem['is_category'] = $category_id;
$subitem['ab__fn_use_origin_image'] = 'N';
$cat_images[] = $category_id;
}else{
$item_images[] = $subitem['param_id'];
}
}
if (!empty($cat_images)){
$cat_images = fn_get_image_pairs(array_unique($cat_images), 'category', 'M', false, true);
}
if (!empty($item_images)){
$item_images = fn_get_image_pairs(array_unique($item_images), 'ab__fn_menu_icon', 'M', true, false);
}
$item['subitems'] = array_filter($item['subitems'], 'ab__fn_filter_items_by_status');
unset($subitem);
foreach ($item['subitems'] as $subkey => &$subitem) {
if (!empty($subitem['is_category']) and !empty($cat_images[$subitem['is_category']])) {
$subitem['image'] = array_shift($cat_images[$subitem['is_category']]);
} elseif (!empty($item_images[$subitem['param_id']])){
$subitem['image'] = array_shift($item_images[$subitem['param_id']]);
}
}
}
function ab__fn_filter_items_by_status( $item ) {
if( $item['ab__fn_menu_status'] == 'Y' || !empty($item['is_category']) )
return $item;
}
function fn_ab__fn_get_element_childs ($parent_id) {
$item = db_get_row('SELECT *
FROM ?:static_data AS sd
INNER JOIN ?:static_data_descriptions AS sdd
ON sd.param_id=sdd.param_id
WHERE sd.param_id=?i
AND lang_code=?s', $parent_id, CART_LANGUAGE);
$item['subitems'] = db_get_hash_array('SELECT *
FROM ?:static_data AS sd
INNER JOIN ?:static_data_descriptions AS sdd
ON sd.param_id=sdd.param_id
WHERE sd.parent_id=?i
AND lang_code=?s', 'param_id', $parent_id, CART_LANGUAGE);
$item = fn_top_menu_form(array( $parent_id => $item ));
$answ = array();
foreach ( $item as &$i ) {
fn_ab__fn_subelements_get_images( $i );
$answ = $i;
}
return $answ;
}