<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
if ($mode == 'refresh') {
if (!empty($_REQUEST['addon']) and $_REQUEST['addon'] == 'ab__fast_navigation') {
$addon = $_REQUEST['addon'];
$settings[$addon] = array(
'gid' => '912181484',
'conds_words' => array(
'abt_menu_icon_items',
'abt_menu_long_names',
'abt_menu_long_names_max_width',
'no_hidden_elements_second_level_view',
'elements_per_column_third_level_view',
'abt_menu_ajax_load',
),
);
$po_head = array(
'ru' => 'msgid ""
msgstr "Project-Id-Version: tygh\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Language-Team: Russian\n"
"Language: ru_RU\n"
',
'en' => 'msgid ""
msgstr "Project-Id-Version: tygh\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Language-Team: English\n"
"Language: en_US\n"
',
);
$conds_words = array('abt__', 'ab__',);
$no_trim_str = array(' - ', ' ');
$config = $settings[$addon];
$content = '';
if (!empty($content) and is_array($content)) {
$po_content = array();
$data = array();
foreach ($content as $str){
$check = false;
$cws = (!empty($config['conds_words'])) ? array_merge($conds_words, $config['conds_words']) : $conds_words;
foreach ($cws as $w){
if (strpos($str, $w) !== false){
$check = true;
break;
}
}
if ($check){
$s = str_getcsv($str);
$id = isset($s[0]) ? trim($s[0]) : "";
$ru = isset($s[1]) ? $s[1] : $id;
$en = isset($s[2]) ? $s[2] : $id;
$msgctxt = $id;
$msgid = $id;
if (strpos($id, 'name::') === 0){
$msgctxt = "Addons::{$id}";
$msgid = str_replace('name::', '', $id);
}elseif (strpos($id, 'description::') === 0){
$msgctxt = "Addons::{$id}";
$msgid = str_replace('description::', '', $id) . "_description";
}elseif (strpos($id, 'SettingsSections::' . $addon . '::') !== false){
$msgctxt = $id;
$msgid = str_replace('SettingsSections::' . $addon . '::', '', $id);
}elseif (strpos($id, 'SettingsOptions::' . $addon . '::') !== false){
$msgctxt = $id;
$msgid = str_replace('SettingsOptions::' . $addon . '::', '', $id);
}elseif (strpos($id, 'SettingsTooltips::' . $addon . '::') !== false){
$msgctxt = $id;
$msgid = str_replace('SettingsTooltips::' . $addon . '::', '', $id);
}elseif (strpos($id, 'SettingsVariants::' . $addon . '::') !== false){
$msgctxt = $id;
list(,,,$msgid) = explode('::', $id);
}else{
$msgctxt = "Languages::{$id}";
}
$ru = str_replace(array('"', ' ', "\n"), array('\"', '', "\"\n\""), $ru);
$en = str_replace(array('"', ' ', "\n"), array('\"', '', "\"\n\""), $en);
if (!in_array($ru, $no_trim_str)) $ru = trim($ru);
if (!in_array($en, $no_trim_str)) $en = trim($en);
if (preg_match_all('/\{\s*(?P<str>[^}]+?)\s*\}/', $ru, $matches)){
foreach ($matches[1] as $match){
if (!empty($data[$match]['ru'])){
$ru = str_replace('{' . $match .'}', $data[$match]['ru'], $ru);
}
}
}
if (preg_match_all('/\{\s*(?P<str>[^}]+?)\s*\}/', $en, $matches)){
foreach ($matches[1] as $match){
if (!empty($data[$match]['en'])){
$en = str_replace('{' . $match .'}', $data[$match]['en'], $en);
}
}
}
$data[$id]['ru'] = $ru;
$data[$id]['en'] = $en;
if (empty($po_content['ru'])) $po_content['ru'] = '';
if (empty($po_content['en'])) $po_content['en'] = '';
$po_content['ru'] .='
msgctxt "' . $msgctxt . '"
msgid "' . $msgid . '"
msgstr "' . $ru . '"
';
$po_content['en'] .='
msgctxt "' . $msgctxt . '"
msgid "' . $msgid . '"
msgstr "' . $en . '"
';
}
}
foreach ($po_content as $lang => &$po){
$po = $po_head[$lang] . $po . "\n#. Alexbranding. Do not modify this file manually!";
fn_put_contents(Registry::get('config.dir.lang_packs') . "$lang/addons/$addon.po", $po);
}
}
}
}
}