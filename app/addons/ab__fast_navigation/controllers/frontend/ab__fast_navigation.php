<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\BlockManager\Block;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
if ( $mode == 'load_subitems' && defined('AJAX_REQUEST') ) {
if ( !empty($_REQUEST['parent']) && !empty($_REQUEST['block']) ) {
$block = Block::instance()->getById($_REQUEST['block']['id'], $_REQUEST['block']['snapping']);
$childs = fn_ab__fn_get_element_childs ($_REQUEST['parent']);
$answer = array();
Tygh::$app['view']->assign('key', $_REQUEST['parent']);
if ( $_REQUEST['scroller'] === 'true' ) {
Tygh::$app['view']->assign('block', $block)
->assign('second_level_scroller', true)
->assign('language_direction', $_REQUEST['dir']);
$answer['js'] = Tygh::$app['view']->fetch('addons/ab__fast_navigation/blocks/ab__fast_navigation/js/second_level_scroller.tpl');
}
if (!empty($childs['subitems'])) {
Tygh::$app['view']->assign('image_path', defined('HTTPS') ? 'https_image_path' : 'http_image_path')
->assign('item', $childs)
->assign('first_level_icon_width', $block['properties']['ab__fn_icon_width']);
$answer['subitems'] = Tygh::$app['view']->fetch('addons/ab__fast_navigation/blocks/ab__fast_navigation/components/second_level_items.tpl');
}
Tygh::$app['ajax']->assign('second_level', $answer);
}
}
exit;
}