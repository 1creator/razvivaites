<?php

defined('BOOTSTRAP') or die('Access Denied');

use Tygh\Registry;
use Tygh\Storage;
use Illuminate\Support\Collection;

function fn_ec_get_vendor_store_points($company_id=0){
    $params['company_id'] = $company_id;
    list($store_locations, $search) = fn_get_store_locations($params, 0, DESCR_SL);
    return $store_locations;
}

function fn_ec_vendor_points_get_store_locations_before_select($params, $fields, &$joins, &$conditions){
    if(isset($params['store_ids'])){
        $store_ids = explode(",",$params['store_ids']);
        if(!empty($store_ids)){
            $conditions['store_ids'] = db_quote('?:store_locations.store_location_id IN (?n)', $store_ids);
        }
    }
}

function fn_get_products_vendor_stores($products)
{
    $stores = array();

    foreach ($products as $v) {
        if(!empty($v['ec_store_location_id'])){
            $stores[] = $v['ec_store_location_id'];
        }
    }
    return array_unique($stores);
}

function fn_ec_get_vendor_filter_map_data($store_ids = ''){
    $sl_search = array();
    $sl_search['store_ids'] = $store_ids;
    
    list($ec_store_locations, $ec_search) = fn_get_store_locations($sl_search);
    $grouped_locations = (new Collection($ec_store_locations))->groupBy('city')->toArray();

    return $grouped_locations;
}