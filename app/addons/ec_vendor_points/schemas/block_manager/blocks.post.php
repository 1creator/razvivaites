<?php

$schema['ec_vendor_points'] = array(
    'templates' => array(
        'addons/ec_vendor_points/blocks/ec_vendor_points.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'disable_cache_when' => array(
            'request_handlers' => array('q')
        )
    )
);

$schema['ec_vendor_map_filter'] = array(
    'templates' => array(
        'addons/ec_vendor_points/blocks/ec_vendor_map_filter.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'disable_cache_when' => array(
            'request_handlers' => array('q')
        )
    )
);

return $schema;
