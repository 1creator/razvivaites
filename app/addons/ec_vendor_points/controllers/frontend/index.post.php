<?php

defined('AREA') OR die('Access Denied');

use Illuminate\Support\Collection;
use Tygh\Registry;
use Tygh\Storage;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
}

if($mode == 'index'){
    $sl_search = isset($_REQUEST['sl_search']) ? $_REQUEST['sl_search'] : [];
    $params = [
        'status' => 'A',
    ];
    $cities = fn_get_store_location_cities($params);
    list($ec_store_locations, $ec_search) = fn_get_store_locations($sl_search);
    $grouped_locations = (new Collection($ec_store_locations))->groupBy('city')->toArray();

    Tygh::$app['view']->assign('ec_store_locations', $grouped_locations);
    Tygh::$app['view']->assign('ec_store_locations_count', count($ec_store_locations));
    Tygh::$app['view']->assign('cities', $cities);
    Tygh::$app['view']->assign('sl_search', $sl_search);  
}
