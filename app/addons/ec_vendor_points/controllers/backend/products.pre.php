<?php
use Tygh\Registry;
defined('AREA') OR die('Access Denied');

if($_SERVER['REQUEST_METHOD'] == 'POST'){
}

if($mode == 'update' || $mode =='add'){
    $ec_store_company = 0;
    if(isset($_REQUEST['product_data']['company_id'])){
        $ec_store_company = $_REQUEST['product_data']['company_id'];
    } elseif(!empty(Registry::get('user_info.company_id'))){
        $ec_store_company = Registry::get('user_info.company_id');
    } elseif(!isset($_REQUEST['product_id'])){
        $ec_store_company = fn_get_default_company_id();
    }
    if($ec_store_company)
    Tygh::$app['view']->assign([
        'ec_store_company' => $ec_store_company
    ]);
}