<?php
/************************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL            *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\Mailer;

/*function fn_get_registration_sttings_data($company_id = null)
{
  static $cache;

  if (empty($cache['settings_' . $company_id])) {
    $settings = Settings::instance()->getValue('wk_registration_confirmation_setting_data', '', $company_id);
    $settings = unserialize($settings);

    if (empty($settings)) {
      $settings = array();
    }

    $cache['settings_' . $company_id] = $settings;
  }
  return $cache['settings_' . $company_id];
}*/

function fn_wk_registration_confirmation_update_user_pre($user_id, $user_data, $auth, $ship_to_another, &$notify_user)
{
  if(Registry::get('settings.General.approve_user_profiles') == 'N')
  {
    if(empty($user_id) && AREA == 'C')
    {
      $notify_user = false;
      $user_id=db_get_field('SELECT user_id FROM ?:users WHERE email = ?s',$user_data['email']);
      if(empty($user_id))
      {
        $data = array(
          'email' => $user_data['email'],
          'password' => $user_data['password1'],
          );
        $_SESSION['wk_un_con_email']=$user_data['email'];
        db_query('INSERT INTO ?:wk_user_registration ?e',$data);
      }
    }
  }
}

function fn_wk_registration_confirmation_update_company($company_data, $company_id, $lang_code, $action) {
	if(Registry::get('addons.wk_registration_confirmation.vend_activate_setting') == 'Y') 
	{
		if($action == 'add' && AREA == 'C') 
		{
			$confirmation_code = fn_generate_password(20);
			$seting_exp_time=Registry::get('addons.wk_registration_confirmation.vend_expire_time');
			$seting_resend_time = !empty(Registry::get('addons.wk_registration_confirmation.vend_resend_time')) || !is_numeric(Registry::get('addons.wk_registration_confirmation.vend_resend_time')) || !(Registry::get('addons.wk_registration_confirmation.vend_resend_time') < 0) ?(3600 * Registry::get('addons.wk_registration_confirmation.vend_resend_time')):3600;
			if(empty($seting_exp_time) || !is_numeric($seting_exp_time) || ($seting_exp_time < 0))
					$exp_tym=6*3600;
				else
					$exp_tym=3600*$seting_exp_time;
			$data = array(
			'email' => $company_data['email'],
			'resend_time' => ($company_data['timestamp']+$exp_tym) - $seting_resend_time,
			'company_id' => $company_id,
			'expire_timestamp' => $company_data['timestamp']+$exp_tym,
			'confirmation_code' => $confirmation_code,
			);
			$_SESSION['wk_un_vend_email']=$company_data['email'];
			db_query('INSERT INTO ?:wk_vendor_registration ?e',$data);

			$email_data['company_id'] = 0;
			$email_data['email'] = $company_data['email'];
			if(isset($company_data['admin_firstname']) && isset($company_data['admin_lastname'])) {
				$email_data['name'] = $company_data['admin_firstname'].' '.$company_data['admin_lastname'];
			}else{
				$email_data['name'] = $company_data['company'];
			}
			$email_data['confirm_link'] = fn_url('user_registration_varify.vendor_varify&confirmation='.$confirmation_code.'&email='.$company_data['email']);
			$email_data['dor'] = date('d/m/Y',$company_data['timestamp']);
			$email_data['days_countdown'] = round(($company_data['timestamp']+$exp_tym - time())/3600);
			$from = 'default_company_users_department';
			
			$link_to_click = "<a href='$email_data[confirm_link]'>ссылке</a>";
			$wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
			$mailer = Tygh::$app['mailer'];
            $mailer->send(array(
			'to' => $company_data['email'],
			'from' => $from,
			'data' => array(
				'wk_data' => $wk_data,
			),
			'template_code' => 'vendor_confirmation_link_mail',
			'tpl' => 'addons/wk_registration_confirmation/vendor_confirmation.tpl',
			'company_id' => 0
			), 'C', Registry::get('settings.Appearance.backend_default_language'));
		}
	}
}

function fn_wk_registration_confirmation_update_profile($action, &$user_data, $current_user_data)
{
	if(Registry::get('settings.General.approve_user_profiles') == 'N')
	{
		if($action == 'add' && AREA == 'C')
		{
			if((fn_allowed_for('MULTIVENDOR') && empty($user_data['company_id'])) || fn_allowed_for('ULTIMATE')) {
				$confirmation_code = fn_generate_password(20);
				$seting_exp_time=Registry::get('addons.wk_registration_confirmation.cust_expire_time');
				$seting_resend_time = !empty(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !is_numeric(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !(Registry::get('addons.wk_registration_confirmation.cust_resend_time') < 0) ?(3600 * Registry::get('addons.wk_registration_confirmation.cust_resend_time')):3600;

				if(empty($seting_exp_time) || !is_numeric($seting_exp_time) || ($seting_exp_time < 0))
					$exp_tym=6*3600;
				else
					$exp_tym=3600*$seting_exp_time;
				$data = array(
				'user_id' => $user_data['user_id'],
				'resend_time' => ($user_data['timestamp']+$exp_tym) - $seting_resend_time,
				'expire_timestamp' => $user_data['timestamp']+$exp_tym,
				'confirmation_code' => $confirmation_code,
				);
				db_query('UPDATE ?:wk_user_registration SET ?u WHERE email = ?s', $data, $user_data['email']); 
				$updatedata = array('status' => 'D',);
				db_query('UPDATE ?:users SET ?u WHERE user_id = ?i', $updatedata, $user_data['user_id']);
				$user_data['status'] = 'D';

				$email_data['company_id'] = $user_data['company_id'];
				$email_data['email'] = $user_data['email'];
				$email_data['dor'] = date('d/m/Y',$user_data['timestamp']);
				$email_data['name'] = fn_get_user_name($user_data['user_id']);
				$email_data['confirm_link'] = fn_url('user_registration_varify.varify&confirmation='.$confirmation_code.'&email='.$user_data['email']);
				$email_data['days_countdown'] = round(($user_data['timestamp']+$exp_tym - time())/3600);
				$from = 'company_users_department';
				if (fn_allowed_for('MULTIVENDOR')) {
					// Vendor administrator's notification
					// is sent from root users department
					if ($user_data['user_type'] == 'V')
						$from = 'default_company_users_department';
				}
				
				$link_to_click = "<a href='$email_data[confirm_link]'>ссылке</a>";
				$wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
				$mailer = Tygh::$app['mailer'];
                $mailer->send(array(
				'to' => $user_data['email'],
				'from' => $from,
				'data' => array(
					'wk_data' => $wk_data,
				),
				'template_code' => 'confirmation_link_mail',
				'tpl' => 'addons/wk_registration_confirmation/customer_confirmation.tpl',
				'company_id' => $user_data['company_id']
				), 'C', Registry::get('settings.Appearance.backend_default_language'));

				fn_set_notification("N",__("notice"),__("check_email_and_confirm_registration"));
			}else{
				$updatedata = array('user_type' => 'V', 'password_change_timestamp' =>1,);
				db_query('UPDATE ?:users SET ?u WHERE user_id = ?i', $updatedata, $user_data['user_id']);
			}
		}
	}
}

function fn_wk_registration_confirmation_checkout_select_default_payment_method(&$cart, $payment_methods, &$completed_steps)
{
  if(isset($_SESSION['wk_un_con_email']))
  {
    $wk_exist_unconfirm=db_get_field('SELECT email FROM ?:wk_user_registration
    WHERE email = ?s',$_SESSION['wk_un_con_email']);
    if(!empty($wk_exist_unconfirm) && empty($cart['user_data']['email']))
    {
       $cart['edit_step']='step_one';
    }
  }
}

function fn_get_user_confirmation_data($user_id, $time) {
    $user_data = db_get_row('SELECT email, user_id, password, confirmation_code, can_resend, resend_time, expire_timestamp FROM ?:wk_user_registration WHERE user_id = ?i AND expire_timestamp > ?i AND can_resend = "Y"', $user_id, $time);
    return $user_data;
}

function fn_get_vendor_confirmation_data($company_id, $time) {
    $company_data = db_get_row('SELECT email, company_id, confirmation_code, can_resend, resend_time, expire_timestamp FROM ?:wk_vendor_registration WHERE company_id = ?i AND expire_timestamp > ?i AND can_resend = "Y"', $company_id, $time);
    return $company_data;
}

function fn_update_resend($resend_data, $user_id) {
    if($resend_data == 'C') {
        db_query('UPDATE ?:wk_user_registration SET can_resend = "N" WHERE user_id = ?i', $user_id);
    }else {
        db_query('UPDATE ?:wk_vendor_registration SET can_resend = "N" WHERE company_id = ?i', $user_id);
    }
    return true;
}
function fn_wk_registration_confirmation_get_users_post(&$users, $params, $auth) {
    if(!empty($users)) {
        foreach($users as $k => $user) {
            if($user['status'] == 'D') {
                $user_confirmation_data = fn_get_user_resend_data($user['user_id'], time());
                if(!empty($user_confirmation_data)) {
                    $users[$k]['can_resend_confirmation'] = 'Y';
                }
                else{
                    $users[$k]['can_resend_confirmation'] = 'Y';
                }
            }
        }
    }
}

function fn_get_user_resend_data($user_id, $time) {
    $user_data = db_get_row('SELECT email, user_id, password, confirmation_code, can_resend, resend_time, expire_timestamp FROM ?:wk_user_registration WHERE user_id = ?i AND expire_timestamp > ?i', $user_id, $time);
    return $user_data;
}

function fn_get_vendor_resend_data($company_id, $time) {
    $company_data = db_get_row('SELECT email, company_id, confirmation_code, can_resend, resend_time, expire_timestamp FROM ?:wk_vendor_registration WHERE company_id = ?i AND expire_timestamp > ?i', $company_id, $time);
    return $company_data;
}

