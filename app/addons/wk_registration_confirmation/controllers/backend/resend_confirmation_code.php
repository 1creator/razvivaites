<?php
/************************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Mailer;

if ($mode == 'customer') {
	if(isset($_REQUEST['user_id'])){
		$params['user_id'] = $_REQUEST['user_id'];
		list($users, $search) = fn_get_users($params, $auth, '');
		$user = $users[0];
		$user_confirmation_data = fn_get_user_resend_data($_REQUEST['user_id'], time());
		if(!empty($user_confirmation_data)) {
			$email_data['company_id'] = $user['company_id'];
            $email_data['email'] = $user['email'];
            $email_data['name'] = fn_get_user_name($user['user_id']);
            $email_data['days_countdown'] = round(($user_confirmation_data['expire_timestamp'] - time())/3600);
            $email_data['dor'] = date('d/m/Y',$user['timestamp']);
            $email_data['confirm_link'] = fn_url('index.php?dispatch=user_registration_varify.varify&confirmation='.$user_confirmation_data['confirmation_code'].'&email='.$user['email']);

            $from = 'company_users_department';

            if (fn_allowed_for('MULTIVENDOR')) {
                // Vendor administrator's notification
                // is sent from root users department
                if ($user['user_type'] == 'V') {
                    $from = 'default_company_users_department';
                }
            }
          
            $link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";
       
             $data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
			$mailer = Tygh::$app['mailer'];
            $mailer->send(array(
                        'to' => $user['email'],
                        'from' => $from,
                        'data' => array(
                            'data' => $data,
                        ),
                        'template_code' => 'confirmation_link_mail',
                        'tpl' => 'addons/wk_registration_confirmation/customer_confirmation.tpl',
                        'company_id' => $user['company_id']
                    ), fn_check_user_type_admin_area($user['user_type']) ? 'A' : 'C', Registry::get('settings.Appearance.backend_default_language'));

            $resend_data = 'C';
	        fn_update_resend($resend_data, $user['user_id']);
		}
		else{
			$pass = fn_generate_password(7);
			$confirmation_code = fn_generate_password(20);
			$seting_exp_time=Registry::get('addons.wk_registration_confirmation.cust_expire_time');
			$seting_resend_time = !empty(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !is_numeric(Registry::get('addons.wk_registration_confirmation.cust_resend_time')) || !(Registry::get('addons.wk_registration_confirmation.cust_resend_time') < 0) ?(3600 * Registry::get('addons.wk_registration_confirmation.cust_resend_time')):3600;

			if(empty($seting_exp_time) || !is_numeric($seting_exp_time) || ($seting_exp_time < 0))
				$exp_tym=6*3600;
			else
				$exp_tym=3600*$seting_exp_time;
			$data = array(
				'email' => $user['email'],
				'user_id' => $user['user_id'],
				'resend_time' => $seting_resend_time,
				'expire_timestamp' => time()+$exp_tym,
				'confirmation_code' => $confirmation_code,
				'password' => $pass,
			);

			db_query('INSERT INTO ?:wk_user_registration ?e',$data); 
			$update = array(
					'email' => $user['email'],
					'password1' => $pass,
					'password2' => $pass,
				);
			$a = fn_update_user($user['user_id'], $update, $auth, '', 0);

			$email_data['company_id'] = $user['company_id'];
			$email_data['email'] = $user['email'];
			$email_data['dor'] = date('d/m/Y',$user['timestamp']);
			$email_data['name'] = fn_get_user_name($user['user_id']);
			$email_data['confirm_link'] = fn_url('index.php?dispatch=user_registration_varify.varify&confirmation='.$confirmation_code.'&email='.$user['email']);
			$email_data['days_countdown'] = round(($data['expire_timestamp'] - time())/3600);

			$from = 'company_users_department';

			if (fn_allowed_for('MULTIVENDOR')) {
				// Vendor administrator's notification
				// is sent from root users department
				if ($user['user_type'] == 'V') {
			    $from = 'default_company_users_department';
				}
			}
		
			$link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";

			$data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
			$mailer = Tygh::$app['mailer'];
            $mailer->send(array(
			          'to' => $user['email'],
			          'from' => $from,
			          'data' => array(
			              'data' => $data,
			          ),
			          'template_code' => 'confirmation_link_mail',
			          'tpl' => 'addons/wk_registration_confirmation/customer_confirmation.tpl',
			          'company_id' => $user['company_id']
			      ), fn_check_user_type_admin_area($user['user_type']) ? 'A' : 'C', Registry::get('settings.Appearance.backend_default_language'));

		}
		fn_set_notification('N', __('notice'), __('confirmation_code_sent_customer'));
	}
	return array(CONTROLLER_STATUS_OK, 'profiles.manage&user_type=C');
}
else if($mode == 'vendor')	{
	if(isset($_REQUEST['company_id'])){
		$params['dispatch'] = 'companies.manage';
		$params['company_id'] = $_REQUEST['company_id'];
		list($companies, $search) = fn_get_companies($params, $auth, '');
		$company = $companies[0];
		$company_confirmation_data = fn_get_vendor_resend_data($company['company_id'], time());
		if(!empty($company_confirmation_data)) {
			$email_data['company_id'] = 0;
            $email_data['email'] = $company['email'];
            $email_data['name'] = fn_get_user_name($company['company']);
            $email_data['dor'] = date('d/m/Y',$company['timestamp']);
            $email_data['days_countdown'] = round(($company_confirmation_data['expire_timestamp'] - time())/3600);
            $email_data['confirm_link'] = fn_url('index.php?dispatch=user_registration_varify.vendor_varify&confirmation='.$company_confirmation_data['confirmation_code'].'&email='.$company['email']);

            $from = 'default_company_users_department';
           
            $link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";
			
            $wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
			$mailer = Tygh::$app['mailer'];
            $mailer->send(array(
                        'to' => $company['email'],
                        'from' => $from,
                        'data' => array(
                            'wk_data' => $wk_data,
                        ),
                        'template_code' => 'vendor_confirmation_link_mail',
                        'tpl' => 'addons/wk_registration_confirmation/vendor_confirmation.tpl',
                        'company_id' => $company['company_id']
                    ), 'C', Registry::get('settings.Appearance.backend_default_language'));

            $resend_data = 'V';
            fn_update_resend($resend_data, $company['company_id']);
        }
        else {
        	$confirmation_code = fn_generate_password(20);
			$seting_exp_time=Registry::get('addons.wk_registration_confirmation.vend_expire_time');
			$seting_resend_time = !empty(Registry::get('addons.wk_registration_confirmation.vend_resend_time')) || !is_numeric(Registry::get('addons.wk_registration_confirmation.vend_resend_time')) || !(Registry::get('addons.wk_registration_confirmation.vend_resend_time') < 0) ?(3600 * Registry::get('addons.wk_registration_confirmation.vend_resend_time')):3600;
			if(empty($seting_exp_time) || is_numeric($seting_exp_time) || $seting_exp_time < 0)
			{
				$exp_tym=3600;
			}else{
				$exp_tym=3600*$seting_exp_time;
			}
			$data = array(
				'email' => $company['email'],
				'resend_time' => $seting_resend_time,
				'company_id' => $company['company_id'],
				'expire_timestamp' => time()+$exp_tym,
				'confirmation_code' => $confirmation_code,
			);
			$_SESSION['wk_un_vend_email']=$company['email'];
			db_query('INSERT INTO ?:wk_vendor_registration ?e',$data);

			$email_data['company_id'] = 0;
			$email_data['email'] = $company['email'];
			if(isset($company['admin_firstname']) && isset($company['admin_lastname'])) {
				$email_data['name'] = $company['admin_firstname'].' '.$company['admin_lastname'];
			}else{
				$email_data['name'] = $company['company'];
			}
			$email_data['confirm_link'] = fn_url('index.php?dispatch=user_registration_varify.vendor_varify&confirmation='.$confirmation_code.'&email='.$company['email']);
			$email_data['dor'] = date('d/m/Y',$company['timestamp']);
			$email_data['days_countdown'] = round(($data['expire_timestamp'] - time())/3600);

			$from = 'default_company_users_department';
			$link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";
			$wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
			$mailer = Tygh::$app['mailer'];
            $mailer->send(array(
			        'to' => $company['email'],
			        'from' => $from,
			        'data' => array(
			            'wk_data' => $wk_data,
			        ),
			        'template_code' => 'vendor_confirmation_link_mail',
			        'tpl' => 'addons/wk_registration_confirmation/vendor_confirmation.tpl',
			        'company_id' => 0
			    ), 'C', Registry::get('settings.Appearance.backend_default_language'));
		}
		fn_set_notification('N', __('notice'), __('confirmation_code_sent_vendor'));
	}
	
	return array(CONTROLLER_STATUS_OK, 'companies.manage');
}
