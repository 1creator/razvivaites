<?php
/************************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($mode == 'varify') {
	if(isset($_REQUEST['confirmation']) && isset($_REQUEST['email']))
	{
		$confirmation_code=$_REQUEST['confirmation'];
		$email=$_REQUEST['email'];
		$user_id=db_get_field('SELECT user_id FROM ?:wk_user_registration WHERE confirmation_code = ?s AND email= ?s',$confirmation_code,$email);
		if(!empty($user_id))
		{	
			$updatedata = array('status' => 'A',);
        	db_query('UPDATE ?:users SET ?u WHERE user_id = ?i', $updatedata, $user_id);

        	$user_data=fn_get_user_info($user_id);
        	$password = db_get_field('SELECT password FROM ?:wk_user_registration WHERE user_id = ?i',$user_id);
        	
        	$from = 'company_users_department';

	        if (fn_allowed_for('MULTIVENDOR')) {
	            // Vendor administrator's notification
	            // is sent from root users department
	            if ($user_data['user_type'] == 'V') {
	                $from = 'default_company_users_department';
	            }
	        }
	        $forgot_pass_url = fn_url().'?dispatch=auth.recover_password';
	        $mailer = Tygh::$app['mailer'];
	        $mailer->send(array(
            'to' => $user_data['email'],
            'from' => $from,
            'data' => array(
                'user_data' => $user_data,
                'login_url' => fn_url(),
                'forgot_pass_url' => $forgot_pass_url
            	),
            'template_code' => 'create_profile',
            'tpl' => 'profiles/create_profile.tpl',
            'company_id' => $user_data['company_id']
        	), fn_check_user_type_admin_area($user_data['user_type']) ? 'A' : 'C', Registry::get('settings.Appearance.backend_default_language'));

        	db_query('DELETE FROM ?:wk_user_registration WHERE user_id = ?i' ,$user_id);

        	fn_set_notification('N',__('notice'),__('customer_varify_successfully'));
        	if(Registry::get('addons.wk_registration_confirmation.cust_login_setting') == 'Y'){
			fn_login_user($user_data['user_id']); }
			
			return array(CONTROLLER_STATUS_OK,'index.index');
		}else{
			
			return array(CONTROLLER_STATUS_DENIED);
		}
	}
}else if($mode == 'vendor_varify') {
	if(isset($_REQUEST['confirmation']) && isset($_REQUEST['email']))
	{
		$confirmation_code=$_REQUEST['confirmation'];
		$email=$_REQUEST['email'];
		$company_id=db_get_field('SELECT company_id FROM ?:wk_vendor_registration WHERE confirmation_code = ?s AND email= ?s',$confirmation_code,$email);
		if(!empty($company_id)) {
			$status = 'A';
			$status_from = '';
			fn_change_company_status($company_id, $status, '', $status_from, false, true);
			db_query('DELETE FROM ?:wk_vendor_registration WHERE company_id = ?i' ,$company_id);
			// db_query("UPDATE ?:users SET password_change_timestamp = 1 WHERE company_id = ?i", $company_id);
			unset($_SESSION['notifications']);
			$msg = Registry::get('view')->fetch('addons/wk_registration_confirmation/views/vendor_confirmation/vendor_confirmation.tpl');
			fn_set_notification('I', __('information'), $msg);
		}
		return array(CONTROLLER_STATUS_OK,'index.index');
	}
	return array(CONTROLLER_STATUS_OK,'index.index');
}