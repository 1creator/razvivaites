<?php
/************************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation     *
# ----------------------------------------------------------------------*
# author    webkul                                                      *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.         *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL               *
# Websites: http://webkul.com                                           *
*************************************************************************
*/

use Tygh\Registry;
use Tygh\Mailer;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode == 'cron') {
	if(Registry::get('addons.wk_registration_confirmation.vend_activate_setting') == 'Y') {
	$company_ids = db_get_array('SELECT company_id,confirmation_code,expire_timestamp FROM ?:wk_vendor_registration WHERE resend_time <= ?i AND can_resend = ?s AND expire_timestamp > ?i',TIME,'Y',TIME);
	if(!empty($company_ids)) {
		foreach($company_ids as $company) {
			$company_data = fn_get_company_data($company['company_id']);
			if(!empty(isset($company_data['company_id']))) {
				$email_data['company_id'] = 0;
		        $email_data['email'] = $company_data['email'];
		        $email_data['name'] = $company_data['company'];
		        $email_data['dor'] = date('d/m/Y',$company_data['timestamp']);
		        $email_data['days_countdown'] = round(($company['expire_timestamp'] - time())/(3600));
		        $email_data['confirm_link'] = fn_url('user_registration_varify.vendor_varify&confirmation='.$company['confirmation_code'].'&email='.$company_data['email']);

		        $from = 'default_company_users_department';

				$link_to_click = "<a href='$email_data[confirm_link]'>Click Here</a>";
		        
		        $wk_data = array(
				        'email' => $email_data['email'],
				        'registration_date' => $email_data['dor'],
				        'link_to_click' => $link_to_click,
				        'days_countdown' => $email_data['days_countdown']
				);
				$mailer = Tygh::$app['mailer'];
	            $mailer->send(array(
			                    'to' => $company_data['email'],
		                    'from' => $from,
		                    'data' => array(
		                        'wk_data' => $wk_data,
		                    ),
							'template_code' => 'vendor_confirmation_link_mail',
		                    'tpl' => 'addons/wk_registration_confirmation/vendor_confirmation.tpl',
		                    'company_id' => 0
		                ), 'C', Registry::get('settings.Appearance.backend_default_language'));

		        $resend_data = 'V';
		        fn_update_resend($resend_data, $company_data['company_id']);
			}
		}
	}}
}