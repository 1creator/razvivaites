<?php
/*********************************************************************
# Wk Registration Confirmation    ---  Wk Registration Confirmation  *
# -------------------------------------------------------------------*
# author    Webkul                                                   *
# copyright Copyright (C) 2010 webkul.com. All Rights Reserved.      *
# @license - http://www.gnu.org/licenses/gpl.html GNU/GPL            *
# Websites: http://webkul.com                                        *
**********************************************************************
*/

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
    'update_user_pre',
	'update_profile',
	'checkout_select_default_payment_method',
	'update_company',
	'get_users_post'
);

?>