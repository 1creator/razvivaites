<?php

defined('BOOTSTRAP') or die('Access denied');

$schema['controllers']['ec_booking_classes'] = array (
    'modes' => array(
        'manage' => array(
            'permissions' => true,
        ),
        'delete_child' => array(
            'permissions' => true,
        ),
        'details' => array(
            'permissions' => true,
        ),
        'show_child' => array(
            'permissions' => true,
        ),
    ),
    'permissions' => true,
);

$schema['controllers']['tools']['modes']['update_status']['param_permissions']['table']['ec_booking_reser_booking_info'] = true;

return $schema;