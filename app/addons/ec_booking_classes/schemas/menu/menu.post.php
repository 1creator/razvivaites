<?php
/**
 * CS-Cart Booking Classes - ec_booking_classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    Ecarter  <support@ecarter.co>
 * @copyright 2019 Ecarter 
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 */
use Tygh\Registry;

if (!Registry::get('runtime.company_id')) {

$schema['central']['orders']['items']['ec_booking_classes'] = array(
    'attrs'     =>array(
        'class' => 'is-addon'
    ),
    'href'      => 'ec_booking_classes.manage',
    'position'  => 100,
    'alt'       => 'ec_booking_classes.manage'
);

}

return $schema;