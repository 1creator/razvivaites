<?php
if (!defined('BOOTSTRAP')) {
    die('Access denied');
}
use Tygh\Enum\ProductTracking;
use Tygh\Mailer;
use Tygh\Registry;
use Tygh\Settings;

/**
 * Hook up1085e_product_pre used for pre Updation of product data
 *
 * @param mixed $product_data 
 * @param mixed $product_id   
 * @param mixed $lang_code 
 * @param mixed $can_update 
 *
 * @return void
 */
function Fn_ec_booking_classes_Update_Product_pre(&$product_data, &$product_id, $lang_code, $can_update)
{
    
    if (isset($product_data['booking_data']) && !empty($product_data['booking_data'])) {
        $product_data['is_edp']='N'; //change for no shipping
        if ($product_data['booking_data']['free_trail'] != 'N' || $product_data['booking_data']['book_type'] != 'N' || $product_data['booking_data']['discount_book'] != 'N') {
            $product_data['tracking'] = 'D';
            $product_data['is_edp']='Y'; //change for no shipping
        }
    }
}

function fn_ec_booking_classes_sms_notifications_info()
{
	$template_info = '';
	$template_info .= '<h5>New order template placeholders</h5>';
    $template_info .= '{book_start_date}: Starting book date <br>';
    $template_info .= '{book_end_date}: Ending book date <br>';
    $template_info .= '{prepayment_amount}: Prepayment amount <br>';
    $template_info .= '{surcharge_amount}: Surcharge Amount <br>';
    $template_info .= '{customer_fullname}: Customer Full Name <br>';
    $template_info .= '{product_name}: Customer Name <br>';
        
	return $template_info;
}

/**
 * Hook update_product_post is used for post Updation of product data
 *
 * @param mixed $product_data 
 * @param mixed $product_id 
 * @param mixed $lang_code 
 * @param mixed $create 
 * 
 * @return void
 */
function Fn_ec_booking_classes_Update_Product_post(&$product_data, &$product_id, &$lang_code, &$create)
{
    if (isset($product_data['booking_data']) && !empty($product_data['booking_data'])) {
        fn_ec_booking_classes_update_booking_data($product_data['booking_data'], $product_id);
    }
}


/**
 * Using join condition and field_list data to get product data
 *
 * @param mixed $product_id 
 * @param mixed $field_list 
 * @param mixed $join 
 * @param mixed $auth 
 *
 * @return void
 */
function Fn_ec_booking_classes_Get_Product_data(&$product_id, &$field_list, &$join, &$auth)
{
    $field_list .= ", ?:ec_booking_reser_classes.free_trail as free_trail";
    $field_list .= ", ?:ec_booking_reser_classes.book_type as book_type";
    $field_list .= ", ?:ec_booking_reser_classes.discount_book as discount_book";
    $field_list .= ", ?:ec_booking_reser_classes.date_from as date_from";
    $field_list .= ", ?:ec_booking_reser_classes.date_to as date_to	";
    $field_list .= ", ?:ec_booking_reser_classes.week_data as week_data";
    $join .= db_quote(" LEFT JOIN ?:ec_booking_reser_classes ON ?:ec_booking_reser_classes.product_id = ?:products.product_id AND ?:ec_booking_reser_classes.product_id = ?i", $product_id);
}

function fn_ec_booking_classes_get_products(&$params, &$fields, &$sortings, &$condition, &$join, &$sorting, $group_by, $lang_code, $having) 
{
    $fields['free_trail']= "?:ec_booking_reser_classes.free_trail as free_trail";
    $fields['book_types']= "?:ec_booking_reser_classes.book_type as book_types";
    $fields['discount_book']= "?:ec_booking_reser_classes.discount_book as discount_book";
    $fields['date_from']= "?:ec_booking_reser_classes.date_from as date_from";
    $fields['date_to']= "?:ec_booking_reser_classes.date_to as date_to";
    $fields['week_data']= "?:ec_booking_reser_classes.week_data as week_data";
    $join .= db_quote(" LEFT JOIN ?:ec_booking_reser_classes ON ?:ec_booking_reser_classes.product_id = products.product_id");
}

/**
 * Get product data
 *
 * @param mixed $product_id 
 * @param mixed $field_list 
 * @param mixed $join 
 * @param mixed $auth 
 *
 * @return void
 */
function Fn_ec_booking_classes_Get_My_Product_data(&$product_id, &$field_list, &$join, &$auth)
{
    $field_list .= ", ?:ec_booking_reser_classes.free_trail as free_trail";
    $field_list .= ", ?:ec_booking_reser_classes.book_type as book_type";
    $field_list .= ", ?:ec_booking_reser_classes.discount_book as discount_book";
    $field_list .= ", ?:ec_booking_reser_classes.date_from as date_from";
    $field_list .= ", ?:ec_booking_reser_classes.date_to as date_to	";
    $field_list .= ", ?:ec_booking_reser_classes.week_data as week_data";
    $join .= db_quote(" LEFT JOIN ?:ec_booking_reser_classes ON ?:ec_booking_reser_classes.product_id = ?:products.product_id AND ?:ec_booking_reser_classes.product_id = ?i", $product_id);
}

function fn_ec_booking_classes_tools_change_status(&$params, &$result)
{
    if($params['table'] == 'ec_booking_reser_booking_info' && $result == 1) {
        $booking_infos = db_get_row("SELECT * FROM ?:ec_booking_reser_booking_info WHERE id = ?i",$params['id']);
        if(!empty($booking_infos['order_id'])) {

        }
        if($params['status'] == 'A') {
            $booking_status = 'confirmed';
        }
        else {
            $booking_status = 'unconfirmed';
        }

        fn_ec_booking_classes_confirm_send_customer_email_notification($booking_infos,$booking_status);
        fn_ec_booking_classes_confirm_send_admin_email_notification($booking_infos,$booking_status);
        fn_ec_booking_classes_confirm_send_vendor_email_notification($booking_infos,$booking_status,$booking_info['product_id']);

    } 
}

/**
 * Update and Verify booking details
 *
 * @param mixed $booking_data 
 * @param mixed $product_id 
 *
 * @return void
 */
function fn_ec_booking_classes_update_booking_data($booking_data, $product_id)
{

    try{
        if (!isset($booking_data['free_trail']) || !isset($booking_data['book_type']) ||  !isset($booking_data['discount_book']) || !$product_id) {
            return false;
        }
    
        $param_data = array(
            'product_id'   => $product_id,
            'free_trail' => '',
            'book_type' => '',
            'discount_type' => '',
            'date_from'    => TIME,
            'date_to'      => TIME,
            'week_data'    => '',
        );
        $f = 0;

        $date_from = fn_parse_date($booking_data['date_from']);
        $date_to = fn_parse_date($booking_data['date_to']);
        $date_curr = strtotime('today midnight');

        if ($date_from < $date_curr || $date_from > $date_to) {
            fn_set_notification('E', __('error'), __('date_from_early'));
            return false;
        } 

        $booking_data = fn_ec_booking_classes_remove_extra_field($booking_data);
        $booking_data['week_data'] = serialize($booking_data['week_data']);
        $booking_data['date_from']  = isset($booking_data['date_from'])? fn_parse_date($booking_data['date_from']):'';
        $booking_data['date_to']    = isset($booking_data['date_to'])? fn_parse_date($booking_data['date_to']):'';
        $booking_data               = array_merge($param_data, $booking_data);
        $p_id                       = db_get_field("SELECT product_id FROM ?:ec_booking_reser_classes WHERE product_id = ?i", $product_id);
        if (empty($p_id)) {
            db_query('INSERT INTO ?:ec_booking_reser_classes ?e', $booking_data);
        } else {
            db_query('UPDATE ?:ec_booking_reser_classes SET ?u WHERE product_id = ?i', $booking_data, $product_id);

            if (db_get_field("SELECT company_id FROM ?:products WHERE product_id=?i", $p_id)==0) {
                $cm_product_ids=db_get_fields("SELECT product_id FROM ?:products WHERE parent_product_id=?i", $p_id);
                if (!empty($cm_product_ids)) {
                    $bk_data=$booking_data;
                    foreach ($cm_product_ids as $cm_key=>$cm_value) {
                        $bk_data['product_id']=$cm_value;
                        db_query('REPLACE INTO ?:ec_booking_reser_classes ?e', $bk_data);
                    }
                }
            }
        }
        return true;
    }
    catch(Exception $e){
        $message = $e->getMessage();
        fn_set_notification("E",__("error"),$message);
        return false;
    }
}

function fn_ec_booking_classes_remove_extra_field($booking_data) {

    $booking_data['week_data']['sunday']['single_start_time'] = array_filter($booking_data['week_data']['sunday']['single_start_time']);
    $booking_data['week_data']['sunday']['single_end_time'] = array_filter($booking_data['week_data']['sunday']['single_end_time']);
    $booking_data['week_data']['sunday']['book_seat'] = array_filter($booking_data['week_data']['sunday']['book_seat']);
    if(empty($booking_data['week_data']['sunday']['single_start_time'])){
        unset($booking_data['week_data']['sunday']['single_start_time']);
        unset($booking_data['week_data']['sunday']['single_end_time']);
        unset($booking_data['week_data']['sunday']['book_seat']);
    }

    $booking_data['week_data']['monday']['single_start_time'] = array_filter($booking_data['week_data']['monday']['single_start_time']);
    $booking_data['week_data']['monday']['single_end_time'] = array_filter($booking_data['week_data']['monday']['single_end_time']);
    $booking_data['week_data']['monday']['book_seat'] = array_filter($booking_data['week_data']['monday']['book_seat']);
    if(empty($booking_data['week_data']['monday']['single_start_time'])){
        unset($booking_data['week_data']['monday']['single_start_time']);
        unset($booking_data['week_data']['monday']['single_end_time']);
        unset($booking_data['week_data']['monday']['book_seat']);
    }

    $booking_data['week_data']['tuesday']['single_start_time'] = array_filter($booking_data['week_data']['tuesday']['single_start_time']);
    $booking_data['week_data']['tuesday']['single_end_time'] = array_filter($booking_data['week_data']['tuesday']['single_end_time']);
    $booking_data['week_data']['tuesday']['book_seat'] = array_filter($booking_data['week_data']['tuesday']['book_seat']);
    if(empty($booking_data['week_data']['tuesday']['single_start_time'])){
        unset($booking_data['week_data']['tuesday']['single_start_time']);
        unset($booking_data['week_data']['tuesday']['single_end_time']);
        unset($booking_data['week_data']['tuesday']['book_seat']);
    }

    $booking_data['week_data']['wednesday']['single_start_time'] = array_filter($booking_data['week_data']['wednesday']['single_start_time']);
    $booking_data['week_data']['wednesday']['single_end_time'] = array_filter($booking_data['week_data']['wednesday']['single_end_time']);
    $booking_data['week_data']['wednesday']['book_seat'] = array_filter($booking_data['week_data']['wednesday']['book_seat']);
    if(empty($booking_data['week_data']['wednesday']['single_start_time'])){
        unset($booking_data['week_data']['wednesday']['single_start_time']);
        unset($booking_data['week_data']['wednesday']['single_end_time']);
        unset($booking_data['week_data']['wednesday']['book_seat']);
    }

    $booking_data['week_data']['thursday']['single_start_time'] = array_filter($booking_data['week_data']['thursday']['single_start_time']);
    $booking_data['week_data']['thursday']['single_end_time'] = array_filter($booking_data['week_data']['thursday']['single_end_time']);
    $booking_data['week_data']['thursday']['book_seat'] = array_filter($booking_data['week_data']['thursday']['book_seat']);
    if(empty($booking_data['week_data']['thursday']['single_start_time'])){
        unset($booking_data['week_data']['thursday']['single_start_time']);
        unset($booking_data['week_data']['thursday']['single_end_time']);
        unset($booking_data['week_data']['thursday']['book_seat']);
    }

    $booking_data['week_data']['friday']['single_start_time'] = array_filter($booking_data['week_data']['friday']['single_start_time']);
    $booking_data['week_data']['friday']['single_end_time'] = array_filter($booking_data['week_data']['friday']['single_end_time']);
    $booking_data['week_data']['friday']['book_seat'] = array_filter($booking_data['week_data']['friday']['book_seat']);
    if(empty($booking_data['week_data']['friday']['single_start_time'])){
        unset($booking_data['week_data']['friday']['single_start_time']);
        unset($booking_data['week_data']['friday']['single_end_time']);
        unset($booking_data['week_data']['friday']['book_seat']);
    }

    $booking_data['week_data']['saturday']['single_start_time'] = array_filter($booking_data['week_data']['saturday']['single_start_time']);
    $booking_data['week_data']['saturday']['single_end_time'] = array_filter($booking_data['week_data']['saturday']['single_end_time']);
    $booking_data['week_data']['saturday']['book_seat'] = array_filter($booking_data['week_data']['saturday']['book_seat']);
    if(empty($booking_data['week_data']['saturday']['single_start_time'])){
        unset($booking_data['week_data']['saturday']['single_start_time']);
        unset($booking_data['week_data']['saturday']['single_end_time']);
        unset($booking_data['week_data']['saturday']['book_seat']);
    }
  
    return $booking_data;
}

function fn_ec_boking_classes_get_all_available_book_type($product_data) {
    $free_trail = $product_data['free_trail'];
    $discount_book = $product_data['discount_book'];  
    if(isset($product_data['book_types']))
        $book_type = $product_data['book_types'];
    else
        $book_type = $product_data['book_type'];
    $data = array();
    $datas = array();
    $first_element = '';
    if($free_trail == 'Y') {
        $data['free_trail'] = __("ec_free_trail");
        $first_element = 'free_trail';
    }
    if($discount_book == 'Y') {
        $data['discount_book'] = __("ec_discount_book");
        if(empty($first_element))
            $first_element = 'discount_book';
    }
    if($book_type == 'Y') {
        $data['book_type'] =__("ec_book_type");
        if(empty($first_element))
            $first_element = 'book_type';
    }
    $datas['type'] = $data;
    $datas['first_element'] = $first_element;
    return $datas;
}

function fn_ec_booking_classes_get_single_day_all_slots($selected_date, $product_id) {
    $product_data = fn_get_product_data($product_id, $_SESSION['auth']);
    $start_date                 = $product_data['date_from'];
    $end_date                   = $product_data['date_to'];
    $selected_date_timestamp    = strtotime($selected_date);
    $available_time_slots       = array();
    $unavailable_time_slots     = array();
    if ($start_date <= $selected_date_timestamp && $end_date >= $selected_date_timestamp) {
        $week_data          = unserialize($product_data['week_data']);
        $start_date_format  = date("Y-m-d", $start_date);
        $end_date_format    = date("Y-m-d", $end_date);
        $day                = date('D', strtotime($selected_date));
        list($val, $search) = Fn_ec_booking_classes_get_booked_info($product_id);
        $booking_info       = array();
        foreach ($val as $index => $booked_orders) {
            foreach ($booked_orders as $key => $booking_data) {
                if (isset($booking_data['booking_info']) && !empty($booking_data['booking_info'])) {
                    $time_slot      = explode(' to ', $booking_data['booking_info']['booking_slot']);
                    $booking_date = $booking_data['booking_info']['booking_date'];
                    list($month,$date,$year) = explode("/",$booking_date);
                    $booking_date_f = date("D", mktime(0, 0, 0, $month, $date, $year));
                    if($booking_date_f == $day) {
                        $booking_info[$index]['booking_slot'] = $time_slot;
                        $booking_info[$index]['no_booking_did'] = count($booking_data['child']['ec_child_name']);
                    }
                }
            }
        }
        if ($day == 'Sun') {
            if ($week_data['sunday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['sunday'], $booking_info);
            }
        } elseif ($day == 'Mon') {
            if ($week_data['monday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['monday'], $booking_info);
            }
        } elseif ($day == 'Tue') {
            if ($week_data['tuesday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['tuesday'], $booking_info);
            }
        } elseif ($day == 'Wed') {
            if ($week_data['wednesday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['wednesday'], $booking_info);
            }
        } elseif ($day == 'Thu') {
            if ($week_data['thursday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['thursday'], $booking_info);
            }
        } elseif ($day == 'Fri') {
            if ($week_data['friday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['friday'], $booking_info);
            }
        } elseif ($day == 'Sat') {
            if ($week_data['saturday']['status'] == 'open') {
                list($available_time_slots, $unavailable_time_slots) = fn__ec_booking_classes_time_slots_array($week_data['friday'], $booking_info);
            }
        }
        return array('available_time_slots' => $available_time_slots, 'unavailable_time_slots' => $unavailable_time_slots);
    }else {
        return false;
    }
    
}

function fn_ec_booking_classes_save_booking_info($product_child,$booking_info,$type,$product_id,$user_id) {
    $booking_infos['child'] = $product_child;
    $book_d = $booking_info;
    $book_d['booking_date'] = strtotime($booking_info['booking_date']);
    $booking_infos['booking_info'] = $booking_info;
    $booking_infos['product_id'] = $product_id;
    if (fn_ec_booking_classes_check_slot_if_already_booked($product_id, $book_d)) {
        $order_id = fn_ec_booking_classes_place_order($params,$product_id,$_SESSION['cart'],$_SESSION['auth']);

        $data = array(
            'order_id' => $order_id,
            'product_id' => $product_id,
            'booking_info' => serialize($booking_infos),
            'user_id'=>$user_id,
            'type'=>$type,
            'status' => 'D'
        );
        $booking_infos['product_name'] = fn_get_product_name($product_id);
        db_query("INSERT INTO ?:ec_booking_reser_booking_info ?e", $data);

        fn_ec_booking_classes_send_customer_email_notification($booking_infos,$user_id);
        fn_ec_booking_classes_send_admin_email_notification($booking_infos,$user_id);
        fn_ec_booking_classes_send_vendor_email_notification($booking_infos,$user_id,$product_id);

       
        $user_info = db_get_row("SELECT phone, firstname, lastname FROM ?:users WHERE user_id = ?i", $user_id);
        $booking_info['customer_fullname'] = $user_info['firstname'] .' '. $user_info['lastname'];
        $phone_number = Registry::get('addons.sms_notifications.phone_number');

        fn_ec_booking_classes_send_sms_notification_to_customer($product_id,$book_d['booking_date'],$type,$booking_infos,$user_info['phone']);
        fn_ec_booking_classes_send_sms_notification_to_admin($product_id,$book_d['booking_date'],$type,$booking_infos,$phone_number);
        fn_ec_booking_classes_send_sms_notification_to_vendor($product_id,$book_d['booking_date'],$type,$booking_infos);

        return true;
    }
}

function fn_ec_booking_classes_place_order($params,$product_id,&$cart,&$auth) {
    $buffer_cart = $cart;
    $buffer_auth = $auth;

    $cart = array(
        'products' => array(),
        'recalculate' => false,
        'payment_id' => 0, // skip payment
    );

    $product_data = '';

    $firstname = $params['name'];
    $lastname = '';
    $cart['user_data']['email'] = $params['email'];
    if (!empty($firstname) && strpos($firstname, ' ')) {
        list($firstname, $lastname) = explode(' ', $firstname);
    }
    $cart['user_data']['firstname'] = $firstname;
    $cart['user_data']['b_firstname'] = $firstname;
    $cart['user_data']['s_firstname'] = $firstname;
    $cart['user_data']['lastname'] = $lastname;
    $cart['user_data']['b_lastname'] = $lastname;
    $cart['user_data']['s_lastname'] = $lastname;
    $cart['user_data']['phone'] = $params['phone'];
    $cart['user_data']['b_phone'] = $params['phone'];
    $cart['user_data']['s_phone'] = $params['phone'];
    foreach (array('b_address', 's_address', 'b_city', 's_city', 'b_country', 's_country', 'b_state', 's_state') as $key) {
        if (!isset($cart['user_data'][$key])) {
            $cart['user_data'][$key] = ' ';
        }
    }

    if (empty($product_data[$product_id]['amount'])) {
        $product_data[$product_id] = array(
            'product_id' => $product_id,
            'amount' => 1,
        );
    }

    fn_add_product_to_cart($product_data, $cart, $auth);

    fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);

    $order_id = 0;
    if ($res = fn_place_order($cart, $auth)) {
        list($order_id) = $res;
    }

    // Restore cart
    $cart = $buffer_cart;
    $auth = $buffer_auth;

    return $order_id;
}

function fn_ec_booking_classes_cancel_order($request_data) {
   
    $booking_infos = db_get_row("SELECT * FROM ?:ec_booking_reser_booking_info WHERE id = ?i",$request_data['id']);
    if(!empty($booking_infos['order_id'])) {

    }
    $data = array(
        'status' => 'C'
    );
    $booking_status = 'canceled ';
    $booking_infos['product_name'] = fn_get_product_name($booking_infos['product_id']);
    $is_cancel = db_query("UPDATE ?:ec_booking_reser_booking_info SET ?u WHERE id = ?i", $data, $request_data['id']);
    fn_ec_booking_classes_confirm_send_customer_email_notification($booking_infos,$booking_status);
    fn_ec_booking_classes_confirm_send_admin_email_notification($booking_infos,$booking_status);
    fn_ec_booking_classes_confirm_send_vendor_email_notification($booking_infos,$booking_status,$booking_infos['product_id']);
    return true;



}

function fn_ec_booking_classes_confirm_send_customer_email_notification($booking_infos,$booking_status) {
    $user_data = fn_get_user_short_info($booking_infos['user_id']);
    $booking_info = unserialize($booking_infos['booking_info']);
    $booking_date = $booking_info['booking_info']['booking_date'];
    $booking_slot = $booking_info['booking_info']['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_info['child']['ec_child_name']);
    $company_data = fn_get_company_by_product_id($booking_infos['product_id']);
    $booking_child = $booking_info['child'];
    $email_data = array(
        'area' => 'C',
        'email' => !empty($user_data['email']) ? $user_data['email'] : '',
        'email_data' => array(
            'booking_status'=> $booking_status,
            'product_name' => $booking_infos['product_name'],
            'company_data' => $company_data,
            'booking_date' => $booking_date,
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_info
        ),
        'template_code' => 'ec_booking_classes.confirmed_notify_customer',
        'company_id' => $company_data['company_id']
    );
   
    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_confirm_send_admin_email_notification($booking_infos,$booking_status) {
    $booking_info = unserialize($booking_infos['booking_info']);
    $company_data = fn_get_company_by_product_id($booking_infos['product_id']);
    $booking_date = $booking_info['booking_info']['booking_date'];
    $booking_slot = $booking_info['booking_info']['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_info['child']['ec_child_name']);
    $booking_child = $booking_info['child'];
    $email_data = array(
        'area' => 'A',
        'email' => 'default_company_orders_department',
        'email_data' => array(
            'customer_name' => fn_get_user_name($booking_infos['user_id']),
            'booking_status'=> $booking_status,
            'product_name' => $booking_infos['product_name'],
            'company_data' => $company_data,
            'booking_date' => $booking_date,
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_child,
        ),
        'template_code' => 'ec_booking_classes__confirmed_notify_admin',
        'company_id' => $company_data['company_id']
    );
   
    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_confirm_send_vendor_email_notification($booking_infos,$booking_status,$product_id) {
    $company_data = fn_get_company_by_product_id($product_id);
    $booking_info = unserialize($booking_infos['booking_info']);
    $booking_date = $booking_info['booking_info']['booking_date'];
    $booking_slot = $booking_info['booking_info']['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_info['child']['ec_child_name']);
    $booking_child = $booking_info['child'];
    $email_data = array(
        'area' => 'A',
        'email' => $company_data['email'],
        'email_data' => array(
            'customer_name' => fn_get_user_name($booking_infos['user_id']),
            'booking_status'=> $booking_status,
            'product_name' => $booking_infos['product_name'],
            'company_data' => $company_data,
            'booking_date' => $booking_date,
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_child
        ),
        'template_code' => 'ec_booking_classes__confirmed_notify_admin',
        'company_id' => $company_data['company_id']

    );
   
    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_send_customer_email_notification($booking_infos,$user_id)
{
    $user_data = fn_get_user_short_info($user_id);
    $booking_info = $booking_infos['booking_info'];
    $booking_date = $booking_info['booking_date'];
    $booking_slot = $booking_info['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_infos['child']['ec_child_name']);
    $booking_child = $booking_infos['child'];
    $company_data = fn_get_company_by_product_id($booking_infos['product_id']);
    if(isset($booking_infos['extra_info'])){
        $extra_info = $booking_infos['extra_info'];
    }
    else 
        $extra_info = array();
    
    $extra_info['company_name'] = $company_data['company'];
    $extra_info['company_phone'] = $company_data['phone'];
    $extra_info['company_address'] = $company_data['address'];
    $extra_info['product_name'] = fn_get_product_name($booking_infos['product_id']);
    $email_data = array(
        'area' => 'C',
        'email' => !empty($user_data['email']) ? $user_data['email'] : '',
        'email_data' => array(
            'booking_date' => $booking_date,
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_child,
            'product_name' => $booking_infos['product_name'],
            'customer_name' => $user_data['firstname'] .' '. $user_data['lastname'],
            'company_data' => $company_data,
            'extra_info' => $extra_info,
        ),
        'template_code' => 'ec_booking_classes.notify_customer',
        'company_id' => $company_data['company_id']
    );

    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_send_admin_email_notification($booking_infos,$user_id)
{
    $booking_info = $booking_infos['booking_info'];
    $booking_date = $booking_info['booking_date'];
    $booking_slot = $booking_info['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_infos['child']['ec_child_name']);
    $company_data = fn_get_company_by_product_id($booking_infos['product_id']);
    $booking_child = $booking_infos['child'];
    $email_data = array(
        'area' => 'A',
        'email' => 'default_company_orders_department',
        'email_data' => array(
            'booking_date' => $booking_date,
            'product_name' => $booking_infos['product_name'],
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_child,
            'company_data' => $company_data,
        ),
        'template_code' => 'ec_booking_classes_notify_admin',
        'company_id' => $company_data['company_id']
    );

    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_send_vendor_email_notification($booking_infos,$user_id,$product_id)
{
    $company_data = fn_get_company_by_product_id($product_id);
    $booking_info = $booking_infos['booking_info'];
    $booking_date = $booking_info['booking_date'];
    $booking_slot = $booking_info['booking_slot'];
    $booking_slot = str_replace('to','по',$booking_slot);
    $total_child = count($booking_infos['child']['ec_child_name']);
    $booking_child = $booking_infos['child'];
    $email_data = array(
        'area' => 'A',
        'email' => $company_data['email'],
        'email_data' => array(
            'booking_date' => $booking_date,
            'booking_slot' => $booking_slot,
            'total_child' => $total_child,
            'booking_child' => $booking_child,
            'product_name' => $booking_infos['product_name'],
            'company_data' => $company_data
        ),
        'template_code' => 'ec_booking_classes_notify_admin',
        'company_id' => $company_data['company_id'],
    );

    $result = fn_ec_booking_classes_send_email_notification($email_data);
    return $result;
}

function fn_ec_booking_classes_send_email_notification(array $email_data)
{
    $result = false;
    if (!empty($email_data['email'])
        && !empty($email_data['template_code'])
        && !empty($email_data['area'])
    ) {

          fn_print_R($email_data);
        /** @var \Tygh\Mailer\Mailer $mailer */
        $mailer = Tygh::$app['mailer'];

        $result = (bool) $mailer->send(array(
            'to' => $email_data['email'],
            'from' => 'default_company_users_department',
            'data' => !empty($email_data['email_data']) ? $email_data['email_data'] : array(),
            'template_code' => $email_data['template_code'],
            'company_id' =>$email_data['company_id'],
        ), $email_data['area']);
    }

    return $result;
}

function fn__ec_booking_classes_time_slots_array($week_days_slot,$boking_info) {
    $booked_orders = array();
    $k=0;
    $l = 0;
    if(!empty($boking_info)) {
        foreach($boking_info as $key => $item) {
            if(empty($booked_orders)) {
                $booked_orders[$k]['booking_slot'] = $item['booking_slot'];
                $booked_orders[$k]['no_booking_did'] = $item['no_booking_did'];
                $k++;
            }
            else {
                foreach($booked_orders as $ke => $ite) {
                    if($ite['booking_slot'][0] == $item['booking_slot'][0] && $ite['booking_slot'][1] == $item['booking_slot'][1]) {
                        $booked_orders[$ke]['no_booking_did'] += $item['no_booking_did'];
                        $l = 1;
                    }
                }
                if($l == 0) {
                    $booked_orders[$k]['booking_slot'] = $item['booking_slot'];
                    $booked_orders[$k]['no_booking_did'] = $item['no_booking_did'];
                    $k++;
                }
            }
        }
    }
    $class_slot = array();
    $cl = 0;
    $unavailable_time_slots = array();
    $un = 0;
    if(isset($week_days_slot['single_start_time'])) {
        foreach($week_days_slot['single_start_time'] as $key => $item) {
            if(!empty($booked_orders)) {
                $k = 0;
                foreach($booked_orders as $ke => $ite) {
                    if($ite['booking_slot'][0] == $item && $ite['booking_slot'][1] == $week_days_slot['single_end_time'][$key] && $ite['no_booking_did'] >= $week_days_slot['book_seat'][$key]) {
                        $k = 1;
                    }
                }
                if($k == 1) {
                    $unavailable_time_slots[$un]['0'] = $item;
                    $unavailable_time_slots[$un]['1'] = $week_days_slot['single_end_time'][$key];
                    $unavailable_time_slots[$un]['2'] = $week_days_slot['book_seat'][$key];
                    $un++;
                }
                else {
                    $class_slot[$cl]['0'] = $item;
                    $class_slot[$cl]['1'] = $week_days_slot['single_end_time'][$key];
                    $class_slot[$cl]['2'] = $week_days_slot['book_seat'][$key];
                    $cl++;
                }
            }
            else {
                $class_slot[$cl]['0'] = $item;
                $class_slot[$cl]['1'] = $week_days_slot['single_end_time'][$key];
                $class_slot[$cl]['2'] = $week_days_slot['book_seat'][$key];
                $cl++;
            }
            
        }
    }
      return array($class_slot,$unavailable_time_slots);
}

/**
 * Get booking info of a booking product
 *
 * @param mixed $product_id 
 * @param mixed $status 
 * @param mixed $params 
 *
 * @return array $all_booking_info,$params
 */
function Fn_ec_booking_classes_Get_Booked_info($product_id = 0, $status = 'A', &$params = array())
{
    $default_params = array (
        'page'              => 1,
        'items_per_page'    => Registry::get('settings.Appearance.elements_per_page')
    );
    $params     = array_merge($default_params, $params);
    $condition  = '';

    if ($status == 'A') {
        $condition = db_quote(' AND status = ?s', 'A');
    }
    $limit          = '';
    $booking_data   = array();
    if ($product_id) {
        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info WHERE product_id = ?i $condition", $product_id);
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
            $params['limit'] = $limit;
        }
        $booking_data = db_get_array("SELECT * FROM ?:ec_booking_reser_booking_info WHERE product_id = ?i", $product_id);
    } else {
        if (!empty($params['items_per_page'])) {
            $params['total_items']  = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info WHERE 1 $condition");
            $limit                  = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
            $params['limit']        = $limit;
        }
        $booking_data = db_get_array("SELECT * FROM ?:ec_booking_reser_booking_info WHERE 1 $condition $limit");
    }
    $all_booking_info = array();
    foreach ($booking_data as $in_dex => $b_data) {
        $booking_info = $b_data;
        $booking_info['booking_info'] = unserialize($b_data['booking_info']);
        $all_booking_info[] = $booking_info;
    }
    return array($all_booking_info, $params);
}

/**
 * Get booking information of a product
 *
 * @param mixed $product_id 
 * @param mixed $params 
 *
 * @return array $booking_info,$params
 */
function Fn_ec_booking_classes_Get_Booked_information($product_id = 0, &$params = array())
{
    $default_params = array (
        'page'              => 1,
        'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
    );

    $limit  = '';
    $join   = $condition = '';
    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array (
        'order_id'      => "orders.order_id",
        'id'            => "ec_booking_reser_booking_info.id",
        'user_id'            => "ec_booking_reser_booking_info.user_id",
        'booking_info'  => "ec_booking_reser_booking_info.booking_info",
        'status'        => "ec_booking_reser_booking_info.status"
    );
    $sortings = array(
        'order_id'  => "orders.order_id",
        'status'        => "ec_booking_reser_booking_info.status"
    );
    
    $join .= "LEFT JOIN ?:orders as orders ON orders.order_id = ec_booking_reser_booking_info.order_id ";   
    $join .= "LEFT JOIN ?:products as products ON products.product_id = ec_booking_reser_booking_info.product_id ";    
    $join .= "LEFT JOIN ?:ec_booking_reser_classes as ec_booking_reser_classes ON ec_booking_reser_classes.product_id = ec_booking_reser_booking_info.product_id "; 
    
    // if ($status == 'A') {
    //     $condition .= db_quote('AND status = ?s', 'A');
    // }

    if (Registry::get('runtime.company_id')) {
        $condition .= db_quote('AND products.company_id = ?i', Registry::get('runtime.company_id'));
    }

        
    if (empty($params['sort_order'])) {
        $params['sort_order'] = 'desc';
    }
    if (empty($params['sort_by'])) {
        $params['sort_by'] = 'order_id';
    }
    if (isset($params['email']) && fn_string_not_empty($params['email'])) {
        $condition .= db_quote(" AND orders.email LIKE ?l", "%" . trim($params['email']) . "%");
    }
    if (!empty($params['status'])) {
        $condition .= db_quote(" AND ec_booking_reser_booking_info.status IN (?a)", $params['status']);
        
    } 
    if (isset($params['order_id']) && !empty($params['order_id'])) {
        $condition.= db_quote(' AND orders.order_id IN (?a)', $params['order_id']);
    }
    if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $arr = fn_explode(' ', $params['name']);
        foreach ($arr as $k => $v) {
            if (!fn_string_not_empty($v)) {
                unset($arr[$k]);
            }
        }
        if (sizeof($arr) == 2) {
            $condition.= db_quote(" AND orders.firstname LIKE ?l AND orders.lastname LIKE ?l",  "%" . array_shift($arr) . "%", "%" . array_shift($arr) . "%");
        } else {
            $condition.= db_quote(" AND (orders.firstname LIKE ?l OR orders.lastname LIKE ?l)", "%" . trim($params['name']) . "%", "%" . trim($params['name']) . "%");
        }
    }
    if (isset($params['order_id']) && !empty($params['order_id'])) {
        $condition.= db_quote(' AND orders.order_id IN (?a)', $params['order_id']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'desc');
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info AS ec_booking_reser_booking_info $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        $params['limit'] = $limit;
    }


    $booking_info = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:ec_booking_reser_booking_info as ec_booking_reser_booking_info $join WHERE 1 $condition $sorting $limit");

    foreach ($booking_info as $in_dex => $b_data) {
        $booking_info[$in_dex]['booking_info'] = unserialize($b_data['booking_info']);

    }
    return array($booking_info, $params);
}

function fn_ec_booking_classes_get_booked_data_child_info($params = array(),$user_id = 0) {
    $default_params = array (
        'page'              => 1,
        'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
    );

    $limit  = '';
    $join   = $condition = '';
    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array (
        'id'   => "ec_booking_save_child_info.id",
        'child_birthdate'   => "ec_booking_save_child_info.child_birthdate",
        'child_name'   => "ec_booking_save_child_info.child_name",
    );
    $sortings = array(
        'child_birthdate'   => "ec_booking_save_child_info.child_birthdate",
        'child_name'   => "ec_booking_save_child_info.child_name",
    );
        
    if (empty($params['sort_order'])) {
        $params['sort_order'] = 'desc';
    }
    if (empty($params['sort_by'])) {
        $params['sort_by'] = 'order_id';
    }
    if($user_id)
        $condition.= db_quote(' AND ec_booking_save_child_info.user_id =?i', $user_id);

    $sorting = db_sort($params, $sortings, 'child_birthdate', 'desc');
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_save_child_info AS ec_booking_save_child_info $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        $params['limit'] = $limit;
    }


    $child_info = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:ec_booking_save_child_info as ec_booking_save_child_info $join WHERE 1 $condition $sorting $limit");

    return array($child_info, $params);
}

function fn_ec_booking_classes_get_booked_data_child_info_complete($user_id) {

    // Define fields that should be retrieved
    $fields = array (
        'id'   => "ec_booking_save_child_info.id",
        'child_name'   => "ec_booking_save_child_info.child_name",
    );
     
    $condition = '';
    $condition.= db_quote(' AND ec_booking_save_child_info.user_id =?i', $user_id);

   
    $child_info = db_get_array("SELECT * FROM ?:ec_booking_save_child_info WHERE user_id = ?i",$user_id);
    return $child_info;
}

function fn_ec_booking_classes_get_child_saved_info($request_data,$user_id) {
    try{
        $child_info = $request_data['ec_child_name'];
        $birthdate = $request_data['ec_child_birthdate'];
        // $phone_number = $request_data['ec_phone_number'];
        // $email = $request_data['ec_email'];
        $data_info = array();
        foreach($child_info as $key => $item) {
            $data_info[$key]['child_name'] = $item;
            $data_info[$key]['child_birthdate'] = $birthdate[$key];
            $data_info[$key]['user_id'] = $user_id;
        }
        db_query("INSERT INTO ?:ec_booking_save_child_info ?m", $data_info);
        return true;
    }
    catch(Exception $e) {
        fn_set_notification("E",__("error"),$e->getMessage());
        return false;
    }
   
}

function fn_ec_booking_classes_delete_child_data($id,$user_id) {
    try{
        db_query("DELETE FROM ?:ec_booking_save_child_info WHERE id = ?i", $id);
        return true;
    }
    catch(Exception $e) {
        return false;
    }
}

/**
 * Get booking info of a booking product
 *
 * @param mixed $params 
 *
 * @return array $all_booking_info,$params
 */
function fn_ec_booking_classes_get_booked_data(&$params = array())
{
    $condition  = '';
    $limit          = '';
    $booking_data   = array();
    $condition  = '';

    $condition = db_quote(' AND id = ?i', $params['id']);

    if (!empty($params['items_per_page'])) {
        $params['total_items']  = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info WHERE 1 $condition");
        $limit                  = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        $params['limit']        = $limit;
    }
    $booking_data = db_get_array("SELECT * FROM ?:ec_booking_reser_booking_info WHERE 1 $condition $limit");
    $all_booking_info = array();
    foreach ($booking_data as $in_dex => $b_data) {
        $booking_info = $b_data;
        $booking_info['booking_info'] = unserialize($b_data['booking_info']);
         $all_booking_info = $booking_info;
    }
    return array($all_booking_info, $params);
}

/**
 * Get booking info of a booking product
 *
 * @param mixed $params 
 *
 * @return array $all_booking_info,$params
 */
function fn_ec_booking_classes_get_booked_data_by_user(&$params = array())
{
    $condition  = '';
    $limit          = '';
    $booking_data   = array();
    $condition  = '';

    $condition .= db_quote(' AND id = ?i', $params['id']);
    $condition .= db_quote(' AND user_id = ?i', $params['user_id']);

    if (!empty($params['items_per_page'])) {
        $params['total_items']  = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info WHERE 1 $condition");
        $limit                  = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        $params['limit']        = $limit;
    }
    $booking_data = db_get_array("SELECT * FROM ?:ec_booking_reser_booking_info WHERE 1 $condition $limit");
    $all_booking_info = array();
    foreach ($booking_data as $in_dex => $b_data) {
        $booking_info = $b_data;
        $booking_info['booking_info'] = unserialize($b_data['booking_info']);
         $all_booking_info = $booking_info;
    }
    return array($all_booking_info, $params);
}

/**
 * Get booking information of a product
 *
 * @param mixed $params 
 *
 * @return array $booking_info,$params
 */
function fn_ec_booking_classes_get_booked_datas(&$params = array())
{
    $default_params = array (
        'page'              => 1,
        'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
    );

    $limit  = '';
    $join   = $condition = '';
    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array (
        'order_id'      => "orders.order_id",
        'id'            => "ec_booking_reser_booking_info.id",
        //'product_id'            => "ec_booking_reser_booking_info.product_id",
        'product_name'            => "product_descriptions.product",
        'product_price'            => "product_prices.price",
        'store_name'            => "store_location_descriptions.name",
        'user_id'            => "ec_booking_reser_booking_info.user_id",
        'booking_info'  => "ec_booking_reser_booking_info.booking_info",
        'status'        => "ec_booking_reser_booking_info.status"
    );
    $sortings = array(
        'order_id'  => "orders.order_id",
        'status'        => "ec_booking_reser_booking_info.status"
    );
    
    $join .= "LEFT JOIN ?:orders as orders ON orders.order_id = ec_booking_reser_booking_info.order_id ";    
    $join .= "JOIN ?:product_descriptions as product_descriptions ON product_descriptions.product_id = ec_booking_reser_booking_info.product_id ";    
    $join .= "JOIN ?:products as products ON products.product_id = ec_booking_reser_booking_info.product_id ";    
    $join .= "JOIN ?:product_prices as product_prices ON product_prices.product_id = products.product_id ";    
    $join .= "JOIN ?:store_location_descriptions as store_location_descriptions ON store_location_descriptions.store_location_id = products.ec_store_location_id ";    
    $join .= "LEFT JOIN ?:ec_booking_reser_classes as ec_booking_reser_classes ON ec_booking_reser_classes.product_id = ec_booking_reser_booking_info.product_id "; 
    
    // if ($status == 'A') {
    //     $condition .= db_quote('AND status = ?s', 'A');
    // }

    if (Registry::get('runtime.company_id')) {
        $condition .= db_quote(' AND orders.company_id = ?i', Registry::get('runtime.company_id'));
    }

    // $condition .= db_quote(' AND ec_booking_reser_booking_info.user_id = ?i', $params['user_id']);
        
    if (empty($params['sort_order'])) {
        $params['sort_order'] = 'desc';
    }
    if (empty($params['sort_by'])) {
        $params['sort_by'] = 'order_id';
    }
    if (isset($params['email']) && fn_string_not_empty($params['email'])) {
        $condition .= db_quote(" AND orders.email LIKE ?l", "%" . trim($params['email']) . "%");
    }
    if (!empty($params['status'])) {
        $condition .= db_quote(" AND ec_booking_reser_booking_info.status IN (?a)", $params['status']);
        
    } 
    if (isset($params['order_id']) && !empty($params['order_id'])) {
        $condition.= db_quote(' AND orders.order_id IN (?a)', $params['order_id']);
    }
    if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $arr = fn_explode(' ', $params['name']);
        foreach ($arr as $k => $v) {
            if (!fn_string_not_empty($v)) {
                unset($arr[$k]);
            }
        }
        if (sizeof($arr) == 2) {
            $condition.= db_quote(" AND orders.firstname LIKE ?l AND orders.lastname LIKE ?l",  "%" . array_shift($arr) . "%", "%" . array_shift($arr) . "%");
        } else {
            $condition.= db_quote(" AND (orders.firstname LIKE ?l OR orders.lastname LIKE ?l)", "%" . trim($params['name']) . "%", "%" . trim($params['name']) . "%");
        }
    }
    if (isset($params['order_id']) && !empty($params['order_id'])) {
        $condition.= db_quote(' AND orders.order_id IN (?a)', $params['order_id']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'desc');
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:ec_booking_reser_booking_info AS ec_booking_reser_booking_info $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        $params['limit'] = $limit;
    }

    $condition.= db_quote(' AND product_descriptions.lang_code = ?s', CART_LANGUAGE);
    $condition.= db_quote(' AND store_location_descriptions.lang_code = ?s', CART_LANGUAGE);
    $booking_info = db_get_array('SELECT ' . implode(', ', $fields) . " FROM ?:ec_booking_reser_booking_info as ec_booking_reser_booking_info $join WHERE 1 $condition $sorting $limit");

    foreach ($booking_info as $in_dex => $b_data) {
        $booking_info[$in_dex]['booking_info'] = unserialize($b_data['booking_info']);
    }

    return array($booking_info, $params);
}

/**
 * Product data updation before adding product to cart
 *
 * @param mixed $product_data 
 * @param mixed $cart 
 * @param mixed $auth 
 * @param mixed $update 
 *
 * @return void
 */
function Fn_ec_booking_classes_Pre_Add_To_cart(&$product_data, &$cart, &$auth, &$update)
{
    if (defined('ORDER_MANAGEMENT') && Registry::get('runtime.mode') == 'add') {
        foreach ($product_data as $product_id => $single_product_data) {
            if (Fn_ec_booking_classes_Check_If_booking_product($product_id)) {
                unset($product_data[$product_id]);
                fn_set_notification('W', __('warning'), __('booking_product_not_allowed_in_cart_from_admin'));
            }
        }
    }
    if ($update == true) {
        foreach ($product_data as $key => $data) {
            $product_id = $data['product_id'];
            if (fn_booking_and_reservation_check_if_booking_product($product_id)) {
                if (isset($cart['products'][$key]['extra']['booking_info']) && !empty($cart['products'][$key]['extra']['booking_info'])) {
                    fn_define('ORDER_MANAGEMENT', true);
                    $product_data[$key]['extra']['booking_info']['booking_slot'] = $product_data[$key]['booking_info']['booking_slot'];
                    $product_data[$key]['extra']['booking_info']['product_child'] = $_REQUEST['product_child'];
                    $_REQUEST['product_child']['ec_child_name'] = array_filter($_REQUEST['product_child']['ec_child_name']);
                    // $_REQUEST['product_child']['ec_phone_number'] = array_filter($_REQUEST['product_child']['ec_phone_number']);
                    // $_REQUEST['product_child']['ec_email'] = array_filter($_REQUEST['product_child']['ec_email']);
                    $number_amount = count($_REQUEST['product_child']['ec_child_name']);
                    $product_data[$key]['extra']['booking_info']['booking_date'] = strtotime($product_data[$key]['booking_info']['booking_date']);
                    $price =fn_get_product_price($product_data[$key]['product_id'],1,$auth);
                    $product_data[$key]['price'] = $price * 0.1 * $number_amount;
                    $product_data[$key]['stored_price'] = 'Y';
                }
                else {
                    fn_set_notification('W', __('warning'), __('no_booking_info_please_choose_booking'));
                    $product_data[$key]['amount'] = 0;
                }
            }
        }
    }
    else {
        foreach ($product_data as $key => $value) {
            if (isset($product_data[$key]['booking_info'])) {
                fn_define('ORDER_MANAGEMENT', true);
                $product_data[$key]['extra']['booking_info']['booking_slot'] = $product_data[$key]['booking_info']['booking_slot'];
                $product_data[$key]['extra']['booking_info']['product_child'] = $_REQUEST['product_child'];
                $_REQUEST['product_child']['ec_child_name'] = array_filter($_REQUEST['product_child']['ec_child_name']);
                // $_REQUEST['product_child']['ec_phone_number'] = array_filter($_REQUEST['product_child']['ec_phone_number']);
                // $_REQUEST['product_child']['ec_email'] = array_filter($_REQUEST['product_child']['ec_email']);
                $number_amount = count($_REQUEST['product_child']['ec_child_name']);
                $product_data[$key]['extra']['booking_info']['booking_date'] = strtotime($product_data[$key]['booking_info']['booking_date']);
                $price =fn_get_product_price($product_data[$key]['product_id'],1,$auth);
                $product_data[$key]['price'] = $price * 0.1 * $number_amount;
                $product_data[$key]['stored_price'] = 'Y';
               
                if (!fn_ec_booking_classes_check_slot_if_already_booked($product_data[$key]['product_id'],$product_data[$key]['extra']['booking_info'])) {
                    fn_set_notification('W', __('warning'), __('booking_already_reserved_or_expired'));
                    unset($product_data[$key]);
                    $product_data[$key]['amount'] = 0;
                    if (isset($cart['products'][$key])) {
                        unset($cart['products'][$key]);
                    }
                    continue;
                    unset($product_data[$key]['booking_info']);
                }
            }
            
        }
    }
}

function fn_ec_booking_classes_get_status() {
    $status = array();
    $status['D'] = __("ec_booking_unconfirmed");
    $status['A'] = __("ec_booking_confirmed");
    $status['C'] = __("ec_booking_cancelled");
    return $status;
}

function fn_ec_booking_classes_get_close_day($product_data) {
    // fn_print_R($product_id);
    // die;
    // $product_data = fn_get_product_data($product_id,$_SESSION['auth']);
    $week_day = unserialize($product_data['week_data']);
    $close_day = array();
    if($week_day['sunday']['status'] == 'close')
        $close_day[] = 0;
    if($week_day['monday']['status'] == 'close')
        $close_day[] = 1;
    if($week_day['tuesday']['status'] == 'close')
        $close_day[] = 2;
    if($week_day['wednesday']['status'] == 'close')
        $close_day[] = 3;
    if($week_day['thursday']['status'] == 'close')
        $close_day[] = 4;
    if($week_day['friday']['status'] == 'close')
        $close_day[] = 5;
    if($week_day['saturday']['status'] == 'close')
        $close_day[] = 6;
    
    return json_encode($close_day);
    
}

function fn_ec_booking_classes_check_slot_if_already_booked($product_id,$booking_info) {
        $selected_date = $booking_info['booking_date'];
        $selected_date_format = fn_date_format($selected_date,"%Y-%m-%d");
        $all_slots = fn_ec_booking_classes_get_single_day_all_slots($selected_date_format,$product_id);
        if (isset($all_slots['available_time_slots']))
        foreach ($all_slots['available_time_slots'] as $_key => $time_slots) {
            unset($time_slots[2]);
            $time_slot = implode(' to ', $time_slots);
            if ($booking_info['booking_slot'] == $time_slot) {
                return true;
            }
        }
        return false;
}

/**
 * Check if product is a booking product
 *
 * @param mixed $product_id 
 *
 * @return boolean
 */
function Fn_ec_booking_classes_Check_If_Booking_product($product_id)
{
    $booking_id = db_get_field("SELECT count(*) FROM ?:ec_booking_reser_classes WHERE  product_id = ?i", $product_id);
    if ($booking_id) {
        return true;
    }
    return false;
}

/**
 * Update order details
 *
 * @param mixed $order_id 
 * @param mixed $force_notification 
 * @param mixed $order_info 
 * @param mixed $_error 
 *
 * @return void
 */
function Fn_ec_booking_classes_Order_Placement_routines(&$order_id, &$force_notification, &$order_info, &$_error)
{
    $cart = fn_get_order_info($order_id);
    $user_id = $_SESSION['auth']['user_id'];
    if (isset($cart['product_groups'])) {
        foreach ($cart['product_groups'] as $index => $company_data) {
            foreach ($company_data['products'] as $hash => $order_product) {
                if (isset($order_product['extra']['booking_info']) && !empty($order_product['extra']['booking_info'])) {
                    if (!fn_ec_booking_classes_check_slot_if_already_booked($order_product['product_id'], $order_product['extra']['booking_info'])) {
                        fn_set_notification('W', __('warning'), __('booking_already_reserved_or_expired'));
                        $input_data = array(
                            'order_id' => $order_id,
                            'product_id' => $order_product['product_id'],
                            'booking_info' => '',
                            'status' => 'D'
                            );
                    } else {
                        $input_data = array(
                            'order_id' => $order_id,
                            'product_id' => $order_product['product_id'],
                            'booking_info' => '',
                            'status' => 'D',
                            'user_id'=>$user_id,
                            'type'=>'discount_book',
                        );
                    }
                    $booking_infos = array();
                    $type = 'discount_book';
                    $booking_infos['child'] = $order_product['extra']['booking_info']['product_child'];
                    $booking_date = $order_product['extra']['booking_info']['booking_date'];
                    $order_product['extra']['booking_info']['booking_date'] = date('m/d/y',$order_product['extra']['booking_info']['booking_date']);
                    $booking_infos['booking_info'] = $order_product['extra']['booking_info'];
                    $input_data['booking_info'] = serialize($booking_infos);
                    db_query("INSERT INTO ?:ec_booking_reser_booking_info ?e", $input_data);
                    $booking_order_info = fn_ec_booking_classes_get_booking_order_info($order_product['product_id'], $booking_date,$booking_infos);
                    $booking_infos['extra_info'] = $booking_order_info;
                    $booking_infos['product_name'] = $order_product['product'];
                    fn_ec_booking_classes_send_customer_email_notification($booking_infos,$user_id);
                    fn_ec_booking_classes_send_admin_email_notification($booking_infos,$user_id);
                    fn_ec_booking_classes_send_vendor_email_notification($booking_infos,$user_id,$order_product['product_id']);

                    $booking_info['customer_fullname'] = $order_info['s_firstname'] .' '. $order_info['s_lastname'];
                    fn_ec_booking_classes_send_sms_notification_to_customer($order_product['product_id'],$booking_date,$type,$booking_infos,$order_info['s_phone']);
                    $phone_number = Registry::get('addons.sms_notifications.phone_number');
                    fn_ec_booking_classes_send_sms_notification_to_admin($order_product['product_id'],$booking_date,$type,$booking_infos,$phone_number);
                    fn_ec_booking_classes_send_sms_notification_to_vendor($order_product['product_id'],$booking_date,$type,$booking_infos);
                }
            }
        }
    }
}

function fn_ec_booking_classes_get_booking_order_info($product_id,$booking_date,$booking_infos) {
    $data = array();
    $product_data = fn_get_product_data($product_id, $_SESSION['auth']);
    $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
    $total_product = count($booking_infos['child']['ec_child_name']);
    $prepayment = $price * 0.1 * $total_product;
    $surcharge = ($price * $total_product)  - $prepayment;
    $end_date = $product_data['date_to'];
    $start_date = $product_data['date_from'];
    $date_of_purchase = date('m/d/y',$booking_date);
    $company_data = fn_get_company_by_product_id($product_id);
    $class_name = $product_data['product'];
     
    $data = array(
        'company_name' => $company_data['company'],
        'company_phone' => $company_data['phone'],
        'company_address' => $company_data['address'],
        'class_name'  => $class_name,
        'dateopur'  => $date_of_purchase,
        'start_date'  => $start_date,
        'end_date'  => $end_date,
        'prepayment'  => $prepayment,
        'surcharge'  => $surcharge
    );

    return $data;
}

function fn_ec_booking_classes_check_child_available($user_id) {
    $is_child = db_get_field("SELECT count(*) FROM ?:ec_booking_save_child_info WHERE user_id = ?i", $user_id);
    if($is_child)
        return true;
    else 
        return false;
}

function fn_ec_booking_classes_send_sms_notification_to_customer($product_id,$booking_date,$type,$booking_infos,$phone) {
    if (Registry::get('addons.ec_booking_classes.send_customer') != 'Y') {
        return false;
    }
    $product_data = fn_get_product_data($product_id, $_SESSION['auth']);
    $product_name = $product_data['product'];
    $customer_name = $booking_infos['customer_fullname'];
    if($type == 'discount_book') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = $price * 0.1 * $total_product;
        $surcharge = ($price * $total_product)  - $prepayment;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }elseif($type == 'free_trail') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = 0;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }else if($type == 'book_type') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = $price * $total_product;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }

    $body = Registry::get('addons.ec_booking_classes.ec_bookin_sms_book_order_template_customer');
    $body = str_replace('{book_start_date}', $start_data, $body);
    $body = str_replace('{book_end_date}', $end_date, $body);
    $body = str_replace('{product_name}', $product_name, $body);
    $body = str_replace('{customer_fullname}', $customer_name, $body);
    $body = str_replace('{prepayment_amount}', fn_format_price_by_currency($prepayment), $body);
    $body = str_replace('{surcharge_amount}', fn_format_price_by_currency($surcharge), $body);
    if(!empty($phone))
        $phone_number = $phone;
    else
        $phone_number = '';
    // fn_ec_booking_classes_send_sms_notification($body,$phone_number);
    // fn_rus_unisender_send_sms($body,$phone_number);
    $sender_name = Registry::get('addons.ec_booking_classes.ec_booking_template_sender_name');
    if(function_exists('fn_targetsms_send_sms'))
        fn_targetsms_send_sms($phone_number, $body,$sender_name,'sendmanualsms');
}

function fn_ec_booking_classes_send_sms_notification_to_admin($product_id,$booking_date,$type,$booking_infos,$phone) {
    if (Registry::get('addons.ec_booking_classes.send_admin') != 'Y') {
        return false;
    }
    $product_data = fn_get_product_data($product_id, $_SESSION['auth']);
    $product_name = $product_data['product'];
    $customer_name = $booking_infos['customer_fullname'];

    if($type == 'discount_book') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = $price * 0.1 * $total_product;
        $surcharge = ($price * $total_product)  - $prepayment;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }
    elseif($type == 'free_trail') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = 0;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    } 
    else if($type == 'book_type') {
        $price =fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = $price * $total_product;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }
        
    $body = Registry::get('addons.ec_booking_classes.ec_bookin_sms_book_order_template_admin');
    $body = str_replace('{book_start_date}', $start_data, $body);
    $body = str_replace('{book_end_date}', $end_date, $body);
    $body = str_replace('{product_name}', $product_name, $body);
    $body = str_replace('{customer_fullname}', $customer_name, $body);
    $body = str_replace('{prepayment_amount}', fn_format_price_by_currency($prepayment), $body);
    $body = str_replace('{surcharge_amount}', fn_format_price_by_currency($surcharge), $body);
    if(!empty($phone))
        $phone_number = trim($phone);
    else
        $phone_number = '';
    
    // fn_ec_booking_classes_send_sms_notification($body,$phone_number);
    // fn_rus_unisender_send_sms($body,$phone_number);
    $sender_name = Registry::get('addons.ec_booking_classes.ec_booking_template_sender_name');
    if(function_exists('fn_targetsms_send_sms'))
        fn_targetsms_send_sms($phone_number, $body,$sender_name,'sendmanualsms');
}

function fn_ec_booking_classes_send_sms_notification_to_vendor($product_id,$booking_date,$type,$booking_infos) {
    if (Registry::get('addons.ec_booking_classes.send_vendor') != 'Y') {
        return false;
    }
    $company_data = fn_get_company_by_product_id($product_id);
    $product_data = fn_get_product_data($product_id, $_SESSION['auth']);
    $product_name = $product_data['product'];
    $customer_name = $booking_infos['customer_fullname'];

    if($type == 'discount_book') {
        $price = fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = $price * 0.1 * $total_product;
        $surcharge = ($price * $total_product)  - $prepayment;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }elseif($type == 'free_trail') {
        $price = fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = 0;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    } else if($type == 'book_type') {
        $price = fn_get_product_price($product_id,1,$_SESSION['auth']);
        $total_product = count($booking_infos['child']['ec_child_name']);
        $prepayment = 0;
        $surcharge = $price * $total_product;
        $end_date = $product_data['date_to'];
        $start_data = date('m/d/y',$booking_date);
    }
    
    $body = Registry::get('addons.ec_booking_classes.ec_book_sms_book_order_template_vendor');
    $body = str_replace('{book_start_date}', $start_data, $body);
    $body = str_replace('{book_end_date}', $end_date, $body);
    $body = str_replace('{product_name}', $product_name, $body);
    $body = str_replace('{customer_fullname}', $customer_name, $body);
    $body = str_replace('{prepayment_amount}', fn_format_price_by_currency($prepayment), $body);
    $body = str_replace('{surcharge_amount}', fn_format_price_by_currency($surcharge), $body);
    if(!empty($company_data['phone']))
        $phone_number = trim($company_data['phone']);
    else 
        $phone_number = '';
    
    // fn_ec_booking_classes_send_sms_notification($body,$phone_number);

    // fn_rus_unisender_send_sms($body,$phone_number);
    $sender_name = Registry::get('addons.ec_booking_classes.ec_booking_template_sender_name');
    if(function_exists('fn_targetsms_send_sms'))
        fn_targetsms_send_sms($phone_number, $body,$sender_name,'sendmanualsms');
}

function fn_ec_booking_classes_send_sms_notification($body,$to)
{
    $addon_settings = Registry::get('addons.sms_notifications');
    $api_key        = $addon_settings['clickatel_api_id'];
    $concat         = $addon_settings['clickatel_concat'];
    $unicode        = $addon_settings['clickatel_unicode'] == 'Y' ? 1 : 0;

    if (fn_is_empty($api_key) || empty($to)) {
        return false;
    }

    //get the last symbol
    if (!empty($concat)) {
        $concat = intval($concat[strlen($concat)-1]);
    }
    if (!in_array($concat, array('1', '2', '3'))) {
        $concat = 1;
    }

    $sms_length = $unicode ? SMS_NOTIFICATIONS_SMS_LENGTH_UNICODE : SMS_NOTIFICATIONS_SMS_LENGTH;
    if ($concat > 1) {
        $sms_length *= $concat;
        $sms_length -= ($concat * SMS_NOTIFICATIONS_SMS_LENGTH_CONCAT); // If a message is concatenated, it reduces the number of characters contained in each message by 7
    }

    $body = html_entity_decode($body, ENT_QUOTES, 'UTF-8');
    $body = fn_substr($body, 0, $sms_length);
    $body = strip_tags($body);

    $data = array(
        'apiKey'  => $api_key,
        'to'      => $to,
        'content' => $body,
    );

    Http::get(SMS_NOTIFICATIONS_API_URL, $data);
}