<?php
/**
 * CS-Cart Booking classes - boking classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    Ecarter Software Private Limited <support@ecarter.co>
 * @copyright 2019 Ecarter Software Private Limited
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 */

if (!defined('AREA')) {
    die('Access denied');
}

fn_register_hooks(
    'update_product_pre',
    'get_product_data',
    'get_my_product_data',
    'update_product_post',
    'pre_add_to_cart',
    'order_placement_routines',
    'tools_change_status',
    'get_products'
);