<?php
/**
 * CS-Cart Booking Classes - ec_booking_classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    Ecarter  <support@ecarter.co>
 * @copyright 2019 Ecarter 
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'check_slots') {
    if (defined('AJAX_REQUEST')) {
        $selected_date          = strtotime($_REQUEST['selected_date']);
        $selected_date_format   = fn_date_format($selected_date, "%Y-%m-%d");
        $all_slots              = fn_ec_booking_classes_get_single_day_all_slots($selected_date_format, $_REQUEST['product_id']);
        
        if (isset($_REQUEST['template']) && $_REQUEST['template'] == 3) {
            $product = array('product_id' => $_REQUEST['product_id']);
            Tygh::$app['view']->assign('product', $product);
            Tygh::$app['view']->assign('all_slots', $all_slots);
            Tygh::$app['view']->assign('obj_id', $_REQUEST['product_id']);

            $res = Registry::get('view')->fetch('addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content_block.tpl');
            Registry::get('ajax')->assignHtml('booking-select-container'.$_REQUEST['product_id'], $res);
            // Tygh::$app['view']->display('addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content_block.tpl');
            exit;
        }
        Tygh::$app['ajax']->assign('selected_date', $selected_date);
        Tygh::$app['ajax']->assign('available_time_slots', $all_slots['available_time_slots']);
        Tygh::$app['ajax']->assign('unavailable_time_slots', $all_slots['unavailable_time_slots']);

        exit;
    }
}

if ($mode == 'get_booking_calendar') {
    // if (defined('AJAX_REQUEST')) {
    $product_id = $_REQUEST['product_id'];
    $product    = fn_get_product_data($product_id, $_SESSION['auth']);

    $curr_date  = time();
    $end_date   = (int)$product['date_to'];
    $start_date = (int)$product['date_from'];

    if ($curr_date > $start_date) {
        $start_date = $curr_date;
    }

    if ($curr_date > $end_date) {
        $end_date = $curr_date;
    }

    $end_date   = date('Y-m-d', $end_date);
    $curr_date  = date('Y-m-d', $curr_date);
    $start_date = date('Y-m-d', $start_date);
        
    Tygh::$app['view']->assign('product', $product);
    Tygh::$app['view']->assign('end_date', $end_date);
    Tygh::$app['view']->assign('start_date', $start_date);
    Tygh::$app['view']->assign('date_val_format', $curr_date);


    $res = Registry::get('view')->fetch('addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content.tpl');
    // fn_print_r($res);
    Registry::get('ajax')->assignHtml('booking-select-container'.$_REQUEST['product_id'], $res);
    exit;
    // }
    exit;
}

if($mode == 'get_child_info') {
    if (defined('AJAX_REQUEST')) {
        $product = array('product_id' => $_REQUEST['product_id']);
        Tygh::$app['view']->assign('product', $product);
        $child_info =  fn_ec_booking_classes_get_booked_data_child_info_complete($auth['user_id']);
        Tygh::$app['view']->assign('child_info', $child_info);
        $res = Registry::get('view')->fetch('addons/ec_booking_classes/views/ec_booking_classes/components/get_child_info.tpl');
        Registry::get('ajax')->assignHtml('booking-select-container'.$_REQUEST['product_id'], $res);
        exit;
    }
}

if ($mode == 'book_slot_info') {
    foreach ($_REQUEST['product_data'] as $_key => $product_data) {
        $book_info                  = $product_data['booking_info']['booking_slot'];
        $book_slot                  = explode(' to ', $book_info);
        $start                      = explode(' ', $book_slot[0]);
        $time_slot['start_date']    = fn_date_format($start[0], '%Y-%m-%d');
        $time_slot['start_time']    = $start[1];
        $end                        = explode(' ', $book_slot[1]);
        $time_slot['end_date']      = fn_date_format($end[0], '%Y-%m-%d');
        $time_slot['end_time']      = $end[1];
        Registry::get('view')->assign('time_slot', $time_slot);
    }
}

if($mode == 'savebookclasses') {
    if($auth['user_id']) {
        if (defined('AJAX_REQUEST')) { 
            $product_child = $_REQUEST['product_child'];           
            foreach($_REQUEST['product_data'] as $key => $item) {
                $booking_info = $item['booking_info'];
                $product_id = $item['product_id'];
                $type=$item['booking_info']['booking_type'];
            }
            if(!empty($product_child) && !empty($booking_info)) {
                $is_order = fn_ec_booking_classes_save_booking_info($product_child,$booking_info,$type,$product_id,$auth['user_id']);
                if($is_order) {
                    fn_set_notification("N",__("notice"),__('booking_is_saved_wait'));
                }
            }
            else {
                fn_set_notification("E",__("error"),__('something_is_missing_please'));
            }
        }
        else {
            $product_child = $_REQUEST['product_child'];           
            foreach($_REQUEST['product_data'] as $key => $item) {
                $booking_info = $item['booking_info'];
                $product_id = $item['product_id'];
                $type=$item['booking_info']['booking_type'];
            }
            
            if(!empty($product_child) && !empty($booking_info)) {
                $is_order = fn_ec_booking_classes_save_booking_info($product_child,$booking_info,$type,$product_id,$auth['user_id']);
                if($is_order) {
                    fn_set_notification("N",__("notice"),__('booking_is_saved_wait'));
                }
            }
            else {
                fn_set_notification("E",__("error"),__('something_is_missing_please'));
            }
            return [CONTROLLER_STATUS_OK, $_REQUEST['redirect_url']];
        }
    }
    else {
        fn_set_notification("E",__("error"),__('please_login_first'));
    }
}
