<?php
/**
 * CS-Cart Booking Classes - ec_booking_classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    Ecarter  <support@ecarter.co>
 * @copyright 2019 Ecarter 
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($mode == 'add_book_type') {
        fn_print_R($_REQUEST);
    }

    if($mode == 'multiple_slot_popup') {
        
    }

    if($mode == 'delete_child') {
        if($auth['user_id']) {
            if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
                $is_delete = fn_ec_booking_classes_delete_child_data($_REQUEST['id'],$auth['user_id']);
                if($is_delete) {
                    fn_Set_notification("N",__("notice"),__("ec_booking_child_dateis_delete_successfully"));
                    return [CONTROLLER_STATUS_OK, 'ec_booking_classes.child_manage'];
                }
            }
        }
        else {
            fn_Set_notification("E",__("error"),__("ec_booking_login_first"));
            return [CONTROLLER_STATUS_OK, 'ec_booking_classes.child_manage'];
        }
    }

    if($mode == 'cancel') {
        if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $is_cancel = fn_ec_booking_classes_cancel_order($_REQUEST);
            if($is_cancel) {
                fn_set_notification("N",__("notice"),__("ec_booking_data_cancel_order_successfullly"));
                return [CONTROLLER_STATUS_OK, 'ec_booking_classes.details&id='.$_REQUEST['id']];
            }
        }
    }

    if($mode == 'add_child') {
        if(isset($_REQUEST['ec_child_name']) && !empty($_REQUEST['ec_child_name'])) {
            $is_saved = fn_ec_booking_classes_get_child_saved_info($_REQUEST,$auth['user_id']);
            if($is_saved) {
                fn_set_notification("N",__("notice"),'ec_booki_child_info_saved');
                return [CONTROLLER_STATUS_OK, 'ec_booking_classes.child_manage'];
            }
            else {
                fn_set_notification("E",__("error"),'ec_some_error_come');
            }
        }
    }
}

else if($mode == 'manage') {
    if($auth['user_id']) {
        fn_add_breadcrumb(__('book_orders'));
        $default_params = array (
            'page'              => 1,
            'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
        );
        $params = $_REQUEST;
        $params['user_id'] = $auth['user_id'];
        $params = array_merge($default_params, $params);
        list($booked_orders,$search) = fn_ec_booking_classes_get_booked_datas($params);
        Tygh::$app['view']->assign('booked_orders', $booked_orders);
        Tygh::$app['view']->assign('search', $search);
    }
    else {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
}

else if ($mode == 'child_manage') {
    if($auth['user_id']) {
    fn_add_breadcrumb(__('ec_book_add_child'));
        $default_params = array (
            'page'              => 1,
            'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
        );
        $params = $_REQUEST;
        $params['user_id'] = $auth['user_id'];
        $params = array_merge($default_params, $params);
        list($child_info,$search) = fn_ec_booking_classes_get_booked_data_child_info($params,$auth['user_id']);
        Tygh::$app['view']->assign('child_info', $child_info);
        Tygh::$app['view']->assign('search', $search);
    }
    else {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
}

else if($mode == 'details') {
    fn_add_breadcrumb(__('book_orders'),fn_url('ec_booking_classes.manage'));
    fn_add_breadcrumb(__('book_orders_info'));
    if($auth['user_id']) {
        Registry::set('navigation.tabs', array (
            'detailed' => array (
                'title' => __('general'),
                'js' => true
            ),
        ));

        $params = $_REQUEST;
        $params['user_id'] = $auth['user_id'];
        list($booked_order,$search) = fn_ec_booking_classes_get_booked_data_by_user($params);
        Tygh::$app['view']->assign('booked_order', $booked_order);
    }
    else {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
}