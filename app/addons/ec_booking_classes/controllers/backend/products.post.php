<?php
/**
 * CS-Cart Booking Classes - Booking Classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    WebKul Software Private Limited <support@ecarter.co>
 * @copyright 2010 WebKul Software Private Limited
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 * @link      Technical Support: Forum - http://support@ecarter.co/ticket
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'update') {
    Registry::set(
        'navigation.tabs.booking_classes', array(
            'title' => __('booking_classes'),
            'js'    => true
        )
    );
}