<?php
/**
 * CS-Cart Booking Classes - ec_booking_classes
 * 
 * PHP version 7.1
 * 
 * @category  Add-on
 * @package   CS_Cart
 * @author    Ecarter  <support@ecarter.co>
 * @copyright 2019 Ecarter 
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @version   GIT: 1.2
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($mode == 'delete_child') {
        if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
            $is_delete = fn_ec_booking_classes_delete_child_data($_REQUEST['id'],$auth['user_id']);
            if($is_delete) {
                fn_Set_notification("N",__("notice"),__("ec_booking_child_dateis_delete_successfully"));
                return [CONTROLLER_STATUS_OK, 'ec_booking_classes.show_child&user_id='.$_REQUEST['user_id']];
            }
        }
    }
}
else if($mode == 'manage') {
    if($auth['user_type'] == 'A'||'V') {
        $default_params = array (
            'page'              => 1,
            'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
        );
        $params = $_REQUEST;
        $params = array_merge($default_params, $params);
        list($booked_orders,$search) = Fn_ec_booking_classes_Get_Booked_information('',$params);
        Tygh::$app['view']->assign('booked_orders', $booked_orders);
        Tygh::$app['view']->assign('search', $search);
    }
}

else if($mode == 'details') {

    Registry::set('navigation.tabs', array (
        'detailed' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

    $params = $_REQUEST;
    list($booked_order,$search) = fn_ec_booking_classes_get_booked_data($params);
    Tygh::$app['view']->assign('booked_order', $booked_order);
}

else if($mode == 'show_child') {

    $default_params = array (
        'page'              => 1,
        'items_per_page'    => Registry::get('settings.Appearance.admin_elements_per_page')
    );
    $params = $_REQUEST;
    $user_id = $_REQUEST['user_id'];
    $params = array_merge($default_params, $params);
    list($child_info,$search) = fn_ec_booking_classes_get_booked_data_child_info($params,$user_id);
    Tygh::$app['view']->assign('child_info', $child_info);
    Tygh::$app['view']->assign('search', $search);
}
