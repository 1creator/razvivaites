<?php
use Tygh\Registry;

defined('BOOTSTRAP') or die('Access Denied');

function fn_settings_variants_addons_ec_age_filter_min_age_feature()
{
    return db_get_hash_single_array("SELECT ?:product_filters.filter_id, ?:product_filter_descriptions.filter FROM ?:product_filters LEFT JOIN ?:product_filter_descriptions ON ?:product_filters.filter_id = ?:product_filter_descriptions.filter_id", array('filter_id', 'filter'));
}

function fn_settings_variants_addons_ec_age_filter_max_age_feature()
{
    return db_get_hash_single_array("SELECT ?:product_filters.filter_id, ?:product_filter_descriptions.filter FROM ?:product_filters LEFT JOIN ?:product_filter_descriptions ON ?:product_filters.filter_id = ?:product_filter_descriptions.filter_id", array('filter_id', 'filter'));
}

function fn_ec_age_filter_get_current_filters_post(array $params, array &$filters, array $selected_filters, $area, $lang_code, array $variant_values, array &$range_values, array &$field_variant_values, array $field_range_values)
{
    if(AREA == 'A'){
        return;
    }
    $min_max_array = array(
        'min' => Registry::get('addons.ec_age_filter.min_age_feature'),
        'max' => Registry::get('addons.ec_age_filter.max_age_feature')
    );
    if (!empty($range_values[$min_max_array['min']]) && !empty($range_values[$min_max_array['max']])) {
        $min = $range_values[$min_max_array['min']]['min'];
        $max = $range_values[$min_max_array['max']]['max'];

        $range_values[$min_max_array['min']]['max'] = $max;
        $range_values[$min_max_array['max']]['min'] = $min;
        
    }
}

function fn_ec_age_filter_get_filters_products_count_post(array $params, $lang_code, array &$filters, array $selected_filters){
    if(AREA == 'A'){
        return;
    }
    $min_max_array = array(
        'min' => Registry::get('addons.ec_age_filter.min_age_feature'),
        'max' => Registry::get('addons.ec_age_filter.max_age_feature')
    );
    if (!empty($filters[$min_max_array['min']]) && !empty($filters[$min_max_array['max']])) {
        $filters[$min_max_array['min']]['max'] = $filters[$min_max_array['max']]['max'];
        $filters[$min_max_array['min']]['filter'] = $filters[$min_max_array['min']]['filter'] ." - ".  $filters[$min_max_array['max']]['filter'];
        unset($filters[$min_max_array['max']]);
    }

}

function fn_ec_age_filter_get_products_pre(&$params, $items_per_page, $lang_code){
    if(AREA == 'A'){
        return;
    }
    if (!empty($params['features_hash']) || !empty($params['filter_variants'])) {
        $min_max_array = array(
            'min' => Registry::get('addons.ec_age_filter.min_age_feature'),
            'max' => Registry::get('addons.ec_age_filter.max_age_feature')
        );
        $values = explode(FILTERS_HASH_SEPARATOR, $params['features_hash']);
        foreach ($values as $value) {
            $variants = explode(FILTERS_HASH_FEATURE_SEPARATOR, $value);
            $filter_id = array_shift($variants);
            if($filter_id == $min_max_array['min']){
                $added_feature_hash = $min_max_array['max'].'-'.$variants[0].'-'.$variants[1];
                $params['features_hash'] = $params['features_hash'].'_'.$added_feature_hash;
            }
        }
    }
}

function fn_ec_age_filter_get_product_features_post(&$data, $params, $has_ungroupped){
    // fn_print_r($data);
    // exit;
    if(AREA == 'A'){
        return;
    }
    $min_feature_id = db_get_field("SELECT feature_id FROM ?:product_filters WHERE filter_id = ?i", Registry::get('addons.ec_age_filter.min_age_feature'));

    $max_feature_id = db_get_field("SELECT feature_id FROM ?:product_filters WHERE filter_id = ?i", Registry::get('addons.ec_age_filter.max_age_feature'));

    if (!empty($data[$min_feature_id]['variants']) && !empty($data[$max_feature_id]['variants'])) {
        $data[$min_feature_id]['variants'][$data[$min_feature_id]['variant_id']]['variant'] = $data[$min_feature_id]['variants'][$data[$min_feature_id]['variant_id']]['variant'] .'-'. $data[$max_feature_id]['variants'][$data[$max_feature_id]['variant_id']]['variant'];

        $data[$min_feature_id]['description'] = $data[$min_feature_id]['description'] .'-'. $data[$max_feature_id]['description'];
        unset($data[$max_feature_id]);
    }
}

function fn_ec_age_filter_get_products_features_list_post(&$products_features_list, $products, $display_on, $lang_code) {
    if(AREA == 'A'){
        return;
    }
    $min_feature_id = db_get_field("SELECT feature_id FROM ?:product_filters WHERE filter_id = ?i", Registry::get('addons.ec_age_filter.min_age_feature'));

    $max_feature_id = db_get_field("SELECT feature_id FROM ?:product_filters WHERE filter_id = ?i", Registry::get('addons.ec_age_filter.max_age_feature'));
    foreach($products_features_list as $product_id=>$products_features){
        if (!empty($products_features[$min_feature_id]['variants']) && !empty($products_features[$max_feature_id]['variants'])) {
            $products_features_list[$product_id][$min_feature_id]['variant'] = $products_features_list[$product_id][$min_feature_id]['variant'] .'-'. $products_features_list[$product_id][$max_feature_id]['variant'];
    
            $products_features_list[$product_id][$min_feature_id]['description'] = $products_features_list[$product_id][$min_feature_id]['description'] .'-'. $products_features_list[$product_id][$max_feature_id]['description'];
            unset($products_features_list[$product_id][$max_feature_id]);
        }
    }
}