<?php

defined('AREA') or die('Access Denied');

fn_register_hooks(
    'get_current_filters_post',
    'get_filters_products_count_post',
    'get_products_pre',
    'get_product_features_post',
    'get_products_features_list_post'
);
