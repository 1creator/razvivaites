<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_ab__hpd_install (){
$objects = array(
array( "t" => "?:product_tabs",
"i" => array(
array("n" => "ab__smc_hide_content", "p" => "char(1) NOT NULL DEFAULT 'N'"),
array("n" => "ab__smc_override", "p" => "char(1) NOT NULL DEFAULT 'N'"),
array("n" => "ab__smc_height", "p" => "int(5) UNSIGNED NOT NULL DEFAULT 250")
)
),
array( "t" => "?:product_tabs_descriptions",
"i" => array(
array("n" => "ab__smc_show_more", "p" => "VARCHAR(50) NOT NULL DEFAULT ''"),
array("n" => "ab__smc_show_less", "p" => "VARCHAR(50) NOT NULL DEFAULT ''")
)
)
);
if (!empty($objects) and is_array($objects)){
foreach ($objects as $o){
$fields = db_get_fields('DESCRIBE ' . $o['t']);
if (!empty($fields) and is_array($fields)){
if (!empty($o['i']) and is_array($o['i'])){
foreach ($o['i'] as $f) {
if (!in_array($f['n'], $fields)){
db_query("ALTER TABLE ?p ADD ?p ?p", $o['t'], $f['n'], $f['p']);
}
}
}
}
}
}
}
function fn_ab__hpd_update_tabs ( $more, $less, $id, $lang_code ) {
db_query('UPDATE ?:product_tabs_descriptions
SET ab__smc_show_more = ?s, ab__smc_show_less = ?s
WHERE tab_id = ?i AND lang_code = ?s', trim($more), trim($less), $id, $lang_code);
}
function fn_ab__hide_product_description_product_tab_updated( $tab_id ) {
if( isset($_REQUEST['tab_data']['ab__smc_show_more']) && isset($_REQUEST['tab_data']['ab__smc_show_less']) ) {
fn_ab__hpd_update_tabs($_REQUEST['tab_data']['ab__smc_show_more'], $_REQUEST['tab_data']['ab__smc_show_less'], $tab_id, DESCR_SL);
}
}
function fn_ab__hide_product_description_product_tab_created( $tab_id ) {
if( isset($_REQUEST['tab_data']['ab__smc_show_more']) && isset($_REQUEST['tab_data']['ab__smc_show_less']) ) {
foreach (array_keys(fn_get_translation_languages()) as $lang) {
fn_ab__hpd_update_tabs($_REQUEST['tab_data']['ab__smc_show_more'], $_REQUEST['tab_data']['ab__smc_show_less'], $tab_id, $lang);
}
}
}