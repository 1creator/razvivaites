<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$navigation=Registry::get('navigation.tabs');
$user_type=(!empty($_REQUEST['user_type'])) ? ($_REQUEST['user_type']) : $user_data['user_type'];

$user_data=Registry::get('user_data');

 if (!fn_allowed_for('ULTIMATE:FREE')) {
        if ($mode == 'update' &&
            (
                (!fn_check_user_type_admin_area($user_type) && !Registry::get('runtime.company_id')) // Customers
                ||
                (fn_check_user_type_admin_area($user_type) && !Registry::get('runtime.company_id')  && (!empty($user_data['company_id']) || (empty($user_data['company_id'])))) // root admin for other admins
                ||
                ($user_data['user_type'] == 'V' && Registry::get('runtime.company_id') && $auth['is_root'] == 'Y' && $user_data['user_id'] != $auth['user_id'] && $user_data['company_id'] == Registry::get('runtime.company_id')) // vendor for other vendor admins
            )
        ) {
            $navigation['usergroups'] = array (
                'title' => __('usergroups'),
                'js' => true
            );
        } else {
            $usergroups = array();
        }
    }

    Registry::set('navigation.tabs', $navigation);
$usergroups = fn_get_available_usergroups($user_type);

Tygh::$app['view']->assign('usergroups', $usergroups);