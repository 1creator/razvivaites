<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

    $tabs=Registry::get('navigation.tabs');
    unset($tabs['pickup']);
    Registry::set('navigation.tabs', $tabs);