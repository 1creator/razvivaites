<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update'||'add') {
    $tabs=Registry::get('navigation.tabs');
    unset($tabs['options']);
    unset($tabs['shippings']);
    unset($tabs['qty_discounts']);
    unset($tabs['subscribers']);
    unset($tabs['addons']);

    if ($auth['company_id']) {
    	unset($tabs['seo']);
    }
    Registry::set('navigation.tabs', $tabs);
      
}
