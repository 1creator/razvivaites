<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'update'||'add') {
    $tabs=Registry::get('navigation.tabs');
    //unset($tabs['terms_and_conditions']);
    unset($tabs);

    $tabs['description'] = array(
        'title' => "Реквизиты",
        'js' => true
    );
    $tabs['detailed'] = array(
        'title' => "Контакты",
        'js' => true
    );
    $tabs['logos'] = array(
        'title' => __('logos'),
        'js' => true
    );
    
    Registry::set('navigation.tabs', $tabs);
      
}
