<?php

use Tygh\Registry;

if ($mode == 'products') {

    $company_data = !empty($_REQUEST['company_id']) ? fn_get_company_data($_REQUEST['company_id']) : array();

    if (empty($company_data) || empty($company_data['status']) || !empty($company_data['status']) && $company_data['status'] != 'A') {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    $company_data['logos'] = fn_get_logos($company_data['company_id']);

    Registry::set('navigation.tabs', array(
        'description' => array(
            'title' => __('description'),
            'js' => true
        )
    ));

    $params = array(
        'company_id' => $_REQUEST['company_id'],
    );

    Tygh::$app['view']->assign('company_data', $company_data);

} 