<?php

$schema['mx_vendor_panel'] = array(
    'templates' => array(
        'addons/mx_vendor_panel/blocks/vendor_header.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'disable_cache_when' => array(
            'request_handlers' => array('q')
        )
    )
);

return $schema;
