<?php

use Tygh\Registry;

if (Registry::get('runtime.company_id')) {

    unset($schema['top']['administration']['items']['import_data']);
	unset($schema['top']['administration']['items']['export_data']);
	unset($schema['top']['administration']['items']['shippings_taxes']);
	unset($schema['top']['administration']['items']['currencies']);
	unset($schema['top']['administration']['items']['logs']);
	unset($schema['top']['administration']['items']['files']);
	unset($schema['top']['administration']['items']['addons_divider']);
	unset($schema['central']['website']);
	unset($schema['central']['orders']['items']['view_orders']);
	unset($schema['central']['orders']['items']['sales_reports']);
	unset($schema['central']['orders']['items']['shipments']);
	unset($schema['central']['orders']['items']['users_carts']);

	unset($schema['central']['products']['items']['categories']);
	unset($schema['central']['products']['items']['features']);
	unset($schema['central']['products']['items']['filters']);
	unset($schema['central']['products']['items']['options']);

	unset($schema['central']['customers']['items']['vendor_administrators']);
	unset($schema['central']['vendors']['items']['vendor_accounting']);
	$schema['central']['vendors']['items'] = $schema['central']['vendors']['items']+$schema['top']['administration']['items'];
	unset($schema['top']['administration']);
	unset($schema['central']['orders']);
	unset($schema['central']['customers']);
	unset($schema['central']['products']);
	unset($schema['central']['vendors']);
/*$schema['central']['mx_vendor_panel'] = array(
	'attrs' => array(
		'class' => 'is-addon'
	),
	'href' => 'addon_name.your_mode',
	'position' => 2000
);
*/
//fn_print_die($schema);
}
else {
	$schema['central']['vendors']['items']['store_locator'] = $schema['top']['administration']['items']['store_locator'];
	unset($schema['top']['administration']['items']['store_locator']);
	unset($schema['central']['orders']['items']['view_orders']);
	unset($schema['central']['orders']['items']['sales_reports']);
	unset($schema['central']['orders']['items']['shipments']);
	unset($schema['central']['orders']['items']['users_carts']);
	unset($schema['central']['products']['items']['options']);
	unset($schema['central']['vendors']['items']['vendor_accounting']);
	unset($schema['central']['marketing']);
}
//fn_print_r(Tygh::$app['session']['auth']['usergroup_ids']);

if (Tygh::$app['session']['auth']['usergroup_ids']) {
	if (Tygh::$app['session']['auth']['usergroup_ids'][0]==5) {
		unset($schema['top']['administration']);
		unset($schema['top']['design']);
		unset($schema['central']['customers']['items']['vendor_administrators']);
		unset($schema['central']['customers']['items']['administrators']);
		unset($schema['central']['website']['items']['pages']);
		unset($schema['central']['ab__addons']);
		}
}

return $schema;