/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function (_, $) {
$.ceEvent('on', 'ce.commoninit', function(context) {
if( _.ab__smc.selector ) {
var elems = context.find(_.ab__smc.selector);
if (elems.length) {
elems.css("opacity", "0.999999");
$.each(elems, function ( ) {
var elem = $(this);
var more_txt = _.ab__smc.more;
var less_txt = _.ab__smc.less;
var max_height = parseInt(_.ab__smc.max_height);
var attr = elem.attr("data-ab-smc-tab-hide");
if ( attr != void(0) ) {
more_txt = elem.attr("data-ab-smc-more");
less_txt = elem.attr("data-ab-smc-less");
attr = attr.split('|')[1];
if ( attr === 'Y' ) {
max_height = parseInt(elem.attr("data-ab-smc-height"));
}
}
var elem_height = parseInt(elem.height());
if (elem_height > max_height) {
var t = _.ab__smc.transition;
elem.addClass('ab-smc-description' + _.ab__smc.description_element_classes).css({
maxHeight: max_height + "px",
transition: "max-height " + t + "s ease",
});
elem.append("<div class='ab-smc-more" + _.ab__smc.additional_classes_parent + "'><span>" + more_txt + "</span><i class='ab-smc-arrow'></i></div>");
elem.find(".ab-smc-more span").addClass(_.ab__smc.additional_classes);
var padding = parseInt( elem.css("padding-bottom") );
var timeout = t * 0.65 * 1000; // изменение текста за 35% до того как закончится анимация
var clickable = elem.find(".ab-smc-more:not(.ab-smc-opened)");
var clickable_height = clickable.outerHeight();
if ( padding < clickable_height ) {
elem.css("padding-bottom", clickable_height + "px");
}
clickable.on("click", function() {
var btn = $(this);
btn.parent().addClass("ab-smc-opened");
btn.parent().css({
maxHeight: elem_height + "px"
});
setTimeout(function(){
btn.addClass("ab-smc-opened");
if ( !_.ab__smc.show_button ) {
elem.find(".ab-smc-more").detach();
return;
}
elem.find(".ab-smc-more").html("<span>" + less_txt + "</span><i class='ab-smc-arrow'></i>");
elem.find(".ab-smc-more span").addClass(_.ab__smc.additional_classes);
}, timeout);
});
_.ab__smc.show_button && elem.on("click", ".ab-smc-more.ab-smc-opened", function(){
var btn = $(this);
btn.parent().css({
maxHeight: max_height + "px"
});
setTimeout(function(){
btn.removeClass("ab-smc-opened");
btn.parent().removeClass("ab-smc-opened");
elem.find(".ab-smc-more").html("<span>" + more_txt + "</span><i class='ab-smc-arrow'></i>");
elem.find(".ab-smc-more span").addClass(_.ab__smc.additional_classes);
}, timeout);
})
}
});
}
}
});
}(Tygh, Tygh.$));