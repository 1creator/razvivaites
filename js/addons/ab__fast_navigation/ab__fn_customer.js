/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function (_, $) {
$.ceEvent('on', 'ce.ab__fn_init', function(context) {
context.find(".ab-fn-parent").each(function(){
var parent = $(this);
var parent_id = parent.attr("data-block-id");
var items = parent.find(".ab-fn-first-level-item");
var subitems = parent.find(".ab-fn-second-level");
parent.find(".ab-fn-first-level").removeClass("ab-fn-clipped");
if (_.ab__fn != void(0)) {
var columns_opt = _.ab__fn.blocks[parent_id];
$.ceEvent('on', 'ce.ab__fn_init', function () { calculate_items_width(parent, columns_opt); });
if ( !subitems.first().hasClass("owl-carousel") ) {
$(window).on("resize", function () { calculate_items_width(parent, columns_opt) });
}
}
items.on('click', function () {
var item = $(this);
var item_id = item.attr("data-item-id");
var subitem_wrap = parent.find("#ab__fn-second-level-" + parent_id + '-' + item_id);
if (!item.hasClass("active")) {
if ( !subitem_wrap.is(".uploaded, .first") && !!subitem_wrap.data("childs-count") ) {
subitem_wrap.addClass("uploaded");
var overlay = $(".ab-fn-overlay");
var scroller = !subitem_wrap.data("add-delimeter");
if ( overlay.length ) {
overlay.show();
} else {
overlay = $("<div class='ab-fn-overlay'><div class='ab-fn-load-progress'><span></span></div></div>").appendTo("body");
};
$.ceAjax('request', fn_url('ab__fast_navigation.load_subitems'), {
method: "POST",
data: {
parent: item_id,
block: {
id: parent_id,
snapping: Number( parent.data("snap") )
},
scroller: scroller,
dir: $("html").attr("dir")
},
hidden: false,
caching: false,
callback: function ( answer ) {
if ( answer.second_level != void(0) ) {
if ( answer.second_level.subitems != void(0) ) {
var items_template = answer.second_level.subitems;
subitem_wrap.prepend( items_template );
}
if ( answer.second_level.js != void(0) ) {
var js_template = answer.second_level.js;
parent.append( js_template );
}
$.ceEvent('trigger', 'ce.ab__fn_init', [context]);
}
overlay.hide();
items.removeClass("active");
subitems.removeClass("active");
item.addClass("active");
subitem_wrap.addClass("active");
}
});
} else {
items.removeClass("active");
subitems.removeClass("active");
item.addClass("active");
subitem_wrap.addClass("active");
}
}
});
parent.find(".delimeter-block").on('click', delimeter_action);
});
});
function calculate_items_width(parent, columns_opt) {
var window_width = $(window).outerWidth();
var columns = 0;
if(window_width > 1230) {
columns = columns_opt.number_of_columns_desktop;
} else if (window_width > 992 && window_width <= 1230) {
columns = columns_opt.number_of_columns_desktop_small;
} else if (window_width > 768 && window_width <= 992) {
columns = columns_opt.number_of_columns_tablet;
} else if (window_width > 576 && window_width <= 768) {
columns = columns_opt.number_of_columns_tablet_small;
} else {
columns = columns_opt.number_of_columns_mobile;
}
var subitems = parent.find(".ab-fn-second-level");
subitems.each(function() {
var subitem = $(this);
if( subitem.hasClass("clicked-delimeter") ) return void(0);
subitem.find(".delimeter-block").detach();
var add_delimeter = ((!!(subitem.data("add-delimeter")) && !subitem.hasClass("clicked-delimeter")) );
var subitem_siblings = subitem.find(".ab-fn-second-level-item");
var delimeter_iteration = 0;
if( add_delimeter && (subitem_siblings.length > columns) ) {
subitem_siblings.each( function( index ) {
var sibling = $(this);
var delimeter_str = this.className.split("delimeter-");
var second_part = ' ';
if (delimeter_str[1]) {
second_part += delimeter_str[1].substr(1);
}
this.className = delimeter_str[0] + " delimeter-" + delimeter_iteration + second_part;
if ( !(index === subitem_siblings.length-1) &&
( (index+2) === (delimeter_iteration * columns + columns) )) {
delimeter_iteration = delimeter_iteration+1;
var x = 0;
var y = subitem_siblings.length;
if( (subitem_siblings.length - (index+1)) >= columns ) {
x = columns;
} else {
x = subitem_siblings.length - (index+1);
}
var str = _.ab__fn.delimeter_data.text.replace("[x]", x).replace("[y]", y);
sibling.after(
"<div class='ab-fn-second-level-item ty-column" + columns + " delimeter-block' data-delimeter='" + delimeter_iteration + "'>" +
"<div>" +
"<div class='ab-fn-item-header'>" +
str +
"</div>" +
"<span class='ab-fn-delimeter-plus'></span>" +
"</div>" +
"</div>"
);
}
});
} else {
subitem_siblings.addClass("delimeter-0");
}
});
$(".delimeter-block").on('click', delimeter_action);
var children = parent.find(".ab-fn-first-level-item, .ab-fn-second-level-item");
for ( var i = 0; i < children.length; i++ ) {
var str_arr = children[i].className.split("ty-column");
children[i].className = str_arr[0] + "ty-column" + columns + str_arr[1].substr(1);
$(children[i]).css("width", (100 / columns) + "%");
}
}
function delimeter_action() {
var delimeter = $(this);
var delimeter_parent = delimeter.parents(".ab-fn-second-level");
var delimeter_id = Number(delimeter.attr("data-delimeter"));
delimeter_parent.addClass("clicked-delimeter");
delimeter_parent.find(".delimeter-" + delimeter_id).css("display", "inline-flex");
delimeter_parent.find(".delimeter-block[data-delimeter='"+(delimeter_id+1)+"']").css("display", "inline-flex");
delimeter.detach();
}
$(document).ready(function() {
if ( window.innerWidth > 767 ) {
$(".ab-fn-parent").each(function () {
var parent = $(this);
var first = parent.find(".ab-fn-first-level-item:first");
var first_el_height = first.outerHeight();
var sub_wraps = parent.find(".ab-fn-second-level").css("min-height", first_el_height + "px");
});
}
});
$.ceEvent('on', 'ce.commoninit', function(context) {
$.ceEvent('trigger', 'ce.ab__fn_init', [context]);
});
})(Tygh, Tygh.$);