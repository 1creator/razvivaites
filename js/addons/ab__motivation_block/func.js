/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2019   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function(_, $) {
function fn_ab__mb_load_block (context) {
$('.ab__motivation_block:not(.loaded)', context).each(function () {
var self = $(this);
$.ceAjax('request', fn_url('ab__motivation_block.view?category_id=' + self.data('caCategoryId')), {
result_ids: self.data('caResultId'),
hidden: true,
callback: function () {
self.addClass('loaded');
}
});
});
}
$.ceEvent('on', 'ce.commoninit', function (context) {
fn_ab__mb_load_block(context);
});
$.ceEvent('on', 'ce:geomap:location_set_after', function (location, $container, response, auto_detect) {
$('.ab__motivation_block.loaded').removeClass('loaded');
fn_ab__mb_load_block(_.doc);
});
}(Tygh, Tygh.$));