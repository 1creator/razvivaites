{assign var="obj_id" value=$company_data.company_id}
{assign var="obj_id_prefix" value="`$obj_prefix``$obj_id`"}
    {include file="common/company_data.tpl" company=$company_data show_name=true show_descr=true show_rating=true show_logo=true show_links=true show_address=true show_location_full=true}
    <div class="ty-company-detail clearfix">

        <div id="block_company_{$company_data.company_id}" class="clearfix">
            <h1 class="ty-mainbox-title">{$company_data.company}</h1>

            <div class="ty-company-detail__info vendor_detail">
                <div class="ty-company-detail__info-list ty-company-detail_info-first">
                    <h5 class="ty-company-detail__info-title">{__("contact_information")}</h5>
                    {$block_id = $block.snapping_id|default:$id}
                    <div class="ec_vendor_filter_map" style="padding: 20px 0px;" id="product_filters_vmap_{$block_id}">
                        {$store_ids = fn_get_products_vendor_stores($products)}

                        {$store_ids = implode(',',$store_ids)}
                        {$ec_store_locations =fn_ec_get_vendor_filter_map_data($store_ids)}
                        {include file="addons/ec_vendor_points/views/ec_vendor_points/map_block.tpl" category_view='category'}

                    <!--product_filters_vmap_{$block_id}--></div>
                     {if $ec_store_locations}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("address")}:</label>
                            {foreach from=$ec_store_locations item=store_location}
                                {foreach from=$store_location item=store}
                                    <span>{$store.city}, {$store.pickup_address}</span> <br/>
                                {/foreach}
                            {/foreach}
                        </div>
                    {/if}
                    {if $company_data.email}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("email")}:</label>
                            <span><a href="mailto:{$company_data.email}">{$company_data.email}</a></span>
                        </div>
                    {/if}
                    {if $company_data.phone}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("phone")}:</label>
                            <span>{$company_data.phone}</span>
                        </div>
                    {/if}
                    {if $company_data.fax}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("fax")}:</label>
                            <span>{$company_data.fax}</span>
                        </div>
                    {/if}
                    {if $company_data.url}
                        <div class="ty-company-detail__control-group">
                            <label class="ty-company-detail__control-lable">{__("website")}:</label>
                            <span><a href="{$company_data.url|normalize_url}">{$company_data.url}</a></span>
                        </div>
                    {/if}
                </div>
                <div class="ty-company-detail__logo vendor_logo">
                    {assign var="capture_name" value="logo_`$obj_id`"}
                    {$smarty.capture.$capture_name nofilter}
                </div>
            </div>
        </div>

        {capture name="tabsbox"}
            <div id="content_description"
                 class="{if $selected_section && $selected_section != "description"}hidden{/if}">
                {if $company_data.company_description}
                    <div class="ty-wysiwyg-content">
                        {$company_data.company_description nofilter}
                    </div>
                {/if}
            </div>
            {hook name="companies:tabs"}
            {/hook}

        {/capture}
    </div>
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section}
    <style>
        .vendor_logo {
            float: right;
            margin-top: -49px;
        }
        .vendor_detail {
            overflow: unset;
        }
        .ty-company-detail__info-list {
            max-width: 360px;
        }
        .ec_vendor_filter_map {
            width: 175%!important;
        }
    </style>