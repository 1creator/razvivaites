 {if $auth.user_id}
    {if $user_info.firstname || $user_info.lastname}
        <li class="ty-account-info__item  ty-account-info__name ty-dropdown-box__item">{$user_info.firstname} {$user_info.lastname}</li>
    {else}
        <li class="ty-account-info__item ty-dropdown-box__item ty-account-info__name">{$user_info.email}</li>
    {/if}
    {if $auth.user_type != 'V'}
    <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"profiles.update"|fn_url}" rel="nofollow" >{__("profile_details")}</a></li>
    {/if}
    {if $settings.General.enable_edp == "Y"}
    <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"orders.downloads"|fn_url}" rel="nofollow">{__("downloads")}</a></li>
    {/if}
{elseif $user_data.firstname || $user_data.lastname}
    <li class="ty-account-info__item  ty-dropdown-box__item ty-account-info__name">{$user_data.firstname} {$user_data.lastname}</li>
{elseif $user_data.email}
    <li class="ty-account-info__item ty-dropdown-box__item ty-account-info__name">{$user_data.email}</li>
{/if}
{if $settings.General.enable_compare_products == 'Y'}
    {assign var="compared_products" value=""|fn_get_comparison_products}
    <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"product_features.compare"|fn_url}" rel="nofollow">{__("view_comparison_list")}{if $compared_products} ({$compared_products|count}){/if}</a></li>
{/if}
{if $auth.user_id && $auth.user_type != 'V'}
<li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"ec_booking_classes.manage"|fn_url}" rel="nofollow">{__("ec_book_classe_info")}</a></li>

<li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"ec_booking_classes.child_manage"|fn_url}" rel="nofollow">{__("ec_book_classe_add_child")}</a></li>
{/if}