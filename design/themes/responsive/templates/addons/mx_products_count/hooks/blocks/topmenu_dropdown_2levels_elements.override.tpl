{foreach from=$item1.$childs item="item2" name="item2"}
    {assign var="item_url2" value=$item2|fn_form_dropdown_object_link:$block.type}
    {assign var="count" value=$item2|fn_mx_products_count_get_count}
    <li class="ty-menu__submenu-item{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item2.class} {$item2.class}{/if}">
        <a class="ty-menu__submenu-link" {if $item_url2} href="{$item_url2}"{/if} {if $item2.new_window}target="_blank"{/if}>{$item2.$name} ({$count})</a>
    </li>
{/foreach}
{if $item1.show_more && $item1_url}
    <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
        <a href="{$item1_url}"
           class="ty-menu__submenu-alt-link">{__("text_topmenu_view_more")}</a>
    </li>
{/if}