{foreach from=$item2.$childs item="item3" name="item3"}
    {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
    {assign var="count3" value=$item3|fn_mx_products_count_get_count}
    <li class="ty-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item3.class} {$item3.class}{/if}">
        <a{if $item3_url} href="{$item3_url}"{/if}
                class="ty-menu__submenu-link" {if $item3.new_window}target="_blank"{/if}>{$item3.$name} ({$count3})</a>
    </li>
{/foreach}
{if $item2.show_more && $item2_url}
    <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
        <a href="{$item2_url}"
           class="ty-menu__submenu-link" {if $item2.new_window}target="_blank"{/if}>{__("text_topmenu_view_more")}</a>
    </li>
{/if}