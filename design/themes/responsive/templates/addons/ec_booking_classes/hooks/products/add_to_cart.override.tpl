{if $product.week_data}
    {if $auth.user_id}
        
            {if $quick_view}
                    {include file="buttons/button.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_text=__("select_booking") but_href="products.view?product_id=`$product.product_id`" but_role=$opt_but_role but_name="" but_meta="ty-btn__primary ty-btn__big"}
            {else}
                {include file="addons/ec_booking_classes/views/ec_booking_classes/components/booking.tpl"}
                {* {include file="addons/ec_booking_classes/buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_meta=$add_to_cart_meta but_text=__('add_booking')} *}
            {/if}
        
    {else}
        <a href="{$config.current_url|fn_url}" {if $settings.Security.secure_storefront != "partial"} data-ca-target-id="login_block{$block.snapping_id}" class="cm-dialog-opener cm-dialog-auto-size ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn booking_button login-booking-button"{/if} rel="nofollow">{__("ec_booking_button")}</a>
        {if $settings.Security.secure_storefront != "partial"}
            <div  id="login_block{$block.snapping_id}" class="hidden" title="{__("sign_in")}">
                <div class="ty-login-popup">
                    {include file="views/auth/login_form.tpl" style="popup" id="popup`$block.snapping_id`"}
                </div>
            </div>
        {/if}
    {/if}
{/if}
<style>
.login-booking-button {
    max-width:200px !important;
}

.object-container .ty-control-group__label {
    float: left !important;
    min-width:unset !important;
}

.ty-login__remember-me-label {
    float:left !important;
}
</style>