{if $order_info.products.$key.extra.booking_info}
    <div class="ty-control-group">
        <strong class="ty-control-group__label">{__('booking_info')}:</strong>
        <span class="ty-control-group__item">
        	{__('booking_date')}: {$order_info.products.$key.extra.booking_info.booking_date|date_format:"`$settings.Appearance.date_format`"}
        	<br>
        	{__('booking_slot')}: {$order_info.products.$key.extra.booking_info.booking_slot}
        </span>
    </div>
{/if}