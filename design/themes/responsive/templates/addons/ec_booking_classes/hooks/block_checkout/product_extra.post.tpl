{if $cart.products.$key.extra.booking_info}
	<div class="ty-control-group">
		<strong class="ty-control-group__label">{__('booking_info')}:</strong>
		<span class="ty-control-group__item">
			{__('booking_date')}: {$cart.products.$key.extra.booking_info.booking_date|date_format:"`$settings.Appearance.date_format`"}
			<br>
			{__('booking_slot')}: {$cart.products.$key.extra.booking_info.booking_slot}
		</span>
	</div>
{/if}