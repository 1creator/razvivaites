{* {$smarty.request|fn_print_R}
{if $smarty.request.dispatch == 'products.view'}
    <form action={""|fn_url} method="post" name="ec_booking_classes_{$product.product_id}" enctype="multipart/form-data" class="cm-disable-empty-files cm-ajax cm-ajax-full-render cm-ajax-status-middle cm-processed-form">

        {assign var="date_val" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
        {assign var="date_val_format" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
        {assign var="min_date_format" value={$smarty.const.TIME|date_format:"%d-%b-%Y"}}
        {assign var=start_date value=$product.date_from|date_format:"%Y-%m-%d"}

        {if $smarty.const.TIME > $product.date_from}
            {assign var="start_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
        {/if}

        {assign var=end_date value=$product.date_to|date_format:"%Y-%m-%d"}

        {if $smarty.const.TIME > $product.date_to}
            {assign var="end_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
        {/if}

        {assign var=all_slots value=fn_ec_booking_classes_get_single_day_all_slots($date_val_format,$product.product_id)}
        {assign var=all_type_available value=fn_ec_boking_classes_get_all_available_book_type($product)}

        <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}">
        <input type="hidden" id="select_type_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_slot]" />
        <input type='hidden' id='select_dat_{$product.product_id}' name="product_data[{$product.product_id}][booking_info][booking_date]" value="{$date_val}"/>
        <input type="hidden" id="select_types_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_type]" value="{$all_type_available.first_element}"/>

        
        {if $product.week_data}
            {assign var="suffix" value=$product.product_id}
            {assign var="obj_prefix" value=''}
            <div class="hidden" id="content_select_booking_{$obj_prefix}{$suffix}" title="{$text}">
                {include file="addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content.tpl" product=$product}
            </div>
        {/if}
    </form>
{/if} *}
{if $smarty.request.dispatch != 'products.view'}
    {foreach from=$products item=product key=key name=name}
        {if $product.week_data}
            <form action={""|fn_url} method="post" name="ec_booking_classes_{$product.product_id}" enctype="multipart/form-data" class="cm-disable-empty-files cm-ajax cm-ajax-full-render cm-ajax-status-middle cm-processed-form">

                {assign var="suffix" value=$product.product_id}
                {assign var="obj_prefix" value=''}
                {assign var="date_val" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
                {assign var="date_val_format" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
                {assign var="min_date_format" value={$smarty.const.TIME|date_format:"%d-%b-%Y"}}
                {assign var=start_date value=$product.date_from|date_format:"%Y-%m-%d"}
                {if $smarty.const.TIME > $product.date_from}
                    {assign var="start_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
                {/if}

                {assign var=end_date value=$product.date_to|date_format:"%Y-%m-%d"}

                {if $smarty.const.TIME > $product.date_to}
                    {assign var="end_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
                {/if}

                <input type="hidden" name="product_data[{$product.product_id}][product_id]" value="{$product.product_id}">
                <input type="hidden" name="redirect_url" value="{$redirect_url|default:$config.current_url}" />
                <input type="hidden" name="result_ids" value="cart_status*,wish_list*,checkout*,account_info*,abt__ut2_wishlist_count" />
                <input type="hidden" id="select_type_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_slot]" />
                <input type='hidden' id='select_dat_{$product.product_id}' name="product_data[{$product.product_id}][booking_info][booking_date]" value="{$date_val}"/>
                <input type="hidden" id="select_types_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_type]" value="{$all_type_available.first_element}"/>


                {$id = "select_booking_{$obj_prefix}{$suffix}"}
                <div class="hidden" id="content_{$id}" title="{$text}">
                    {include file="addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content.tpl" product=$product}
                </div>
            </form>
        {/if}
    {/foreach}
{/if}

<script type="text/javascript">
		(function (_, $) {
			$(document).on('click', '.closeOptionPanel', function(){
				var requestData = {};
				productId =  $(this).data('productId');
				requestData.product_id = productId;
				$.ceAjax('request', fn_url('products.get_booking_calendar'),{
					method: 'POST',
					result_ids: 'booking-select-container'+productId,
					full_render: true,
					data: requestData,
					callback: function(data) {
						var topOffset 	= $('#booking-select-container'+productId).parents('.ui-dialog.ui-widget').offset();
						topOffset.top	= topOffset.top - 100;
						$('#booking-select-container'+productId).parents('.ui-dialog.ui-widget').offset(topOffset);
						$(document).trigger('wk_test_event'+productId);
					}
				});
			});
			$(document).on('click','.select_booking_slot',function(event){
				var productId = $(this).data('productId');
				var book_data_val = $(this).siblings('.booking_info').val();
				$('#select_type_'+productId).val(book_data_val);
				$('#select_type_'+productId).val(book_data_val);
				$('#button_cart_'+productId).trigger('click');
				$('#button_cart_ajax'+productId).trigger('click');
			});

            $(document).on('click','.booking_info_slot',function(event){
                event.preventDefault();
				var requestData = {};
				var productId = $(this).data('productId');
				requestData.product_id =  productId;
				var book_data_val = $(this).siblings('.booking_info').val();
				$('#select_type_'+productId).val(book_data_val);
				var book_data_val = $(this).siblings('.booking_info').val();
				$.ceAjax('request', fn_url('products.get_child_info'),{
					method: 'POST',
					result_ids: 'booking-select-container'+productId,
					full_render: true,
					data: requestData,
					callback: function(data) {
						
					}
				});
			});
		})(Tygh, Tygh.$);
	</script>