<form action="{""|fn_url}" method="post" target="_self" name="ec_book_child_add">
{script src="js/addons/ec_booking_classes/node-clone/node_cloning.js"}
    <div class="booking-select-container">
     <div class="inner-child-info">
        </div>
        <div class="outer_main_child_info">
            <div class="outer-child-info" >
                {assign var=id value=rand()}
                <table class="ty-table ty-table-middle ty-table-responsive">
                    <thead>
                        <tr>
                            <th>{__("ec_child_birthdate")}</th>
                            <th>{__("ec_child_name")}</th>
                            {* <th>{__("ec_phone_number")}</th>
                            <th>{__("ec_email")}</th> *}
                            <th>&nbsp;</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr id="box_main_content_child">
                            <td>
                                <input type="date" name="ec_child_birthdate[]" value="" class=""/>
                            </td>
                            <td>
                                <input type="text" name="ec_child_name[]" value="" class=""/>
                            </td>
                            {* <td>
                                <input type="text" name="ec_phone_number[]" value="" class=""/>    
                            </td>
                            <td>
                                <input type="text" name="ec_email[]" value="" class=""/>
                            </td> *}
                            <td>
							    {include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="main_content_child"}                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="buttons-container clearfix buttons-container-picker">
            <div class="ty-float-right">
                <button class="ty-btn__login ty-btn__secondary ty-btn cm-post" type="submit" name="dispatch[ec_booking_classes.add_child]">{__("ec_button_submit_child")}</button>
            </div>
        </div>
        <style>
        .booking-select-container {
                min-width:300px;
                min-height: 300px;
                margin-top:25px;
                overflow:scroll;
            }
            .ec_child_button {
                font-size: unset !important;
            }
            .object-container {
                height:249px;
                overflow:hidden;
            } 

            .buttons-container {
                
                margin: 0px 20px -44px -47px !important;
            }
        </style>
    </div>
</form>