{$suffix = ""}
{if $product}
    {$suffix = $product.product_id}
{/if}

{$id = $id|default:"select_booking_{$obj_prefix}{$suffix}"}
{assign var="is_ajax" value=false}


{assign var="link_meta" value="ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn"}

<div class="ec_btn_calender_container">
    <div class="ec_book_btn_div">
        {foreach from=$all_type_available.type item=type key=key}
            {if $key == 'free_trail'}
                <a id="opener_select_booking_free_trail_{$obj_prefix}{$suffix}" class="cm-dialog-opener cm-dialog-auto-size ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn booking_button" data-ca-target-id="content_{$id}" data-ca-dialog-title="{__("ec_select_date_and_slot")}" rel="nofollow" data-ca-id="free_trail"><span>{__("ec_booking_free_trail")}</span></a>
            {/if}

            {if $key == 'book_type'}
                <a id="opener_select_booking_book_type_{$obj_prefix}{$suffix}" class="cm-dialog-opener cm-dialog-auto-size ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn booking_button" data-ca-target-id="content_{$id}" data-ca-dialog-title="{__("ec_select_date_and_slot")}" rel="nofollow" data-ca-id="book_type"><span>{__("ec_booking_book_type")}</span></a>
            {/if}

            {if $key == 'discount_book'}
                <a id="opener_select_booking_discount_book_{$obj_prefix}{$suffix}" class="cm-dialog-opener cm-dialog-auto-size ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn booking_button" data-ca-target-id="content_{$id}" data-ca-dialog-title="{__("ec_select_date_and_slot")}" rel="nofollow" data-ca-id="discount_book"><span>
                {__("ec_booking_discount_book")}</span></a>
            {/if}
        {/foreach}
    </div>
</div>

{if $smarty.request.dispatch == 'products.view'}
    {assign var="ec_capture" value="select_booking_popup`$obj_prefix``$suffix`"}
    {capture name=select_booking_popup}
        {include file="addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup_content.tpl" product=$product}
    {/capture}
    <div class="hidden{if $wysiwyg} ty-wysiwyg-content{/if}" id="content_{$id}" title="{$text}">
        {$smarty.capture.select_booking_popup nofilter}
    </div>
{/if}

<script>

(function (_, $) {
    $(document).on('click','.booking_button',function(){
       var e = $(this);
       var select_id = e.attr('data-ca-id');
       $('#select_types_{$product.product_id}').val(select_id);
    });
})(Tygh, Tygh.$);
</script>