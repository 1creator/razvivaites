{assign var="date_val" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
{assign var="date_val_format" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
{assign var="min_date_format" value={$smarty.const.TIME|date_format:"%d-%b-%Y"}}
{assign var=start_date value=$product.date_from|date_format:"%Y-%m-%d"}

{if $smarty.const.TIME > $product.date_from}
	{assign var="start_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
{/if}

{assign var=end_date value=$product.date_to|date_format:"%Y-%m-%d"}

{if $smarty.const.TIME > $product.date_to}
	{assign var="end_date" value={$smarty.const.TIME|date_format:"%Y-%m-%d"}}
{/if}

{include file="addons/ec_booking_classes/views/ec_booking_classes/components/booking_classes_template.tpl"}