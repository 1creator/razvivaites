 <div class="ty-cart-content__top-buttons clearfix">
    <div class="ty-float-right ty-cart-content__right-buttons">
        {if $auth.user_id}
            <a  href="{'ec_booking_classe.add_child'|fn_url}" id="opener_ec_booking_add_child" class="cm-dialog-opener cm-post cm-ajax cm-dialog-auto-size ty-btn ty-btn__secondary ec-seller-second-sear-icon" rel="nofollow" data-ca-target-id="ec_booking_Add_child_block"  data-ca-dialog-title="{__("ec_booking_add_child")}">{__("ec_add_book_child")}</a>
            <div id="ec_booking_Add_child_block" class="hidden" title="{__("sign_in")}">
                {include file="addons/ec_booking_classes/views/ec_booking_classes/components/ec_add_child_in.tpl"}
            </div>
        {/if}
    </div>
</div>

<form action="{""|fn_url}" method="post" target="_self" name="ec_book_child_add" style="min-height: 400px;">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{assign var="page_title" value=__("booked_orders")}
{assign var="extra_status" value=$config.current_url|escape:"url"}

{if $child_info}
<div class="table-responsive-wrapper">
    <table width="100%" class="ty-table ty-orders-search">
    <thead>
    <tr>
        <th width="25%"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_birthdate")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        {* <th width="25%"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th> *}
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_name")}{if $search.sort_by == "child_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        {* <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_phone&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_phone")}{if $search.sort_by == "child_phone"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_email")}{if $search.sort_by == "child_email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th> *}
        <th width="10%" class="right">&nbsp;&nbsp;</th>
    </tr>
    </thead>
    {foreach from=$child_info item="child_inf"}
    <tr>
       {* <td data-th="{__("id")}">
            {__("ec_book_child_name")} <bdi>#{$child_inf.id}</bdi>
        </td> *}
        <td data-th="{__("ec_child_name")}">
           {$child_inf.child_birthdate}
        </td>
        <td data-th="{__("ec_child_name")}">
           {$child_inf.child_name}
        </td>
         {* <td data-th="{__("ec_child_phone")}">
            {$child_inf.child_phone}
        </td>
        <td class="right" data-th="{__("ec_child_email")}">
            {$child_inf.child_email}
        </td> *}
        <td class="right">
            <a href='{"ec_booking_classes.delete_child&id=`$child_inf.id`"|fn_url}'  class="cm-post ty-btn btn ty-btn__primary">{__("ec_booking_classes_delete")}</a>
        </td>  
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="ty-no-items">{__("no_data")}</p>
{/if}


{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

</form>

{capture name="mainbox_title"}{__("ec_booked_add_child")}{/capture}

