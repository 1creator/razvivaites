<div id="booking-select-container{$product.product_id}">
	<div id="datepicker{$product.product_id}"></div>
	<style>
	#booking-select-container{$product.product_id} {
		position: relative;
	}
	#booking-select-container{$product.product_id} #datepicker{$product.product_id} {
		
	}
	.ui-datepicker td span, .ui-datepicker td a{
		text-align: center;
	}
	
	.booking-select-container{$product.product_id} {
		min-width:300px;
		min-height: 300px;
	}
	</style>
	{assign var=close_day value=fn_ec_booking_classes_get_close_day($product)}
	<script type="text/javascript">
		(function (_, $) {
			$(document).on('wk_test_event{$product.product_id}', function(){
				var dateObject 		= new Date(); 
				var product_id = '{$product.product_id}';
				var start_date = '{$date_val_format}';
				var end_date = '{$end_date}';
				var dateStringCheck = dateObject.getFullYear() + '-' + (dateObject.getMonth() + 1) + '-' + dateObject.getDate();
				ldate 				= '{$end_date}';
				sdate 				= dateStringCheck.split('-');
				psdate 				= '{$start_date}';
				close_day = '{$close_day}'
				psdate 				= psdate.split('-');
				ldate 				= ldate.split('-');
				var date1 			= new Date(sdate[0], sdate[1]-1, sdate[2]);
				var date2 			= new Date(ldate[0], ldate[1]-1, ldate[2]); 
				var date3 			= new Date(psdate[0], psdate[1]-1, psdate[2]);
			
				if(date3 >= date1) {
					date1 = date3;
				}
				data = JSON.parse(close_day);
				$('#datepicker{$product.product_id}').datepicker({
					beforeShowDay: function (date) {
						var day = date.getDay();
						if(jQuery.inArray(day,data) != -1) {
							return [false, '', ''];
						}
						if (date >= date1 && date <= date2) {
							//return [true, 'available', '{__("ec_select_date_tooltip")}'];
							return [true, 'available'];
						}
						
						return [false, '', ''];
					},
					onSelect: function(selectedDate) {
						$('#select_dat_{$product.product_id}').val(selectedDate);
						// openOptionPanel();
						// var ddd = new Date(selectedDate);
						// var selected_date = (ddd.getTime()-ddd.getMilliseconds()) / 1000;
						if (selectedDate != ''){
							$.ceAjax('request', fn_url('products.check_slots?template=3&selected_date=' + selectedDate +'&product_id={$product.product_id}'), {
								method: 'POST',
								result_ids: 'booking-select-container{$product.product_id}',
								full_render: true,
								callback: function(data) {
								}
							});
						}
					},
					firstDay: 1,
					{if $smarty.const.CART_LANGUAGE == "ru"}
					dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
					monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
					{/if}
				});

				$('#datepickertemp').datepicker({
					beforeShowDay: function (date) {
						var day = date.getDay();
						if(jQuery.inArray(day,data) != -1) {
							return [false, '', ''];
						}
						if (date >= date1 && date <= date2) {
							return [true, 'available', ''];
						}
						
						return [false, '', ''];
					},
					onSelect: function(selectedDate) {
					},
					firstDay: 1,
					{if $smarty.const.CART_LANGUAGE == "ru"}
					dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
					monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
					{/if}
				});
			});
			$(document).trigger('wk_test_event{$product.product_id}');
		})(Tygh, Tygh.$);
	</script>
<!--booking-select-container{$product.product_id}--></div>