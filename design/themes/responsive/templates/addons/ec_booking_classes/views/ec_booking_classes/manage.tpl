<form action="{""|fn_url}" method="post" target="_self" name="orders_list_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{assign var="page_title" value=__("booked_orders")}
{assign var="extra_status" value=$config.current_url|escape:"url"}

{if $booked_orders}
<div class="table-responsive-wrapper" style="min-height: 400px;">
    <table width="100%" class="ty-table ty-orders-search">
    <thead>
    <tr>
        <th width="5%"><a class="cm-ajax" href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "order_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_booking_classes_company_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="25%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_book_product_name")}{if $search.sort_by == "ec_book_product_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="15%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_booked_date")}{if $search.sort_by == "ec_booked_date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_booking_slot")}{if $search.sort_by == "ec_booking_slot"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_name_text")}{if $search.sort_by == "ec_child_name_text"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("price")}{if $search.sort_by == "price"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_book_status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    </tr>
    </thead>
    {foreach from=$booked_orders item="booked_order"}
    
    <tr>
        <td data-th="{__("id")}">
            <a href="{"ec_booking_classes.details?id=`$booked_order.id`"|fn_url}" class="underlined"><bdi>#{$booked_order.id}</bdi></a>
        </td>
        <td data-th="{__("name")}">
            {$booked_order.name}
            {* {assign var=user_name value=fn_get_user_name($booked_order.user_id)}
            {$user_name} *}
        </td>
        <td data-th="{__("ec_booked_date")}">
           {$booked_order.product}
        </td>             
        <td data-th="{__("ec_booked_date")}">
           {$booked_order.booking_info.booking_info.booking_date}
        </td>
        <td data-th="{__("ec_booked_date")}">
           {$booked_order.booking_info.booking_info.booking_slot}
        </td>
        <td data-th="{__("ec_booked_date")}">
           {$booked_order.booking_info.child.ec_child_name[0]}
        </td>
        <td data-th="{__("ec_booked_date")}">
           {$booked_order.price}
        </td>
        <td class="right" data-th="{__("total")}">
            {if $booked_order.status == 'A'} 
                {__("ec_booking_confirmed")}  
            {else if $booked_order.status == 'D'}
                {__("ec_booking_unconfirmed")}
            {else if $booked_order.status == 'C'}
                {__("ec_booking_cancelled")}
            {/if}
        </td>
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}


{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

</form>

{capture name="mainbox_title"}{__("ec_booked_orders")}{/capture}

