{script src="js/addons/ec_booking_classes/node-clone/node_cloning.js"}
<div id="booking-select-container">
    <div class="bookingSlotoption">
		<h3>
            {* <a class="button-icon ty-btn ty-btn__secondary" id="main_content_child" alt="Add empty item" title="Add empty item" onclick="Tygh.$('#box_' + this.id).cloneNode(1); ">{__("ec_child_add_info")}</a> *}
            {__('select_book_child')}

			<span class="close optionClose closeOptionPanel" data-product-id="{$product.product_id}">&#x276e;</span> 
		</h3>
    </div>
    <div class="inner-child-info">
    </div>
    <div class="outer_main_child_info">
        <div class="outer-child-info" >
            {assign var=id value=rand()}
            <table class="ty-table ty-table-middle ty-table-responsive">
                <thead>
                    <tr>
                        <th>{__("ec_child_birthdate")}</th>
                        <th width="10%">&nbsp;</th>
                        <th>{__("ec_child_name")}</th>
                        <th width="10%">&nbsp;</th>

                    </tr>
                </thead>
                <tbody>
                    {if $child_info}
                        {foreach from=$child_info item=value}
                            {assign var=ids value=rand()}
                            <tr id="box_main_content_child{$ids}">
                                <td>
                                    <input type="date" name="product_child[ec_child_birthdate][]" value="{$value.child_birthdate}" class=""/>
                                </td>
                                <td>
                                    <input type="text" name="product_child[ec_child_name][]" value="{$value.child_name}" class=""/>
                                </td>
                                <td class="right">
                                    {include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="main_content_child$ids" only_delete="Y"}
                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                    {assign var=id value=rand()}
                    <tr id="box_main_content_child{$id}">
                        <td>
                            <input type="date" name="product_child[ec_child_birthdate][]" value="" class=""/>
                        </td>
                        <td>
                            <input type="text" name="product_child[ec_child_name][]" value="" class=""/>
                        </td>
                        <td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="main_content_child$id"}
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="buttons-container clearfix buttons-container-picker" style="position: absolute; bottom: 0px;">
        <div class="ty-float-right">
            <button class="ty-btn__login ty-btn__secondary ty-btn cm-ajax cm-dialog-closer select_booking_slot_button_cart" type="submit" name="dispatch[products.savebookclasses]">{__("ec_button_submit")}</button>

            <button id="button_cart_{$product.product_id}" class="ty-btn__primary ty-btn__big ty-btn__add-to-cart cm-form-dialog-closer ty-btn select_booking_slot_button cm-ajax" type="submit" name="dispatch[checkout.add..{$product.product_id}]" data-ca-target-form="ec_booking_classes_{$product.product_id}">{__("ec_add_booking_cart")}</button>

        </div>
    </div>
    <style>
        .booking-select-container {
            min-width:300px;
            min-height: 300px;
        }
        .ec_child_button {
            font-size: unset !important;
        }
        .outer_main_child_info {
            margin-bottom: 35px;
            padding-bottom: 63px;
        }
	</style>
    <script>
        (function (_, $) {
            var book_data_val = $('#select_types_{$product.product_id}').val();
            console.log(book_data_val);
            if(book_data_val == 'free_trail' || book_data_val == 'book_type')
                $('.select_booking_slot_button').hide();
            else 
                $('.select_booking_slot_button_cart').hide();
        })(Tygh, Tygh.$);
    </script>
<!--booking-select-container--></div>