<div id="booking-select-container{$product.product_id}">
	<div class="bookingSlotoption">
		<h3>
			{__('select_slot_to_book')}
			<span class="close optionClose closeOptionPanel" data-product-id="{$product.product_id}">&#x276e;</span>
		</h3>
		<div class="slotOptions">
			<span id="beforeThis"></span>
			<table class="ty-table">
				<thead>
					<tr>
						<th class="text-center" style="text-align: center;">
							{__('start_time')}
						</th>
						<th class="text-center" style="text-align: center;">
							{__('last_time')}
						</th>
						<th class="text-center" style="text-align: center;">
							{__('action')}
						</th>
					</tr>
				</thead>
				<tbody id="tbody_booking_{$product.product_id}">
					{foreach from=$all_slots.available_time_slots item=slots key=i_d}
						<tr class="ty-center">
							<td class="td-booking">
								{$slots.0}
							</td>
							<td>
								{$slots.1}
							</td>
							<td>
								<input type="hidden" value="{$slots.0} to {$slots.1}" class="booking_info">
                                <input type="hidden" name="slot_0" value="{$slots.0}" />
                                <input type="hidden" name="slot_1" value="{$slots.1}" />
                                {* <input type="radio" name="" value= ""  class="" /> *}
								{* {include file="buttons/button.tpl" but_id=$but_id but_text=$but_text|default:__("ec_book_cl_book") but_name=$but_name  but_role=$but_role|default:"text" but_meta="ty-btn__primary ty-btn__big select_booking_slot booking_info_slot"} *}
								<a href="" class="ty-btn ty-btn__primary ty-btn__big select_booking_slot booking_info_slot" data-product-id="{$product.product_id}">
									{__("ec_book_cl_book") }
								</a>
								
							</td>
						</tr>
					{foreachelse}
						<tr class="ty-table__no-items">
							<td colspan="3"><p class="ty-no-items">{__("no_slots_available")}</p></td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
	
	<style>
	.booking-select-container {
		min-width:300px;
		min-height: 300px;
	}
	</style>
<!--booking-select-container{$product.product_id}--></div>