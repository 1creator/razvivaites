{style src="addons/ec_booking_classes/jquery-ui-1.10.4.custom.css"}
{script src="js/addons/ec_booking_classes/datepicker_custom/jquery-ui-1.10.4.custom.js"}

{assign var=all_slots value=fn_ec_booking_classes_get_single_day_all_slots($date_val_format,$product.product_id)}
{assign var=all_type_available value=fn_ec_boking_classes_get_all_available_book_type($product)}

<input type="hidden" id="select_type_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_slot]" />
<input type='hidden' id='select_dat_{$product.product_id}' name="product_data[{$product.product_id}][booking_info][booking_date]" value="{$date_val}"/>
<input type="hidden" id="select_types_{$product.product_id}" name="product_data[{$product.product_id}][booking_info][booking_type]" value="{$all_type_available.first_element}"/>

{* <div class="ty-control-group">
    <label class="ty-control-group__label " for="select_book_type_{$product.product_id}" >{__("ec_book_select_type")}</label>
    <div class="ty-control-group__items">
        <select name="select_book_type" class="ec_book_sel_type" id="select_book_type_{$product.product_id}" >
            {foreach from=$all_type_available.type item=type key=key}
                <option value="{$key}">{$type}</option>
            {/foreach}
        </select>
    </div>
</div> *}
<style>
    .ty-control-group__items {
        display: inline-block;
        margin-left: 21px;
        word-break: break-word;
    }
</style>
<script>
	(function (_, $) {
        $(".ec_book_sel_type").change(function(){
            var bok_type = $(this).children("option:selected").val();
            $('#select_types_{$product.product_id}').val(bok_type);
        });
    })(Tygh, Tygh.$);
</script>
{include file="addons/ec_booking_classes/views/ec_booking_classes/components/multiple_slot_popup.tpl" product=$product link_text=__('add_booking') text=__('wk_select_date_and_slot')}