{strip}
<div id="ab__fn-second-level-{$block.block_id}-{$key}" class="ab-fn-second-level{if $is_first} active first{/if}" data-childs-count="{$item.subitems|count}" data-add-delimeter="{if $scroller}false{else}true{/if}">
{if $is_first && $item.subitems}
{include file="addons/ab__fast_navigation/blocks/ab__fast_navigation/components/second_level_items.tpl" item=$item}
{/if}
{if $item.href && $properties.ab__fn_add_link == 'Y'}
<a href="{$item.href|fn_url}" class="ab-fn-second-level-item ab-fn-common-item-link ty-column{$properties.ab__fn_number_of_columns_desktop}{if !$item.subitems} ab-fn-no-second-lvl{$min_height=$first_level_icon_width+48+14}{else}{$min_height=$first_level_icon_width}{/if}" style="min-height: {$min_height}px;">
<div class="ab-fn-item-header">
<span>{__("ab__fn.more")}</span>
</div>
<span class="ab-fn-arrow-more"><i></i></span>
</a>
{/if}
</div>
{if $scroller && $is_first}
{include file="addons/ab__fast_navigation/blocks/ab__fast_navigation/js/second_level_scroller.tpl" key=$key scroller=$second_level_scroller}
{/if}
{/strip}