{strip}
<script>
Tygh.ab__fn.scroller_settings_{$block.block_id} = {
direction: '{$language_direction}',
navigation: true,
pagination: false,
navigationText: ["<i class='ty-icon-left-open-thin ab-fn-nav-buttons'></i>", "<i class='ty-icon-right-open-thin ab-fn-nav-buttons'></i>"],
items: {$properties.ab__fn_number_of_columns_desktop},
itemsDesktopSmall: [1230, {$properties.ab__fn_number_of_columns_desktop_small}],
itemsTablet: [992, {$properties.ab__fn_number_of_columns_tablet}],
itemsTabletSmall: [768, {$properties.ab__fn_number_of_columns_tablet_small}],
itemsMobile: [576, {$properties.ab__fn_number_of_columns_mobile}],
afterMove: function() {
var scroller = $(this)[0];
var tabs_content = $(".ab-fn-second-level[id^='ab__fn-second-level-{$block.block_id}-']");
var visible_elems = scroller.visibleItems;
var customer_elements = scroller.$userItems;
var active_index_of = visible_elems.indexOf(Number(customer_elements.filter(".active").attr("data-item-index")));
if ( active_index_of === -1 ) {
if ( Number(customer_elements.filter(".active").attr("data-item-index")) > visible_elems[visible_elems.length-1] ) {
customer_elements.removeClass("active");
customer_elements.eq(visible_elems[visible_elems.length-1]).click();
tabs_content.removeClass("active");
tabs_content.eq(visible_elems[visible_elems.length-1]).addClass("active");
} else {
customer_elements.removeClass("active");
customer_elements.eq(visible_elems[0]).click();
tabs_content.removeClass("active");
tabs_content.eq(visible_elems[0]).addClass("active");
}
}
},
afterAction: function() {
var scroller = $(this)[0];
var scroller_elem = scroller.$elem;
if (!scroller_elem.hasClass("active"))
return;
var visible_elems = scroller.visibleItems;
var customer_elements = scroller.$userItems;
Tygh.ab__fn_lazy_load(customer_elements, visible_elems);
if (visible_elems[0] === 0) {
scroller_elem.find(".owl-prev").addClass("ab-fn-hidden");
if (visible_elems[visible_elems.length-1] !== customer_elements.length - 1) {
scroller_elem.find(".owl-next").removeClass("ab-fn-hidden");
}
} else if (visible_elems[visible_elems.length-1] === customer_elements.length - 1) {
scroller_elem.find(".owl-next").addClass("ab-fn-hidden");
if (visible_elems[0] !== 0) {
scroller_elem.find(".owl-prev").removeClass("ab-fn-hidden");
}
} else {
scroller_elem.find(".owl-prev, .owl-next").removeClass("ab-fn-hidden");
}
{if $first_level_scroller && $properties.ab__fn_init_scrollbar == 'Y'}
var scrollbar = scroller_elem.siblings(".ab-fn-scrollbar").find(".ab-fn-scrollbar-plate");
var percents_by_item = 100 / (customer_elements.length - visible_elems.length);
var scroll_to = visible_elems[0]*percents_by_item;
scrollbar.css("width", Math.round(scroll_to) + '%');
{/if}
}
};
</script>
{/strip}