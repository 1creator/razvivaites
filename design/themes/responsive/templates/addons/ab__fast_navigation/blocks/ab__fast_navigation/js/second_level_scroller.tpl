{strip}
<script>
(function (_, $) {
$.ceEvent('on', 'ce.ab__fn_init', function(context) {
context.find("#ab__fn-second-level-{$block.block_id}-{$key}").addClass("ab-fn-second-level-scroller").owlCarousel({
direction: '{$language_direction}',
navigation: true,
pagination: false,
navigationText: ["<i class='ty-icon-left-open-thin ab-fn-nav-buttons'></i>", "<i class='ty-icon-right-open-thin ab-fn-nav-buttons'></i>"],
items: {$block.properties.ab__fn_number_of_columns_desktop},
itemsDesktopSmall: [1230, {$block.properties.ab__fn_number_of_columns_desktop_small}],
itemsTablet: [992, {$block.properties.ab__fn_number_of_columns_tablet}],
itemsTabletSmall: [768, {$block.properties.ab__fn_number_of_columns_tablet_small}],
itemsMobile: [576, {$block.properties.ab__fn_number_of_columns_mobile}],
afterAction: function () {
var scroller = $(this)[0];
var visible_elems = scroller.visibleItems;
var customer_elements = scroller.$userItems;
_.ab__fn_lazy_load(customer_elements, visible_elems);
var scroller_elem = scroller.$elem;
if (visible_elems[0] === 0) {
scroller_elem.find(".owl-prev").addClass("ab-fn-hidden");
if (visible_elems[visible_elems.length-1] !== customer_elements.length-1) {
scroller_elem.find(".owl-next").removeClass("ab-fn-hidden");
}
} else if (visible_elems[visible_elems.length-1] === customer_elements.length-1) {
scroller_elem.find(".owl-next").addClass("ab-fn-hidden");
if (visible_elems[0] !== 0) {
scroller_elem.find(".owl-prev").removeClass("ab-fn-hidden");
}
} else {
scroller_elem.find(".owl-prev, .owl-next").removeClass("ab-fn-hidden");
}
}
});
});
})(Tygh, Tygh.$);
</script>
{/strip}