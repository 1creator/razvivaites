<script>
(function ( _, $ ) {
$.extend(_, {
ab__fn: {
blocks: { },
delimeter_data: {
text: '{__("ab__fn.show_more")|escape:"javascript"}'
}
}
});
function ab__fn_lazy_load (customer_elements, visible_elems) {
customer_elements.each(function(index) {
if( !$(this).hasClass("ab-fn-loaded-image") )
if ( visible_elems.indexOf(index) !== -1 ) {
var data_src = $(this).find("img").attr("data-src");
$(this).find("img").on("load", function(){
$(this).parents(".scroller-item").find(".ab-fn-item-header").attr("style", '');
$(this).parents(".scroller-item").addClass("ab-fn-loaded-image");
});
$(this).find("img").attr("src", data_src);
}
});
}
_.ab__fn_lazy_load = ab__fn_lazy_load;
})(Tygh, Tygh.$);
</script>
{script src="js/addons/ab__fast_navigation/ab__fn_customer.js"}