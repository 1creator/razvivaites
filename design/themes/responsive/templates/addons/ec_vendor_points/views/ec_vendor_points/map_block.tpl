{if $shipping.service_params.display}
    {$display_type = $shipping.service_params.display}
{else}
    {$display_type = "ML"}
{/if}

{if $display_type != "L"}
    {$display_pickup_map = true}
{/if}

<form action="{"index.index"|fn_url}" id="store_locator_search_form" class="{if !$type}ty-hidden hidden{/if}">
    <input type="hidden" name="result_ids" value="store_locator_search_controls,store_locator_location,store_locator_search_block_*" />
    <input type="hidden" name="full_render" value="Y" />
{if !$type}
<style>
    .ec-vendor-point-map .pickup--map-list, .ec-vendor-point-map .pickup--list{
        display: block !important;
    }
    .ec-vendor-point-map .pickup__map-wrapper{
        width: 100% !important;
        height: 405px !important;
    }
    .ty-geo-maps__geolocation__map.ec_category_map{
        width: 100%;
        height: auto;
    }


</style>
{/if}
{if $type == 'homepage'}
    <style>
    {* @media (min-width: 768px){ *}
        .ty-geo-maps__geolocation__map.ec_homepage_map {
            width: 100% !important;
            height: 100% !important;
        }
    {* } *}
    .store-locator .pickup--map-list .ty-one-city__name, .store-locator .pickup--list .ty-one-city__name{
        padding: 5px 20px !important;
    }
    .store-locator .pickup--map-list .pickup__search {
        background-color: transparent;
        padding-bottom: 0;
    }
    .store-locator__all-stores-btn{
        padding: 5px;
    }
    .ec-vendor-point-map .pickup--map-list, .ec-vendor-point-map .pickup--list{
        display: block !important;
    }
    .ec-vendor-point-map .pickup__map-wrapper{
        width: 100% !important;
        height: 405px !important;
    }
    </style>
    <div id="store_locator_search_controls" class="store-locator__location--wrapper">
        <h2 class="store-locator__step-title">{__("ec_vendor_points_header")}</h2>
    <!--store_locator_search_controls--></div>
{/if}

<div id="store_locator_location">
    <div class="store-locator ec-vendor-point-map ty-geo-maps__geolocation__map {if $type == 'homepage'}ec_homepage_map {else}ec_category_map{/if}">
        {include file="addons/ec_vendor_points/views/ec_vendor_points/components/map_and_list_store.tpl" store_locations=$ec_store_locations group_key=0 store_locations_count=$ec_store_locations_count category_view=$category_view }
    </div>
<!--store_locator_location--></div>

{script src="js/addons/store_locator/pickup_search.js"}
{script src="js/addons/store_locator/map.js"}
</form>
{if !$type}
    {if $runtime.controller == 'products' && $runtime.mode == 'view'}
    {else}
    <div class="ty-left"><a class="ty-btn ty-btn__primary" onclick="Tygh.$(this).toggleClass('ty-btn__primary');Tygh.$('#store_locator_search_form').slideToggle(450);">{__("ec_vendor_points_map")}</a></div>
    {/if}
{/if}
