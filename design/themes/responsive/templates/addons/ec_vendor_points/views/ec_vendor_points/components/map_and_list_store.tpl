{$_max_desktop_items = $max_desktop_items|default:5}

<div class="pickup{if $display_pickup_map} pickup--map-list{else} pickup--list{/if}">
    {if $display_pickup_map}
        {$map_container_id = "store_locator_stores_map_`$category_view`"}
        {hook name="checkout:store_locator_search_map"} 
        {/hook}
    {/if}

    {$group = $group_key|cat:$category_view}

    {* For mobiles; List wrapper with selected pickup item *}
    {foreach from=$store_locations item=store_location}
        {foreach from=$store_location item=store}
            {if $old_store_id == $store.store_location_id}
            <div class="pickup__offices-wrapper visible-phone pickup__offices-wrapper--near-map">
                {* List *}
                <div class="store-locator__fields-row store-locator__fields-row--wrapped pickup__offices pickup__offices--list pickup__offices--list-no-height">
                    {include file="addons/store_locator/components/items/store.tpl" store=$store group_key=$group}
                </div>
                {* End of List *}
            </div>
            {/if}
        {/foreach}
    {/foreach}
    <a href="#store_locator_search_form" class="store-locator__scroll-top-btn">{__("store_locator.scroll_to_top")}</a>

   

</div>
