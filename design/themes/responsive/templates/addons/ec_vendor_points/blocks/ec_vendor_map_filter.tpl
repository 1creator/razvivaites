{$block_id = $block.snapping_id|default:$id}
<div class="ec_vendor_filter_map" style="padding: 20px 0px;" id="product_filters_vmap_{$block_id}">
{$products = $products}
{if $runtime.controller == 'products' && $runtime.mode == 'view'}
    {$products = array($product)}
{/if}
    {$store_ids = fn_get_products_vendor_stores($products)}
    {$store_ids = implode(',',$store_ids)}
    {$ec_store_locations =fn_ec_get_vendor_filter_map_data($store_ids)}
    {include file="addons/ec_vendor_points/views/ec_vendor_points/map_block.tpl" category_view='category'}

<!--product_filters_vmap_{$block_id}--></div>
