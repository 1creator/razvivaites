{if "MULTIVENDOR"|fn_allowed_for && ($company_name || $company_id) && $settings.Vendors.display_vendor == "Y"}
    <div class="ut2-vendor-block">
        <div class="ut2-vendor-block__logo">{include file="common/image.tpl" images=$company_data.logos.theme.image image_width="80" image_height="80"}</div>
        <div class="ut2-vendor-block__content">
			<div class="ut2-vendor-block__name"><label>{__("vendor")}:</label><a href="{"companies.products?company_id=`$company_id`"|fn_url}">{if $company_name}{$company_name}{else}{$company_id|fn_get_company_name}{/if}</a></div>
	        <div class="ut2-vendor-block__info">{$company_data.company_description|strip_tags|truncate:90:"...":true}</div>
	        <div class="ut2-vendor-block__contacts">
				{if $company_data.phone}<div class="ut2-vendor-block__phone"><i class="ut2-icon-outline-headset_mic"></i> {$company_data.phone nofilter}</div>{/if}
				{hook name="companies:product_company_data"}{/hook}
			</div>
        </div>
    </div>
{/if}