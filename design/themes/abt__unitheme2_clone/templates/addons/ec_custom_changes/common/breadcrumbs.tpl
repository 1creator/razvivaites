<div id="breadcrumbs_{$block.block_id}" class="ec_breadcrumbs">

{if $breadcrumbs && $breadcrumbs|@sizeof > 1}
    <div class="breadcrumb">
		{strip}
            {foreach from=$breadcrumbs item="bc" name="bcn" key="key"}
                {if $bc.link}
                    <a href="{$bc.link|fn_url}" class="{if $additional_class} {$additional_class}{/if}"{if $bc.nofollow} rel="nofollow"{/if}>    
                        <span class="breadcrumb__inner">
                            <span class="breadcrumb__title">{$bc.title|strip_tags|escape:"html" nofilter}</span>
                        </span>
                    </a>
                {else}
                    <a href="{$bc.link|fn_url}" class="ec_disabled_link {if $additional_class} {$additional_class}{/if}"{if $bc.nofollow} rel="nofollow"{/if}>
                        <span class="breadcrumb__inner">
                            <span class="breadcrumb__title">{$bc.title|strip_tags|escape:"html" nofilter}</span>
                        </span>
                    </a>
                {/if}
            {/foreach}
            {* {include file="common/view_tools.tpl"} *}
        {/strip}
	</div>
{/if}
<!--breadcrumbs_{$block.block_id}--></div>
	