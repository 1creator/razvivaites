{script src="js/addons/ec_booking_classes/datepair-lib/jquery.timepicker.js" class="cm-ajax-force"}
{style src="addons/ec_booking_classes/datepair-lib/jquery.timepicker.css"}
{script src="js/addons/ec_booking_classes/datepair-lib/jquery.datepair.js" class="cm-ajax-force"}
{script src="js/addons/ec_booking_classes/datepair-lib/datepair.js" class="cm-ajax-force"}

<div class="hidden" id="content_booking_classes">
    {include file="common/subheader.tpl" title=__("ec_book_classes_header") target="#acc_pos"}
    {if $product_data.week_data}
	    {assign var=week_data value=unserialize($product_data.week_data)}
	{else}
	    {assign var=week_data value=array()}
	{/if}

{* {$week_data|fn_print_rx	} *}
    <div id="acc_pos" class="collapse in">
        <div class="control-group">
            <label class="control-label" for="book_type">{__("ec_book_classes_type")}:</label>
            <div class="controls">
                <div class="ec_main_check_container">
                    <div class="ec_controls">
                        <input type="hidden" name="product_data[booking_data][free_trail]" value="N">
                        <span class="checkbox">
                            <input type="checkbox" id="ec_free_trail" name="product_data[booking_data][free_trail]" value="Y" {if $product_data.free_trail == "Y"}checked="checked"{/if}>
                        </span>
                    </div>
                    <label for="ec_free_trail" class="ec_control-label">{__("ec_free_trail")}:</label>
                </div>
                <div class="ec_main_check_container">
                    <div class="ec_controls">
                        <input type="hidden" name="product_data[booking_data][book_type]" value="N">
                        <span class="checkbox">
                            <input type="checkbox" id="ec_book_type" name="product_data[booking_data][book_type]" value="Y" {if $product_data.book_type == "Y"}checked="checked"{/if}>
                        </span>
                    </div>
                    <label for="ec_book_type" class="ec_control-label">{__("ec_book_type")}:</label>
                </div>
              <div class="ec_main_check_container" style="display:none;">
                    <div class="ec_controls">
                        <input type="hidden" name="product_data[booking_data][discount_book]" value="N">
                        <span class="checkbox">
                            <input type="checkbox" id="ec_discount_book" name="product_data[booking_data][discount_book]" value="Y" {if $product_data.discount_book == "Y"}checked="checked"{/if}>
                        </span>
                    </div>
                    <label for="ec_discount_book" class="ec_control-label">{__("ec_discount_book")}:</label>
                </div>

            </div>
        </div>
		 <div class="control-group">
            <label for="elm_date_from" class="control-label cm-required">{__("date_from")}:</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="elm_date_from" date_name="product_data[booking_data][date_from]" date_val=$product_data.date_from|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
            </div>
        </div>
        <div class="control-group">
            <label for="elm_date_to" class="control-label cm-required">{__("date_to")}:</label>
            <div class="controls">
                {include file="common/calendar.tpl" date_id="elm_date_to" date_name="product_data[booking_data][date_to]" date_val=$product_data.date_to|default:$smarty.const.TIME start_year=$settings.Company.company_start_year}
            </div>
        </div>
    </div>

	{include file="common/subheader.tpl" title=__("ec_book_monday_header") target="#acc_monday"}
	<div class="control-group ec_date_main_data" id="acc_monday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][monday][status]" {if $week_data.monday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][monday][status]" {if $week_data.monday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.monday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.monday}
						{if $week_data.monday.single_start_time}
							{assign var=max_lenght value=count($week_data.monday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="mon`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][monday][single_start_time][]" value="{$week_data.monday.single_start_time.$index}" /> до
										<input type="text" class="time end input-small" id="elm_monday_timing_end" name="product_data[booking_data][week_data][monday][single_end_time][]" value="{$week_data.monday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][monday][book_seat][]" value="{$week_data.monday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="mon`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][monday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_monday_timing_end" name="product_data[booking_data][week_data][monday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][monday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	{include file="common/subheader.tpl" title=__("ec_book_tuesday_header") target="#acc_tuesday"}
	<div class="control-group ec_date_main_data" id="acc_tuesday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][tuesday][status]" {if $week_data.tuesday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][tuesday][status]" {if $week_data.tuesday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.tuesday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.tuesday}
						{if $week_data.tuesday.single_start_time}
							{assign var=max_lenght value=count($week_data.tuesday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="tues`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][tuesday][single_start_time][]" value="{$week_data.tuesday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_tuesday_timing_end" name="product_data[booking_data][week_data][tuesday][single_end_time][]" value="{$week_data.tuesday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][tuesday][book_seat][]" value="{$week_data.tuesday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="tues`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][tuesday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_tuesday_timing_end" name="product_data[booking_data][week_data][tuesday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][tuesday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	{include file="common/subheader.tpl" title=__("ec_book_wednesday_header") target="#acc_wednesday"}
	<div class="control-group ec_date_main_data" id="acc_wednesday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][wednesday][status]" {if $week_data.wednesday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][wednesday][status]" {if $week_data.wednesday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.wednesday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.wednesday}
						{if $week_data.wednesday.single_start_time}
							{assign var=max_lenght value=count($week_data.wednesday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="wed`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][wednesday][single_start_time][]" value="{$week_data.wednesday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_wednesday_timing_end" name="product_data[booking_data][week_data][wednesday][single_end_time][]" value="{$week_data.wednesday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][wednesday][book_seat][]" value="{$week_data.wednesday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="wed`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][wednesday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_wednesday_timing_end" name="product_data[booking_data][week_data][wednesday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][wednesday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	{include file="common/subheader.tpl" title=__("ec_book_thursday_header") target="#acc_thursday"}
	<div class="control-group ec_date_main_data" id="acc_thursday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][thursday][status]" {if $week_data.thursday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][thursday][status]" {if $week_data.thursday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.thursday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.thursday}
						{if $week_data.thursday.single_start_time}
							{assign var=max_lenght value=count($week_data.thursday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="thu`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][thursday][single_start_time][]" value="{$week_data.thursday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_thursday_timing_end" name="product_data[booking_data][week_data][thursday][single_end_time][]" value="{$week_data.thursday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][thursday][book_seat][]" value="{$week_data.thursday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="thu`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][thursday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_thursday_timing_end" name="product_data[booking_data][week_data][thursday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][thursday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	{include file="common/subheader.tpl" title=__("ec_book_friday_header") target="#acc_friday"}
	<div class="control-group ec_date_main_data" id="acc_friday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][friday][status]" {if $week_data.friday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][friday][status]" {if $week_data.friday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.friday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.friday}
						{if $week_data.friday.single_start_time}
							{assign var=max_lenght value=count($week_data.friday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="fri`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][friday][single_start_time][]" value="{$week_data.friday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_friday_timing_end" name="product_data[booking_data][week_data][friday][single_end_time][]" value="{$week_data.friday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][friday][book_seat][]" value="{$week_data.friday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="fri`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][friday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_friday_timing_end" name="product_data[booking_data][week_data][friday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][friday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	{include file="common/subheader.tpl" title=__("ec_book_saturday_header") target="#acc_saturday"}
	<div class="control-group ec_date_main_data" id="acc_saturday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][saturday][status]" {if $week_data.saturday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][saturday][status]" {if $week_data.saturday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.saturday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.saturday}
						{if $week_data.saturday.single_start_time}
							{assign var=max_lenght value=count($week_data.saturday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="sat`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][saturday][single_start_time][]" value="{$week_data.saturday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_saturday_timing_end" name="product_data[booking_data][week_data][saturday][single_end_time][]" value="{$week_data.saturday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][saturday][book_seat][]" value="{$week_data.saturday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="sat`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][saturday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_saturday_timing_end" name="product_data[booking_data][week_data][saturday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][saturday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	    {include file="common/subheader.tpl" title=__("ec_book_sunday_header") target="#acc_sunday"}
	<div class="control-group ec_date_main_data" id="acc_sunday">
		<label class="control-label">{__("ec_book_classes_status")}:</label>
		<div class="controls">
			<p>
				<label class="open-close-radio day-open">{__('open')}<input value="open" type="radio" name="product_data[booking_data][week_data][sunday][status]" {if $week_data.sunday.status == "open"}checked{/if} /></label>
				<label class="open-close-radio day-close">{__('close')}<input value="close" type="radio" name="product_data[booking_data][week_data][sunday][status]" {if $week_data.sunday.status != "open"}checked{/if} /></label>
			</p>
		</div>
		<div class="controlss ec_date_inner" {if $week_data.sunday.status != 'open'}hidden{/if}>
			<table class="table table-middle" width="100%">
				<thead class="cm-first-sibling">
					<tr>
						<th width="20%">{__("start_time")}{__('ec__booking_to_hypen')}{__("end_time")}</th>
						<th width="20%">{__("ec_number_available_seat")}</th>
						<th width="15%">&nbsp;</th>
					</tr>
				</thead>
				<tbody>	
					{if $week_data.sunday}
						{if $week_data.sunday.single_start_time}
							{assign var=max_lenght value=count($week_data.sunday.single_start_time) - 1}
						{else}
							{assign var=max_lenght value=0}
						{/if}
						{for $index=0 to $max_lenght}
							{assign var=ids value=rand()}
							{assign var=id value="sun`$ids`"}
							<tr class="cm-row-item" id="box_book_slot{$id}">
								<td>
									<p id="datepairRange_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][sunday][single_start_time][]" value="{$week_data.sunday.single_start_time.$index}" /> по
										<input type="text" class="time end input-small" id="elm_sunday_timing_end" name="product_data[booking_data][week_data][sunday][single_end_time][]" value="{$week_data.sunday.single_end_time.$index}"/>
									</p>
									<script>
										(function (_, $) {
											$.ceEvent('on', 'ce.commoninit', function(context) {
												$('#datepairRange_{$id} .time').timepicker({
													'showDuration': true,
													'timeFormat': 'H:i',
													'step': '5'
												});
												var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
												var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
											});
										}(Tygh, Tygh.$));
									</script>
								</td>
								<td>
									<p id="datepaiseat_{$id}" class="dateRange">
										<input type="text" class="time start input-small" name="product_data[booking_data][week_data][sunday][book_seat][]" value="{$week_data.sunday.book_seat.$index}" />
									</p>
								</td>
								<td class="right">
									{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id" only_delete="Y"}
								</td>
							</tr>
						{/for}
					{/if}
							{assign var=ids value=rand()}
							{assign var=id value="sun`$ids`"}
					<tr class="cm-row-item" id="box_book_slot{$id}">
						<td>
							<p id="datepairRange_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][sunday][single_start_time][]" value="" /> по
								<input type="text" class="time end input-small" id="elm_sunday_timing_end" name="product_data[booking_data][week_data][sunday][single_end_time][]" value=""/>
							</p>
							<script>
								(function (_, $) {
									$.ceEvent('on', 'ce.commoninit', function(context) {
										$('#datepairRange_{$id} .time').timepicker({
											'showDuration': true,
											'timeFormat': 'H:i',
											'step': '5'
										});
										var datepairRange_{$id} = document.getElementById('datepairRange_{$id}');
										var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});

										$(document).on('click','#datepairRange_{$id}_1 .time',function(){
											$('#datepairRange_{$id}_1 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2 .time',function(){
											$('#datepairRange_{$id}_1_2 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});

										$(document).on('click','#datepairRange_{$id}_1_2_3 .time',function(){
											$('#datepairRange_{$id}_1_2_3 .time').timepicker({
												'showDuration': true,
												'timeFormat': 'H:i',
												'step': '5'
											});
											var datepairRange_{$id} = document.getElementById('datepairRange_{$id}_1_2_3');
											var timeOnlyDatepair_{$id} = new Datepair(datepairRange_{$id});
										});
									});
								}(Tygh, Tygh.$));
							</script>
						</td>
						<td>
							<p id="datepaiseat_{$id}" class="dateRange">
								<input type="text" class="time start input-small" name="product_data[booking_data][week_data][sunday][book_seat][]" value="" />
							</p>
						</td>
						<td class="right">
							{include file="addons/ec_booking_classes/buttons/multiple_buttons.tpl" item_id="book_slot$id"}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
</div>

<style>
	.ec_controls {
		display:inline-block;
	}

	.ec_control-label {
		display:inline-block;
	}

  .open-close-radio:first-of-type{
        margin-right: 10px;
    }
    .open-close-radio input{
        margin-left: 5px;
    }
</style>

<script>
 	(function (_, $) {
		$('.day-open').on('click',function(){
            $(this).parents('.ec_date_main_data').children('.ec_date_inner').show();
        });
        $('.day-close').on('click',function(){
            $(this).parents('.ec_date_main_data').children('.ec_date_inner').hide();
        });
	}(Tygh, Tygh.$));
</script>