{capture name="mainbox"}
{if $booked_order}
    {capture name="tabsbox"}
        <div class="product-manage form-horizontal" id="content_detailed">
            <div class="control-group">
                <label class="control-label ec-booked_inlie" for="elm_product_product_name">{__("ec_book_product_name")}:</label>
                <div class="controls ec-booked_inlie">
                    {assign var = product_name value= fn_get_product_name($booked_order.product_id)}
                    <a href="{"products.update?product_id=`$booked_order.product_id`"|fn_url}" id="elm_product_product_name" class="underlined"><bdi id="test">{$product_name}</bdi></a>                    
                </div>
            </div>
            {assign var=user_data value=fn_get_user_short_info($booked_order.user_id)}
            <div class="control-group">
                <label class="control-label ec-booked_inlie" for="elm_product_customer_name">{__("ec_customer_name")}:</label>
                <div class="controls ec-booked_inlie">
                <input type="text" name="booking_data[name]" id="elm_price_price" size="10" value="{$user_data.firstname} - {$user_data.lastname}" class="input-long">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label ec-booked_inlie" for="elm_product_customer_email">{__("ec_customer_email")}:</label>
                <div class="controls ec-booked_inlie">
                <input type="text" name="booking_data[email]" id="elm_product_customer_email" size="10" value="{$user_data.email}" class="input-long">
                </div>
            </div>
             <div class="control-group">
                <label class="control-label ec-booked_inlie" for="elm_product_book_date">{__("ec_booking_date")}:</label>
                <div class="controls ec-booked_inlie">
                <input type="text" name="booking_data[book_date]" id="elm_product_book_date" size="10" value="{$booked_order.booking_info.booking_info.booking_date}" class="input-long">
                </div>
            </div>
             <div class="control-group">
                <label class="control-label ec-booked_inlie" for="elm_product_book_slot">{__("ec_booking_slot")}:</label>
                <div class="controls ec-booked_inlie">
                <input type="text" name="booking_data[book_slot]" id="elm_product_book_slot" size="10" value="{$booked_order.booking_info.booking_info.booking_slot}" class="input-long">
                </div>
            </div>
            {include file="common/subheader.tpl" title=__("ec_book_child") target="#ec_book_child"}
            {assign var=total_child value=count($booked_order.booking_info.child.ec_child_name)}
            <div id="ec_book_child" class="collapse in">
                <table width="100%" class="table table-middle table-responsive">
                    <thead>
                        <tr>
                            <th>{__("ec_price_text")}</th>
                            <th>{__("ec_child_name_text")}</th>
                            {* <th>{__("ec_phone_number_text")}</th>
                            <th>{__("ec_email_text")}</th> *}
                        </tr>
                    </thead>
                    <tbody>
                        {for $i=0 to $total_child}
                            <tr>
                                <td>
                                    {$booked_order.booking_info.child.ec_child_name[$i]}
                                </td>
                                {* <td>
                                    {$booked_order.booking_info.child.ec_phone_number[$i]}
                                </td>
                                <td>
                                    {$booked_order.booking_info.child.ec_email[$i]}
                                </td>    *}
                            </tr>
                        {/for}
                    <tbody>
                </table>
            </div>
        <!--content_detailed--></div>
    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}
{/if}
{/capture}

{capture name="mainbox_title"}
    {__("ec_booked_orders")}
{/capture}

<style>

</style>

{include file="common/mainbox.tpl"
    title_start=$title_start
    title_end=$title_end
    title=$smarty.capture.mainbox_title
    content=$smarty.capture.mainbox
    select_languages=$id
    buttons=$smarty.capture.buttons
    adv_buttons=$smarty.capture.adv_buttons
}