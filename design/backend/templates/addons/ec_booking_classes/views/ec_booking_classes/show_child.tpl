{capture name="mainbox"}

{capture name="sidebar"}
    {hook name="orders:manage_sidebar"}
    {include file="addons/ec_booking_classes/views/ec_booking_classes/components/booked_orders_search_form.tpl" dispatch="orders.manage"}
    {/hook}
{/capture}

<form action="{""|fn_url}" method="post" target="_self" name="orders_list_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{assign var="page_title" value=__("ec_booked_add_child")}
{assign var="extra_status" value=$config.current_url|escape:"url"}

{if $child_info}
<div class="table-responsive-wrapper">
    <table width="100%" class="table table-middle table-responsive">
    <thead>
    <tr>
        <th width="25%"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_name")}{if $search.sort_by == "child_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
         <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_phone&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_phone")}{if $search.sort_by == "child_phone"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=child_email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("ec_child_email")}{if $search.sort_by == "child_email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th> *}
        <th width="10%" class="right">&nbsp;&nbsp;</th>
    </tr>
    </thead>
    {foreach from=$child_info item="child_inf"}
    <tr>
        <td data-th="{__("id")}">
            {__("ec_book_child_name")} <bdi>#{$child_inf.id}</bdi>
        </td>
        <td data-th="{__("ec_child_name")}">
           {$child_inf.child_name}
        </td>
         {* <td data-th="{__("ec_child_phone")}">
            {$child_inf.child_phone}
        </td>
        <td class="right" data-th="{__("ec_child_email")}">
            {$child_inf.child_email}
        </td> *}
        <td class="right">
            <a href='{"ec_booking_classes.delete_child&id=`$child_inf.id`&user_id=`$smarty.request.user_id`"|fn_url}'  class="cm-post btn btn-primary">{__("ec_booking_classes_delete")}</a>
        </td> 
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}


{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

</form>
{/capture}

{include file="common/mainbox.tpl" title=$page_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="manage_orders"}
