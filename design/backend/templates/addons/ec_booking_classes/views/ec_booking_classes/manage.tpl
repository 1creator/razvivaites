{capture name="mainbox"}

{capture name="sidebar"}
    {hook name="orders:manage_sidebar"}
    {include file="addons/ec_booking_classes/views/ec_booking_classes/components/booked_orders_search_form.tpl" dispatch="orders.manage"}
    {/hook}
{/capture}




<form action="{""|fn_url}" method="post" target="_self" name="orders_list_form">

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{assign var="c_icon" value="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"icon-dummy\"></i>"}
{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}
{assign var="page_title" value=__("booked_orders")}
{assign var="extra_status" value=$config.current_url|escape:"url"}
{if $booked_orders}
<div class="table-responsive-wrapper">
    <table width="100%" class="table table-middle table-responsive">
    <thead>
    <tr>
        <th  class="left mobile-hide" width="1%">
        {include file="common/check_items.tpl" check_statuses=$order_status_descr}
        </th>
        <th width="5%"><a class="cm-ajax" href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%">
            {assign var = product_name value = fn_get_product_name($bk_order.id)}
            <a class="cm-ajax"  data-ca-target-id={$rev}>{__("ec_book_product_name")}{if $search.sort_by == "ec_book_product_name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax"  data-ca-target-id={$rev}>ФИО клиента{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%">
            {assign var=total_child value=count($bk_order.booking_info.child.ec_child_name)}
            <a class="cm-ajax"  data-ca-target-id={$rev}>{__("ec_child_name_text")}{if $search.sort_by == "ec_child_name_text"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%">
            <a class="cm-ajax" data-ca-target-id={$rev}>{__("ec_phone_number_text")}{if $search.sort_by == "ec_phone_number_text"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="20%"><a class="cm-ajax" data-ca-target-id={$rev}>{__("ec_booked_date")}{if $search.sort_by == "ec_booked_date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th width="30%"><a class="cm-ajax"  data-ca-target-id={$rev}>{__("ec_booking_slot")}{if $search.sort_by == "ec_booking_slot"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th class="mobile-hide">&nbsp;</th>
        <th width="10%" class="right"><a class="cm-ajax"  data-ca-target-id={$rev}>{__("ec_book_status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    </tr>
    </thead>
    {foreach from=$booked_orders item="bk_order"}
    <tr>
        <td class="left mobile-hide">
            <input type="checkbox" name="ids[]" value="{$bk_order.id}" class="cm-item cm-item-status-{$bk_order.status|lower}" /></td>
        <td data-th="{__("id")}">
            <a href="{"ec_booking_classes.details?id=`$bk_order.id`"|fn_url}" class="underlined"><bdi>#{$bk_order.id}</bdi></a>
        </td>
        {assign var = product_number value = fn_get_order_info($bk_order.order_id)}
        {* assign var = product_item value = current($product_number.products) *}
        <td data-th="{__("product_name")}">
        <a href="{"ec_booking_classes.details?id=`$bk_order.id`"|fn_url}" id="elm_product_product_name" class="underlined">
            {$product_number.products[{$product_item.item_id}].product}
        </a>
        </td>
        <td data-th="{__("name")}">
            {assign var=user_name value=fn_get_user_name($bk_order.user_id)}
            {$user_name}
        </td>
        {for $i=0 to $total_child}
        <td data-th="{__("ec_child_name_text")}">
            {$bk_order.booking_info.child.ec_child_name[$i]}
        </td>
        <td data-th="{__("ec_phone_number_text")}">
        {$bk_order.booking_info.child.ec_phone_number[$i]}
        </td>
        {/for}
         <td data-th="{__("ec_booked_date")}">
           {$bk_order.booking_info.booking_info.booking_date}
        </td>
        <td data-th="{__("ec_booking_slot")}">
           {$bk_order.booking_info.booking_info.booking_slot}
        </td>
        <td width="9%" class="nowrap mobile-hide">
                <div class="hidden-tools">
                    {capture name="tools_list"}
                        <li>{btn type="list" text=__("edit") href="ec_booking_classes.details?id=`$bk_order.id`"}</li>
                    {/capture}
                    {dropdown content=$smarty.capture.tools_list}
                </div>
            </td>
        <td class="right" data-th="{__("total")}">
            {assign var=items_status value=fn_ec_booking_classes_get_status()}
            {include file="common/select_popup.tpl" popup_additional_class="dropleft" id=$bk_order.id status=$bk_order.status hidden=true object_id_name="id" table="ec_booking_reser_booking_info" items_status=$items_status}
        </td>
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}


{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

</form>
{/capture}


{include file="common/mainbox.tpl" 
title=$page_title 
sidebar=$smarty.capture.sidebar 
content=$smarty.capture.mainbox 
buttons=$smarty.capture.buttons 
adv_buttons=$smarty.capture.adv_buttons 
content_id="manage_orders"
} 
    