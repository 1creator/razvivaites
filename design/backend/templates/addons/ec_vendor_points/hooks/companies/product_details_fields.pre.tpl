{if "MULTIVENDOR"|fn_allowed_for && $mode != "add"}
    {assign var=js_action value="fn_ec_change_vendor_for_product();" scope="global"}
    {if "MULTIVENDOR"|fn_allowed_for}
    <script type="text/javascript">
    var fn_ec_change_vendor_for_product = function(){
        $.ceAjax('request', Tygh.current_url, {
        data: {
            product_data: {
            company_id: $('[name="product_data[company_id]"]').val(),
            category_ids: $('[name="product_data[category_ids]"]').val()
            }
        },
        result_ids: 'product_categories,ec_vendor_points'
        });
    };
    </script>
    {/if}
{/if}