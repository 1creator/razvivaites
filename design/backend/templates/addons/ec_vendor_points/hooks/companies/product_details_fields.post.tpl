{assign var=result_ids value=$result_ids|cat:',ec_vendor_points' scope="global"}
<div class="control-group {$no_hide_input_if_shared_product}" id="ec_vendor_points">
    <label for="ec_vendor_points_add" class="control-label">{__("ec_select_vendor_point")}</label>
    <div class="controls">
        {$store_points = fn_ec_get_vendor_store_points($ec_store_company|default:$product_data.company_id)}
        <div class="object-selector">
            <select id="ec_vendor_points_add"
                class="cm-object-selector"
                single
                name="product_data[ec_store_location_id]"
                data-ca-enable-images="false"
                data-ca-enable-search="true"
                data-ca-load-via-ajax="false"
                data-ca-placeholder="{__("type_to_search")}"
                data-ca-allow-clear="true">

                <option value=""></option>
                {if $store_points} 
                    {foreach from=$store_points item=store_point}
                        <option value="{$store_point.store_location_id}" {if $product_data.ec_store_location_id == $store_point.store_location_id} selected='selected'{/if}>{$store_point.name}</option>
                    {/foreach}
                {/if}
            </select>
        </div> 
    </div>
<!--ec_vendor_points--></div>