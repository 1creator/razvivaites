{if !empty($user.can_resend_confirmation)}
	<li class="divider"></li>
	<li>{btn type="list" text=__("wk_resend_confirmation_code") href="resend_confirmation_code.customer?user_id=`$user.user_id`"}</li>
{/if}