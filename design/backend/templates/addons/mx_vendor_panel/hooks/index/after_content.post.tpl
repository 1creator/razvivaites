{if $content_tpl!='views/products/update.tpl'}
{style src="addons/mx_vendor_panel/styles.less"}
{/if}

{if $content_tpl='views/companies/update.tpl'}
<style>
/* hide status and language company update */
#content_detailed fieldset > div:nth-child(3) {
    display: none;
}

#content_detailed fieldset > div:nth-child(4) {
    display: none;
}
</style>
{/if}

{if $content_tpl!='views/products/add.tpl'}
<style>
/*hide category input*/
.object-categories-add .object-categories-add__picker {
    right: unset;
    left: 1px;
}

.object-categories-add .select2-search.select2-search--inline {
    display: none;
}

.object-categories-add .select2.select2-container.select2-container--default.ui-sortable {
    padding-left: 40px;
    width: 50%!important;
}

</style>
{/if}