                <div class="control-group">
                    <label for="elm_name" class="cm-required control-label">{__("name")}:</label>
                    <div class="controls">
                        <input type="text" id="elm_name" name="store_location_data[name]" value="{$store_location.name}">
                    </div>
                </div>

                {include file="views/companies/components/company_field.tpl"
                    name="store_location_data[company_id]"
                    id="company_id_{$id}"
                    selected=$store_location.company_id
                }

                <div class="control-group">
                    <label class="control-label" for="elm_position">{__("position")}:</label>
                    <div class="controls">
                        <input type="text" name="store_location_data[position]" id="elm_position" value="{$store_location.position}" size="3">
                    </div>
                </div>

                <div class="control-group">
                    <label for="elm_pickup_address" class="control-label">{__("address")}</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="store_location_data[pickup_address]" id="elm_pickup_address" size="55" value="{$store_location.pickup_address}" />
                    </div>
                </div>

                <div class="control-group">
                    <label for="elm_pickup_phone" class="control-label">{__("phone")}</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="store_location_data[pickup_phone]" id="elm_pickup_phone" size="55" value="{$store_location.pickup_phone}" />
                    </div>
                </div>

                <div class="control-group">
                    <label for="elm_pickup_work_time" class="control-label">{__("store_locator.work_time")}</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="store_location_data[pickup_time]" id="elm_pickup_work_time" size="55" value="{$store_location.pickup_time}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="elm_description">{__("description")}:</label>
                    <div class="controls">
                        <textarea id="elm_description" name="store_location_data[description]" cols="55" rows="2" class="cm-wysiwyg input-textarea-long">{$store_location.description}</textarea>
                    </div>
                </div>

            {*  <div class="control-group">
                    <label class="control-label" for="elm_country">{__("country")}:</label>
                    <div class="controls">
                        {assign var="countries" value=1|fn_get_simple_countries:$smarty.const.CART_LANGUAGE}
                        <select id="elm_country_{$id}" name="store_location_data[country]" class="select cm-country cm-location-{$id}">
                            <option value="">- {__("select_country")} -</option>
                            {foreach from=$countries item="country" key="code"}
                                <option {if $store_location.country == $code}selected="selected"{/if} value="{$code}" title="{$country}">{$country}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="elm_country">{__("state")}:</label>
                    <div class="controls">
                        <select id="elm_state_{$id}" class="cm-state cm-location-{$id}" name="store_location_data[state]">
                            <option value="">- {__("select_state")} -</option>
                            {foreach $states[$store_location.country] as $state_id => $state}
                                <option {if $state_id == $store_location.state}selected{/if} value="{$state_id}">{$state.state}</option>
                            {/foreach}
                        </select>
                        <input type="text"
                               id="elm_state_{$id}_d"
                               name="store_location_data[state]"
                               value="{$store_location.state}"
                               disabled="disabled"
                               class="cm-state cm-location-{$id} hidden"
                        />
                    </div>
                </div>
            *}
                <script type="text/javascript">
                    (function(_, $) {
                        $.ceRebuildStates('init', {
                            default_country: '{$store_location.country|escape:javascript}',
                            states: {$states|json_encode nofilter}
                        });
                    }(Tygh, Tygh.$));
                </script>


                <div class="control-group">
                    <label class="control-label" for="elm_city">{__("city")}:</label>
                    <div class="controls">
                        <input type="text" name="store_location_data[city]" id="elm_city" value="{$store_location.city}">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label cm-required">{__("coordinates")}:</label>
                    <div class="controls">
                        {hook name="store_locator:select_coordinates"}
                        <input type="text" name="store_location_data[latitude]" id="elm_latitude" value="{$store_location.latitude}" data-ca-latest-latitude="{$store_location.latitude}" class="input-small">
                        &times;
                        <input type="text" name="store_location_data[longitude]" id="elm_longitude" value="{$store_location.longitude}" data-ca-latest-longitude="{$store_location.longitude}" class="input-small"> 
                        {/hook}
                    </div>
                </div>

                {include file="views/localizations/components/select.tpl" data_from=$store_location.localization data_name="store_location_data[localization]"}

                {hook name="store_locator:detailed_content"}
                {/hook}

                {include file="common/select_status.tpl" input_name="store_location_data[status]" id="elm_status" obj_id=$store_location.location_id obj=$store_location}