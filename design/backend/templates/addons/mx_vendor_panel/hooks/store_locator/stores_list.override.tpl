                {assign var="allow_save" value=$loc|fn_allow_save_object:"store_locations"}
                {if $allow_save}
                    {assign var="no_hide_input" value="cm-no-hide-input"}
                    {assign var="display" value=""}
                {else}
                    {assign var="no_hide_input" value=""}
                    {assign var="display" value="text"}
                {/if}

                <td class="left {$no_hide_input} mobile-hide">
                    <input type="checkbox" name="store_locator_ids[]" value="{$loc.store_location_id}" class="cm-item" /></td>

                <td data-th="{__("store_locator")}">
                    <a class="row-status" href="{"store_locator.update?store_location_id=`$loc.store_location_id`"|fn_url}">{$loc.name}</a>
                    {include file="views/companies/components/company_name.tpl" object=$loc}
                </td>

                <td data-th="{__("city")}">
                    <a class="row-status" >{$loc.city}</a>
                </td>
