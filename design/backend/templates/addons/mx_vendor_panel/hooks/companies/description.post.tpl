<div class="control-group">
    <label class="control-label">{__("mx_vendor_panel_vendor_type")}:</label>
    <div class="controls">
        <label class="radio"><input type="radio" id="ip" name="company_data[vendor_type]" value="I" {if $company_data.vendor_type == "I"}checked="checked"{/if}/>{__("mx_vendor_panel_ip")}</label>
        <label class="radio"><input type="radio" id="ooo" name="company_data[vendor_type]" value="O" {if $company_data.vendor_type == "O"}checked="checked"{/if}/>{__("mx_vendor_panel_ooo")}</label>        
        <label class="radio"><input type="radio" id="dr" name="company_data[vendor_type]" value="D" {if $company_data.vendor_type == "D"}checked="checked"{/if}/>{__("mx_vendor_panel_other")}</label>
    </div>
</div>

 <div class="control-group" id="other">
    <label for="other" class="control-label">{__("mx_vendor_panel_other_name")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[other]" size="10" value="{$company_data.other}" />
    </div>
</div>

 <div class="control-group" id="fio">
    <label for="fio" class="control-label">{__("mx_vendor_panel_fio")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[fio]" size="10" value="{$company_data.fio}" />
    </div>
</div>

 <div class="control-group" id="name">
    <label for="name" class="control-label">{__("mx_vendor_panel_name")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[name]" size="10" value="{$company_data.name}" />
    </div>
</div>

 <div class="control-group">
    <label for="inn" class="control-label">{__("mx_vendor_panel_inn")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[inn]" size="10" value="{$company_data.inn}" />
    </div>
</div>

 <div class="control-group">
    <label for="kpp" class="control-label">{__("mx_vendor_panel_kpp")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[kpp]" size="10" value="{$company_data.kpp}" />
    </div>
</div>

 <div class="control-group">
    <label for="ogrn" class="control-label">{__("mx_vendor_panel_ogrn")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[ogrn]" size="10" value="{$company_data.ogrn}" />
    </div>
</div>

<h4 class="subheader">{__("mx_vendor_panel_rekvizity")}</h4>

 <div class="control-group">
    <label for="rs" class="control-label">{__("mx_vendor_panel_rs")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[rs]" size="10" value="{$company_data.rs}" />
    </div>
</div>

 <div class="control-group">
    <label for="ks" class="control-label">{__("mx_vendor_panel_ks")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[ks]" size="10" value="{$company_data.ks}" />
    </div>
</div>

 <div class="control-group">
    <label for="bik" class="control-label">{__("mx_vendor_panel_bik")}</label>
    <div class="controls">
        <input class="input-long" type="text" name="company_data[bik]" size="10" value="{$company_data.bik}" />
    </div>
</div>

 <div class="control-group">
    <label for="bank" class="control-label">{__("mx_vendor_panel_bank")}</label>
    <div class="controls">
        <input class="input-large" type="text" name="company_data[bank]" size="10" value="{$company_data.bank}" />
    </div>
</div>

 <div class="control-group" id="u_address">
    <label for="u_address" class="control-label">{__("mx_vendor_panel_u_address")}</label>
    <div class="controls">
        <input class="input-large" type="text" name="company_data[u_address]" size="10" value="{$company_data.u_address}" />
    </div>
</div>

 <div class="control-group" id="manager">
    <label for="manager" class="control-label">{__("mx_vendor_panel_manager")}</label>
    <div class="controls">
        <input class="input-large" type="text" name="company_data[manager]" size="10" value="{$company_data.manager}" />
    </div>
</div>


<script>
(function($)
{
	 if($('#ip').is(':checked')) {
            $('#u_address').hide();
            $('#manager').hide();           
            $('#name').hide(); 
            $('#other').hide(); 
            $('#fio').show();           
       }

       else if($('#ooo').is(':checked')) {
            $('#u_address').show();
            $('#manager').show();   
            $('#name').show();  
            $('#other').hide();
            $('#fio').hide();  
       }         
       else if($('#other').is(':checked')) {
            $('#u_address').show();
            $('#manager').show();   
            $('#name').show();  
            $('#other').show();
            $('#fio').hide();  
       }       

       else {
            $('#u_address').show();
            $('#manager').show();   
            $('#name').show();  
            $('#other').hide();
            $('#fio').hide();  
       }
	 $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'ip') {
            $('#u_address').hide();
            $('#manager').hide();           
            $('#name').hide();  
            $('#other').hide();
            $('#fio').show();          
       }

       else if($(this).attr('id') == 'ooo'){
            $('#u_address').show();
            $('#manager').show();   
            $('#name').show();  
            $('#other').hide();
            $('#fio').hide();  
       }       
       else {
            $('#u_address').show();
            $('#manager').show();   
            $('#name').show(); 
            $('#other').show();  
            $('#fio').hide();  
       }
   });
	   
})(jQuery);
</script>