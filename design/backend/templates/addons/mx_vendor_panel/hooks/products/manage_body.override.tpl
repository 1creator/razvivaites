            <td width="6%" class="left mobile-hide">
            <input type="checkbox" name="product_ids[]" value="{$product.product_id}" class="cm-item cm-item-status-{$product.status|lower} hide" /></td>
            {if $search.cid && $search.subcats != "Y"}
            <td class="{if $no_hide_input_if_shared_product}{$no_hide_input_if_shared_product}{/if}">
                <input type="text" name="products_data[{$product.product_id}][position]" size="3" value="{$product.position}" class="input-micro" /></td>
            {/if}
            <td class="products-list__image">
                {include 
                        file="common/image.tpl" 
                        image=$product.main_pair.icon|default:$product.main_pair.detailed 
                        image_id=$product.main_pair.image_id 
                        image_width=$settings.Thumbnails.product_admin_mini_icon_width 
                        image_height=$settings.Thumbnails.product_admin_mini_icon_height 
                        href="products.update?product_id=`$product.product_id`"|fn_url
                        image_css_class="products-list__image--img"
                        link_css_class="products-list__image--link"
                }
            </td>
            <td class="product-name-column" data-th="{__("name")}">
                <input type="hidden" name="products_data[{$product.product_id}][product]" value="{$product.product}" {if $no_hide_input_if_shared_product} class="{$no_hide_input_if_shared_product}"{/if} />
                <a class="row-status" title="{$product.product|strip_tags}" href="{"products.update?product_id=`$product.product_id`"|fn_url}">{$product.product|truncate:40 nofilter}</a>
                <div class="product-list__labels">
                    {hook name="products:product_additional_info"}
                        <div class="product-code">
                            <span class="product-code__label">{$product.product_code}</span>
                        </div>
                    {/hook}
                </div>
                {include file="views/companies/components/company_name.tpl" object=$product}
            </td>
            <td width="13%" class="{if $no_hide_input_if_shared_product}{$no_hide_input_if_shared_product}{/if}" data-th="{__("price")}">
                {include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id="price_`$product.product_id`" name="update_all_vendors[price][`$product.product_id`]"}
                <input type="text" name="products_data[{$product.product_id}][price]" size="6" value="{$product.price|fn_format_price:$primary_currency:null:false}" class="input-mini input-hidden"/>
            </td>