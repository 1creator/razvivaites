<div id="container_addon_option_ab__motivation_block_bg_color" class="control-group setting-wide ab__motivation_block">
<label class="control-label cm-color" for="addon_option_ab__motivation_block_bg_color">{__('ab__mb_bg_color')}:</label>
<div class="controls">
<input class="cm-ab-mb-colorpicker" style="font-family: monospace;" type="text" name="addon_data[ab__motivation_block][bg_color]" id="addon_option_ab__motivation_block_bg_color" value="{$addons.ab__motivation_block.bg_color|default:'#FFFFFF'}"/>
</div>
</div>