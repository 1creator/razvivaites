{if $in_popup}
<div class="adv-search">
<div class="group">
{else}
<div class="sidebar-row">
<h6>{__("search")}</h6>
{/if}
<form action="{""|fn_url}" name="ab__motivation_items_search_form" method="get" class="{$form_meta}" id="ab__motivation_items_search_form">
{if $put_request_vars}
{array_to_fields data=$smarty.request skip=["callback"]}
{/if}
{capture name="simple_search"}
<div class="sidebar-field">
<label>{__("category")}</label>
<div class="break clear correct-picker-but">
{if "categories"|fn_show_picker:$smarty.const.CATEGORY_THRESHOLD}
{if $search.category_id}
{assign var="s_cid" value=$search.category_id}
{else}
{assign var="s_cid" value="0"}
{/if}
{include file="pickers/categories/picker.tpl" data_id="location_category" input_name="category_id" item_ids=$s_cid hide_link=true hide_delete_button=true default_name=__("all_categories") extra=""}
{else}
{include file="common/select_category.tpl" name="category_id" id=$search.category_id}
{/if}
</div>
</div>
<div class="sidebar-field">
<label>{__("ab__mb.destinations")}</label>
<div class="break clear correct-picker-but">
{if $search.destination_id}
{assign var="s_did" value=$search.destination_id}
{else}
{assign var="s_did" value="0"}
{/if}
{include
file="addons/ab__motivation_block/pickers/destinations/picker.tpl"
input_name="destination_id"
item_ids=$s_did
data_id="location_destinations"
default_name=__("ab__mb_all_destinations")
prepend=true
}
</div>
</div>
<div class="sidebar-field">
<label for="ab__mb_name">{__("name")}:</label>
<div class="break">
<input type="text" name="name" id="ab__mb_name" value="{$search.name}" size="30" class="search-input-text" />
</div>
</div>
{/capture}
{include file="common/advanced_search.tpl" simple_search=$smarty.capture.simple_search advanced_search=$smarty.capture.advanced_search dispatch=$dispatch view_type="ab__motivation_items" in_popup=$in_popup}
<!--ab__motivation_items_search_form--></form>
{if $in_popup}
</div></div>
{else}
</div><hr>
{/if}