{strip}
{foreach $item.subitems as $subitem}
<a href="{$subitem.href|fn_url}" class="ab-fn-second-level-item {$subitem.class}{if $second_level_scroller} scroller-item{/if} ty-column{$properties.ab__fn_number_of_columns_desktop}">
<span class="ab-fn-sl-content">
<span class="ab-fn-image-wrap{if !$subitem.image} ab-fn-no-image-wrapper{/if}" style="width: {$first_level_icon_width}px;">
<span class="ab-fn-img-loader"></span>
{if ($subitem.image && $subitem.ab__fn_use_origin_image == 'Y')}
<img {if $second_level_scroller}src="." data-{/if}src="{$subitem.image.icon.$image_path}" alt="{$subitem.item}" style="width: {$first_level_icon_width}px">
{elseif $subitem.image}
{include file="common/image.tpl" lazy_load=$second_level_scroller image_height=$first_level_icon_width image_width=$first_level_icon_width images=$subitem.image}
{else}
<span class="ab-fn-no-image" style="width: {$first_level_icon_width}px;"><i class="ty-no-image__icon ty-icon-image" title="{__("no_image")}"></i></span>
{/if}
</span>
<span class="ab-fn-item-header"{if $subitem.image && $second_level_scroller} style="max-width: {$first_level_icon_width}px"{/if}>
<span>
{$subitem.item}
</span>
{if $subitem.ab__fn_label_text && $subitem.ab__fn_label_show == 'Y'}
<span class="ab-fn-label" style="background: {$subitem.ab__fn_label_background};">
<span style="color: {$subitem.ab__fn_label_color}">{$subitem.ab__fn_label_text}</span>
</span>
{/if}
</span>
</span>
</a>
{/foreach}
{/strip}