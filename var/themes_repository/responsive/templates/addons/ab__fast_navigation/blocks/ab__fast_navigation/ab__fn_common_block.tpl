{if $items}
{$properties = $block.properties}
{$content = $block.content}
{$first_level_icon_width=$properties.ab__fn_icon_width}
{$second_level=$second_level|default:false}
{$template_name=$template_name|default:'one_level'}
{$first_level_scroller = $properties.ab__fn_display_type == 'ab__fn_scroller' || $second_level}
{$second_level_scroller = $properties.ab__fn_init_second_level_scroll == 'Y'}
{strip}
{include file="addons/ab__fast_navigation/blocks/ab__fast_navigation/js/first_level_scroller.tpl"}
{$image_path = 'http_image_path'}
{if $smarty.const.HTTPS === true}
{$image_path = 'https_image_path'}
{/if}
<div class="ab-fn-parent ab-fn-block-{$block.block_id} {$block.user_class} clearfix" data-block-id="{$block.block_id}" data-snap="{$block.snapping_id}">
<div id="ab__fn-first-level-{$block.block_id}" class="ab-fn-first-level ab-fn-clipped {$properties.ab__fn_display_type} {$template_name} active">
{foreach $items as $item}
<div data-item-id="{$item@key}" data-item-index="{$item@index}" class="ab-fn-first-level-item{if $first_level_scroller} scroller-item{/if} ty-column{$properties.ab__fn_number_of_columns_desktop} {$item.class} ab-fn-dont-allow-link{!$second_level}{if $item@first} active{/if}">
<a href="{$item.href|fn_url}" class="ab-fn-fl-content ab-fn-allow-link-{!$second_level|default:'not'}">
<div class="ab-fn-image-wrap{if !$item.image} ab-fn-no-image-wrapper{/if}" style="width: {$first_level_icon_width}px;">
<div class="ab-fn-img-loader"></div>
{if $item.image && $item.ab__fn_use_origin_image == 'Y'}
<img {if $first_level_scroller}src="." data-{/if}src="{$item.image.icon.$image_path}" alt="{$item.item}" style="width: {$first_level_icon_width}px">
{elseif $item.image}
{include file="common/image.tpl" lazy_load=$first_level_scroller image_height=$first_level_icon_width image_width=$first_level_icon_width images=$item.image}
{else}
<span class="ab-fn-no-image" style="width: {$first_level_icon_width}px;"><i class="ty-no-image__icon ty-icon-image" title="{__("no_image")}"></i></span>
{/if}
</div>
<span class="ab-fn-item-header"{if $item.image && $first_level_scroller} style="max-width: {$first_level_icon_width}px"{/if}>
<span>{$item.item}</span>
{if $item.ab__fn_label_text && $item.ab__fn_label_show == 'Y'}
<span class="ab-fn-label" style="background: {$item.ab__fn_label_background};">
<span style="color: {$item.ab__fn_label_color}">{$item.ab__fn_label_text}</span>
</span>
{/if}
</span>
</a>
</div>
{/foreach}
</div>
{if $first_level_scroller && $properties.ab__fn_init_scrollbar == 'Y'}
<div id="ab__fn-scrollbar-{$block.block_id}" class="ab-fn-scrollbar">
<div class="ab-fn-scrollbar-plate"></div>
</div>
{/if}
{if $second_level}
{foreach $items as $item}
{$is_first=false}
{if $item@first}{$is_first=true}{/if}
{include file="addons/ab__fast_navigation/blocks/ab__fast_navigation/components/second_level_block.tpl" is_first=$is_first item=$item key=$item@key scroller=$second_level_scroller}
{/foreach}
{/if}
{if $content.ab__fn_show_common_btn == 'Y'}
<div class="ab-fn-common-link">
{if $content.ab__fn_common_btn_type == 'ab__fn_cbt_btn'}
<a href="{$content.ab__fn_show_common_btn_link|fn_url}" class="ty-btn ty-btn__primary ty-btn__big {$content.ab__fn_common_btn_class}">
<span>{$content.ab__fn_common_btn_text|default:__('ab__fn.front.button.defult_text')}</span>
</a>
{else}
<a href="{$content.ab__fn_show_common_btn_link|fn_url}" class="ab-fn-common-text-link {$content.ab__fn_common_btn_class}">
<span>{$content.ab__fn_common_btn_text|default:__('ab__fn.front.button.defult_text')}</span>
<i class="ty-product-switcher__icon ty-icon-right-circle"></i>
</a>
{/if}
</div>
{/if}
</div>
{/strip}
<script>
(function ( _ ) {
_.ab__fn.blocks['{$block.block_id}'] = {
number_of_columns_desktop: {$properties.ab__fn_number_of_columns_desktop},
number_of_columns_desktop_small: {$properties.ab__fn_number_of_columns_desktop_small},
number_of_columns_tablet: {$properties.ab__fn_number_of_columns_tablet},
number_of_columns_tablet_small: {$properties.ab__fn_number_of_columns_tablet_small},
number_of_columns_mobile: {$properties.ab__fn_number_of_columns_mobile}
};
})(Tygh);
</script>
{if $first_level_scroller}
<script>
(function (_, $) {
$.ceEvent('on', 'ce.ab__fn_init', function(context) {
context.find("#ab__fn-first-level-{$block.block_id}").owlCarousel(_.ab__fn.scroller_settings_{$block.block_id});
});
})(Tygh, Tygh.$);
</script>
{/if}
{/if}